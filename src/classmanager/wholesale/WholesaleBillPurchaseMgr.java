package classmanager.wholesale;

import datamanager.config.Config;
import datamanager.wholesale.WholesaleBillPurchase;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AMS
 */
public class WholesaleBillPurchaseMgr {
    
    //method to insert purchase in database
    public boolean insWholesaleBillPurchase(ArrayList<WholesaleBillPurchase> wholesalebillpurchase) {
                return insertPurchase(wholesalebillpurchase,null);
    }
//===============================================================================================================
//===============================================================================================================
    
    //method to insert purchase in database
    public boolean updWholesaleBillPurchase(ArrayList<WholesaleBillPurchase> wholesalebillpurchase, String wholesalebillid){
        try {
            Config.sql = "delete from wholesalebillpurchase where wholesalebillid = '" +wholesalebillid+ "'";
            Config.stmt.addBatch(Config.sql);
            return insertPurchase(wholesalebillpurchase,wholesalebillid);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
//===============================================================================================================
//===============================================================================================================
    
    //method to delete purchase in database
    public boolean delWholesaleBillPurchase(String wholesalebillid) {
        try {
            Config.sql = "delete from wholesalebillpurchase where wholesalebillid = '" + wholesalebillid + "'";
            Config.stmt.addBatch(Config.sql);
                return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }    
//===============================================================================================================
    
    public boolean insertPurchase(ArrayList<WholesaleBillPurchase> wholesalebillpurchase,String wholesalebillid){
        
        for (int i = 0; i < wholesalebillpurchase.size(); i++) {
                WholesaleBillPurchase wbp = wholesalebillpurchase.get(i);
                if (wholesalebillid==null) {
                    Config.sql = "insert into wholesalebillpurchase ("
                        + "wholesalebillid,"
                        + "product,"
                        + "flavor,"
                        + "contentrate,"
                        + "batchno,"
                        + "expdate,"
                        + "unit,"
                        + "mrp,"
                        + "quantity,"
                        + "pack,"
                        + "rate,"
                        + "amount,"
                        + "vat,"
                        + "vatamt,"
                        + "scheme,"
                        + "schemeamt)"
                        + "values ((Select max(wholesalebillid) wholesalebillid from wholesalebill),"
                        + "'"+wbp.getProduct()+"',"
                        + "'"+wbp.getFlavor()+"',"
                        + "'"+wbp.getContentrate()+"',"
                        + "'"+wbp.getBatchno()+"',"
                        + "'"+wbp.getExpdate()+"',"
                        + "'"+wbp.getUnit()+"',"
                        + "'"+wbp.getMrp()+"',"
                        + "'"+wbp.getQuantity()+"',"
                        + "'"+wbp.getPack()+"',"
                        + "'"+wbp.getRate()+"',"
                        + "'"+wbp.getAmount()+"',"
                        + "'"+wbp.getVat()+"',"
                        + "'"+wbp.getVatamt()+"',"
                        + "'"+wbp.getScheme()+"',"
                        + "'"+wbp.getSchemeamt()+"')";
                } else {
                    Config.sql = "insert into wholesalebillpurchase ("
                        + "wholesalebillid,"
                        + "product,"
                        + "flavor,"
                        + "contentrate,"
                        + "batchno,"
                        + "expdate,"
                        + "unit,"
                        + "mrp,"
                        + "quantity,"
                        + "pack,"
                        + "rate,"
                        + "amount,"
                        + "vat,"
                        + "vatamt,"
                        + "scheme,"
                        + "schemeamt)"
                        + "values ('"+wholesalebillid+"',"
                        + "'"+wbp.getProduct()+"',"
                        + "'"+wbp.getFlavor()+"',"
                        + "'"+wbp.getContentrate()+"',"
                        + "'"+wbp.getBatchno()+"',"
                        + "'"+wbp.getExpdate()+"',"
                        + "'"+wbp.getUnit()+"',"
                        + "'"+wbp.getMrp()+"',"
                        + "'"+wbp.getQuantity()+"',"
                        + "'"+wbp.getPack()+"',"
                        + "'"+wbp.getRate()+"',"
                        + "'"+wbp.getAmount()+"',"
                        + "'"+wbp.getVat()+"',"
                        + "'"+wbp.getVatamt()+"',"
                        + "'"+wbp.getScheme()+"',"
                        + "'"+wbp.getSchemeamt()+"')";
                }
                
                
            try {
                Config.stmt.addBatch(Config.sql);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
               Config.stockmgr.subStockArray(wbp);
            }
        return true;
    }
//===============================================================================================================
}
