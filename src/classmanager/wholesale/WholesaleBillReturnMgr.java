package classmanager.wholesale;

import datamanager.config.Config;
import datamanager.wholesale.WholesaleBillReturn;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author AMS
 */
public class WholesaleBillReturnMgr {

    //method to insert purchase in database
    public boolean insWholesaleBillReturn(ArrayList<WholesaleBillReturn> wholesalebillreturn) {
            return insertReturn(wholesalebillreturn);
    }
//===============================================================================================================
//===============================================================================================================
    
    //method to insert purchase in database
    public boolean updWholesaleBillReturn(ArrayList<WholesaleBillReturn> wholesalebillreturn, String wholesalebillid) {
        
        try {
            Config.sql = "delete from wholesalebillreturn where wholesalebillid = '" +wholesalebillid+ "'";
            Config.stmt.addBatch(Config.sql);
            if (insertReturn(wholesalebillreturn)) {
                return true;
            } else {
                return false;
            } 
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        
    }
//===============================================================================================================
    
//===============================================================================================================
    
    //method to delete purchase in database
    public boolean delWholesaleBillReturn(String wholesalebillid) {
        try {
            Config.sql = "delete from wholesalebillreturn where wholesalebillid = '" + wholesalebillid + "'";
            Config.stmt.addBatch(Config.sql);
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }    
//===============================================================================================================
    public boolean insertReturn(ArrayList<WholesaleBillReturn> wholesalebillreturn){
        for (int i = 0; i < wholesalebillreturn.size(); i++) {
                WholesaleBillReturn wbr = wholesalebillreturn.get(i);
                Config.sql = "insert into wholesalebillreturn ("
                        + "wholesalebillid,"
                        + "product,"
                        + "flavor,"
                        + "contentrate,"
                        + "batchno,"
                        + "expdate,"
                        + "unit,"
                        + "mrp,"
                        + "quantity,"
                        + "pack,"
                        + "rate,"
                        + "amount,"
                        + "vat,"
                        + "vatamt,"
                        + "scheme,"
                        + "schemeamt)"
                        + "values ((Select max(wholesalebillid) wholesalebillid from wholesalebill),"
                        + "'"+wbr.getProduct()+"',"
                        + "'"+wbr.getFlavor()+"',"
                        + "'"+wbr.getContentrate()+"',"
                        + "'"+wbr.getBatchno()+"',"
                        + "'"+wbr.getExpdate()+"',"
                        + "'"+wbr.getUnit()+"',"
                        + "'"+wbr.getMrp()+"',"
                        + "'"+wbr.getQuantity()+"',"
                        + "'"+wbr.getPack()+"',"
                        + "'"+wbr.getRate()+"',"
                        + "'"+wbr.getAmount()+"',"
                        + "'"+wbr.getVat()+"',"
                        + "'"+wbr.getVatamt()+"',"
                        + "'"+wbr.getScheme()+"',"
                        + "'"+wbr.getSchemeamt()+"')";
                    try {
                        Config.stmt.addBatch(Config.sql);
                    } catch (SQLException ex) {
                        Logger.getLogger(WholesaleBillReturnMgr.class.getName()).log(Level.SEVERE, null, ex);
                    }
            }
            return true;
    }
//===============================================================================================================
}
