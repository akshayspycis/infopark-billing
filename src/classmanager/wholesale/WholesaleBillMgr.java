package classmanager.wholesale;

import datamanager.Support.SupportWholeSaleBill;
import datamanager.config.Config;
import datamanager.customer.CustomerProfile;
import datamanager.wholesale.WholesaleBill;
import datamanager.wholesale.WholesaleBillPurchase;
import datamanager.wholesale.WholesaleBillReturn;
import java.sql.SQLException;

public class WholesaleBillMgr {
    
    //method to insert complete bill in database
    public boolean insWholesaleBill(WholesaleBill wholesalebill) {        
        try {
            //to fetch customer id
            String customerid = null;
            
            if (wholesalebill.getCustomerprofile().getCustomerid().equals("")) {
                
                CustomerProfile cp = new CustomerProfile();
                cp.setOrganization(wholesalebill.getCustomerprofile().getOrganization());
                cp.setCustomername(wholesalebill.getCustomerprofile().getCustomername());
                cp.setTinno(wholesalebill.getCustomerprofile().getTinno());
                cp.setLocality(wholesalebill.getCustomerprofile().getLocality());
                
                if (Config.customerprofilemgr.insCustomerProfile(cp)) {
                    Config.sql = "select max(customerid) customerid from customerprofile";
                    Config.rs = Config.stmt.executeQuery(Config.sql);
                    while (Config.rs.next()) {
                        customerid = Config.rs.getString("customerid");
                    }
                }
                } else {
                    customerid = wholesalebill.getCustomerprofile().getCustomerid();
                }
        //===============================================================================================================
            //to insert bill entry
            Config.conn.setAutoCommit(false);
            Config.sql = "insert into wholesalebill ("
                    + "customerid,"
                    + "billno,"
                    + "date,"
                    + "salesman,"
                    + "purchaseamt,"
                    + "schemeamt,"
                    + "vatamt,"
                    + "discount,"
                    + "payableamt,"
                    + "paidamt,"
                    + "balanceamt,"
                    + "print)"
                    + "values ('"+customerid+"',"
                    + "'"+wholesalebill.getBillno()+"',"
                    + "'"+wholesalebill.getDate()+"',"
                    + "'"+wholesalebill.getSalesman()+"',"
                    + "'"+wholesalebill.getPurchaseamt()+"',"
                    + "'"+wholesalebill.getSchemeamt()+"',"
                    + "'"+wholesalebill.getVatamt()+"',"
                    + "'"+wholesalebill.getDiscount()+"',"
                    + "'"+wholesalebill.getPayableamt()+"',"
                    + "'"+wholesalebill.getPaidamt()+"',"
                    + "'"+wholesalebill.getBalanceamt()+"',"
                    + "'"+wholesalebill.getPrint()+"')";
            Config.stmt.addBatch(Config.sql);
            
        //===============================================================================================================
            
            //to insert purchase entry and return entry            
            if (Config.wholesalebillpurchasemgr.insWholesaleBillPurchase(wholesalebill.getWholesalebillpurchase()) && Config.wholesalebillreturnmgr.insWholesaleBillReturn(wholesalebill.getWholesalebillreturn())) {
                Config.stmt.executeBatch();
                Config.configmgr.setCommit();
                return true;
            } else {
                return false;
            }
        //===============================================================================================================
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
//===============================================================================================================
//===============================================================================================================

    //method to update complete bill in database    
    public boolean updWholesaleBill(WholesaleBill wholesalebill) {
        
        try {
            //to fetch customer id
            String customerid = null;
            
            if (wholesalebill.getCustomerprofile().getCustomerid().equals("")) {
                
                CustomerProfile cp = new CustomerProfile();
                cp.setOrganization(wholesalebill.getCustomerprofile().getOrganization());
                cp.setCustomername(wholesalebill.getCustomerprofile().getCustomername());
                cp.setTinno(wholesalebill.getCustomerprofile().getTinno());
                cp.setLocality(wholesalebill.getCustomerprofile().getLocality());
                
                if (Config.customerprofilemgr.insCustomerProfile(cp)) {
                    Config.sql = "select max(customerid) customerid from customerprofile";
                    Config.rs = Config.stmt.executeQuery(Config.sql);
                    while (Config.rs.next()) {
                        customerid = Config.rs.getString("customerid");
                    }
                }
            } else {
                customerid = wholesalebill.getCustomerprofile().getCustomerid();
            }
        //===============================================================================================================
            
            //to update bill entry
            Config.sql = "update wholesalebill set "
                    + "customerid = '"+customerid+"', "
                    + "billno = '"+wholesalebill.getBillno()+"', "
                    + "date = '"+wholesalebill.getDate()+"', "                        
                    + "salesman = '"+wholesalebill.getSalesman()+"', "
                    + "purchaseamt = '"+wholesalebill.getPurchaseamt()+"', "
                    + "schemeamt = '"+wholesalebill.getSchemeamt()+"', "
                    + "vatamt = '"+wholesalebill.getVatamt()+"', "
                    + "discount = '"+wholesalebill.getDiscount()+"', "
                    + "payableamt = '"+wholesalebill.getPayableamt()+"', "
                    + "paidamt = '"+wholesalebill.getPaidamt()+"', "
                    + "balanceamt = '"+wholesalebill.getBalanceamt()+"', "
                    + "print = '"+wholesalebill.getPrint()+"' "
                    + "where wholesalebillid = '" + wholesalebill.getWholesalebillid() + "'";

            Config.stmt.addBatch(Config.sql);
            
        //===============================================================================================================
            
            //to update purchase entry, and return entry
            if (Config.wholesalebillpurchasemgr.updWholesaleBillPurchase(wholesalebill.getWholesalebillpurchase(), wholesalebill.getWholesalebillid()) && Config.wholesalebillreturnmgr.updWholesaleBillReturn(wholesalebill.getWholesalebillreturn(), wholesalebill.getWholesalebillid())&&Config.stockmgr.updStock() ) {
                Config.configmgr.setCommit();
                return true;
            } else {
                return false;
            }
        //===============================================================================================================            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }        
    }
    
    public boolean updWholeSale_PaidAmount(SupportWholeSaleBill support_wholesale_bill) {
        try {
            Config.sql = "update wholesalebill set "
                    + "paidamt = '"+support_wholesale_bill.getPaidamt()+"', "
                    + "balanceamt = '"+support_wholesale_bill.getBalanceamt()+"' "
                    + "where wholesalebillid = '" + support_wholesale_bill.getBillid()+ "'";
            Config.stmt.addBatch(Config.sql);
        //===============================================================================================================
            int a[]=Config.stmt.executeBatch();
            if (a.length>0) {
                Config.configmgr.setCommit();
                return true;
            } else {
                return false;
            }
        //===============================================================================================================            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }        
    }
//===============================================================================================================
//===============================================================================================================

    
    //method to delete complete bill in database
    public boolean delWholesaleBill(String wholesalebillid) {
        try {
            Config.sql = "delete from wholesalebill where wholesalebillid = '" + wholesalebillid + "'";
            Config.stmt.addBatch(Config.sql);
            if ( Config.wholesalebillpurchasemgr.delWholesaleBillPurchase(wholesalebillid) && Config.wholesalebillreturnmgr.delWholesaleBillReturn(wholesalebillid)&&Config.stockmgr.updStock()) {
                Config.configmgr.setCommit();
                Config.configmgr.loadStock();
                return true;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    


}
