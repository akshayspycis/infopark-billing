/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classmanager.thread;

import classmanager.pdfMgr.create.WholesaleBillIGst_pdf;
import classmanager.pdfMgr.create.WholesaleBill_Medical;
import classmanager.pdfMgr.create.WholesaleBill_MedicalIGst;
import classmanager.pdfMgr.create.WholesaleBill_pdf;
import classmanager.pdfMgr.create.WholesaleBill_pdf1;
import datamanager.config.Config;
import datamanager.wholesale.WholesaleBill;
import javax.swing.JOptionPane;

/**
 *
 * @author akshay
 */
public class WholeSaleNewBillPrint {
 public WholeSaleNewBillPrint(WholesaleBill wholesalebill,boolean check_inter_state){
         if(!check_inter_state){
             WholesaleBill_pdf1 pdfmgr=null;
             WholesaleBill_Medical pdfmgr1=null;
            if(Config.config_other_config.get(0).getWholesale_bill_type()==null||Config.config_other_config.get(0).getWholesale_bill_type().equals("")|| Config.config_other_config.get(0).getWholesale_bill_type().equals("false")?true:false){
                pdfmgr = new WholesaleBill_pdf1();                
                String filename = pdfmgr.createPdf(wholesalebill);
                if (filename != null) {
                    pdfmgr.printPdf(filename);
                } else {
                    JOptionPane.showMessageDialog(null,"Problem in bill printing.","Error.",JOptionPane.ERROR_MESSAGE);
                }           
            }else{
                pdfmgr1 = new WholesaleBill_Medical();                
                    String filename = pdfmgr1.createPdf(wholesalebill);
                if (filename != null) {
                    pdfmgr1.printPdf(filename);
                } else {
                    JOptionPane.showMessageDialog(null,"Problem in bill printing.","Error.",JOptionPane.ERROR_MESSAGE);
                }           
            }
            
         }else{
            WholesaleBillIGst_pdf pdfmgr=null;
            WholesaleBill_MedicalIGst pdfmgr1=null;
            if(Config.config_other_config.get(0).getWholesale_bill_type()==null||Config.config_other_config.get(0).getWholesale_bill_type().equals("")|| Config.config_other_config.get(0).getWholesale_bill_type().equals("false")?true:false){
                 pdfmgr = new WholesaleBillIGst_pdf();                
                 String filename = pdfmgr.createPdf(wholesalebill);
                    if (filename != null) {
                    pdfmgr.printPdf(filename);
                    } else {
                    JOptionPane.showMessageDialog(null,"Problem in bill printing.","Error.",JOptionPane.ERROR_MESSAGE);
                    }
            }else{
                 pdfmgr1 = new WholesaleBill_MedicalIGst();                
                 String filename = pdfmgr1.createPdf(wholesalebill);
                if (filename != null) {
                pdfmgr1.printPdf(filename);
                } else {
                JOptionPane.showMessageDialog(null,"Problem in bill printing.","Error.",JOptionPane.ERROR_MESSAGE);
                }
            }
            
         }
 }
}
