/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classmanager.billing;

import datamanager.billing.BillingDetails;
import datamanager.config.Config;


/**
 *
 * @author akshay
 */
public class BillingDetailsMgr {

    public boolean insBillingDetails(BillingDetails billing_details) {
        try {          
            Config.sql = "update billing_details set"
                    + " tin_no = ?, "
                    + " company_name = ?, "
                    + " tagline = ?, "
                    + " address = ?, "
                    + " contact_no = ?, "
                    + " alt_contact_no = ? ";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, billing_details.getTin_no());
            Config.pstmt.setString(2, billing_details.getCompany_name());
            Config.pstmt.setString(3, billing_details.getTagline());
            Config.pstmt.setString(4, billing_details.getAddress());
            Config.pstmt.setString(5, billing_details.getContact_no());
            Config.pstmt.setString(6, billing_details.getAlt_contact_no());
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                Config.configmgr.loadBillingDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    
    public boolean updBillingDate(BillingDetails billing_details) {
        try {          
            Config.sql = "update billing_details set"
                    + " start_date = ?, "
                    + " renew_date = ?, "
                    + " end_date = ?, "
                    + " last_date = ? ";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, billing_details.getStart_date());
            Config.pstmt.setString(2, billing_details.getRenew_date());
            Config.pstmt.setString(3, billing_details.getEnd_date());
            Config.pstmt.setString(4, billing_details.getLast_date());
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                Config.configmgr.loadBillingDetails();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
        
}

