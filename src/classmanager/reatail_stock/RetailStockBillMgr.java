/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classmanager.reatail_stock;

import datamanager.config.Config;
import datamanager.retail_stock.RetailStockBill;
import datamanager.supplier.SupplierProfile;
import java.sql.SQLException;

/**
 *
 * @author akshay
 */
public class RetailStockBillMgr {
        //method to insert complete bill in database
    public boolean insRetailStockBill(RetailStockBill retailstockbill) {
        
        try {
            //to fetch supplier id
            String supplierid = null;            
            if (retailstockbill.getSupplier_profile().getSupplierid().equals("")) {                
                SupplierProfile sp = new SupplierProfile();
                sp.setOrganization(retailstockbill.getSupplier_profile().getOrganization());
                sp.setSuppliername(retailstockbill.getSupplier_profile().getSuppliername());
                sp.setTinno(retailstockbill.getSupplier_profile().getTinno());
                
                if (Config.supplierprofilemgr.insSupplierProfile(sp)) {
                    Config.sql = "select max(supplierid) supplierid from supplierprofile";
                    Config.rs = Config.stmt.executeQuery(Config.sql);
                    while (Config.rs.next()) {
                        supplierid = Config.rs.getString("supplierid");
                    }
                }
            } else {
                supplierid = retailstockbill.getSupplier_profile().getSupplierid();
            }
        //===============================================================================================================
                    
            //to insert bill entry
            Config.sql = "insert into retail_stock_bill ("
                    + "supplierid,"
                    + "bill_no,"
                    + "date,"                        
                    + "salesman,"
                    + "purchaseamt,"
                    + "schemeamt,"
                    + "vatmat,"
                    + "discountamt,"
                    + "payableamt,"
                    + "paidamt,"
                    + "balanceamt)"
                    + "values ("
                    + "'"+supplierid+"',"
                    + "'"+retailstockbill.getBill_no()+"',"
                    + "'"+retailstockbill.getDate()+"',"
                    + "'"+retailstockbill.getSalesman()+"',"
                    + "'"+retailstockbill.getPurchaseamt()+"',"
                    + "'"+retailstockbill.getSchemeamt()+"',"
                    + "'"+retailstockbill.getVatmat()+"',"
                    + "'"+retailstockbill.getDiscountamt()+"',"
                    + "'"+retailstockbill.getPayableamt()+"',"
                    + "'"+retailstockbill.getPaidamt()+"',"
                    + "'"+retailstockbill.getBalanceamt()+"')";
            Config.stmt.addBatch(Config.sql);
        //===============================================================================================================
                        
        //===============================================================================================================
            //to insert purchase entry and return entry            
            if (Config.retail_stock_purchase_mgr.insRetailStockPurchase(retailstockbill.getRetail_stock_purchase())) {
                Config.retail_stock_mgr.updRetailStock();
                Config.configmgr.loadRetailStock();
//                Config.home.loadShortExpiry();
                Config.configmgr.setCommit();
                return true;
            } else {
                return false;
            }
        //===============================================================================================================
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
//===============================================================================================================
//===============================================================================================================
    
    
    //method to update complete bill in database
    public boolean updRetailStockBill(RetailStockBill retailstockbill) {
        try {            
            //to fetch supplier id
            String supplierid = null;            
            if (retailstockbill.getSupplier_profile().getSupplierid().equals("")) {                
                SupplierProfile sp = new SupplierProfile();
                sp.setOrganization(retailstockbill.getSupplier_profile().getOrganization());
                sp.setSuppliername(retailstockbill.getSupplier_profile().getSuppliername());
                sp.setTinno(retailstockbill.getSupplier_profile().getTinno());
                
                if (Config.supplierprofilemgr.insSupplierProfile(sp)) {
                    Config.sql = "select max(supplierid) supplierid from supplierprofile";
                    Config.rs = Config.stmt.executeQuery(Config.sql);
                    while (Config.rs.next()) {
                        supplierid = Config.rs.getString("supplierid");
                    }
                }
            } else {
                supplierid = retailstockbill.getSupplier_profile().getSupplierid();
            }
        //===============================================================================================================
            
            //to update bill entry
            Config.sql = "update retail_stock_bill set "
                    + "supplierid = '"+supplierid+"', "
                    + "bill_no = '"+retailstockbill.getBill_no()+"', "
                    + "date = '"+retailstockbill.getDate()+"', "                        
                    + "salesman = '"+retailstockbill.getSalesman()+"', "
                    + "purchaseamt = '"+retailstockbill.getPurchaseamt()+"', "
                    + "schemeamt = '"+retailstockbill.getSchemeamt()+"', "
                    + "vatmat = '"+retailstockbill.getVatmat()+"', "
                    + "discountamt = '"+retailstockbill.getDiscountamt()+"', "
                    + "payableamt = '"+retailstockbill.getPayableamt()+"', "
                    + "paidamt = '"+retailstockbill.getPaidamt()+"', "
                    + "balanceamt = '"+retailstockbill.getBalanceamt()+"'"
                    + " where retail_stock_bill_id = '" + retailstockbill.getRetail_stock_bill_id()+ "'";

            Config.stmt.addBatch(Config.sql);
        //===============================================================================================================
            
            //to update purchase entry, and return entry            
            if (Config.retail_stock_purchase_mgr.updRetailStockPurchase(retailstockbill.getRetail_stock_purchase(), retailstockbill.getRetail_stock_bill_id())) {
                Config.retail_stock_mgr.updRetailStock();
                Config.configmgr.loadRetailStock();
                Config.configmgr.setCommit();
                return true;
            } else {
                return false;
            }
        //===============================================================================================================
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }        
    }
//===============================================================================================================
//===============================================================================================================
    
    
    //method to delete complete bill in database
    public boolean delRetailStockBill(String retailstockbillid) {
        try {
            Config.sql = "delete from retail_stock_bill where retail_stock_bill_id = '" + retailstockbillid + "'";
            Config.stmt.addBatch(Config.sql);
            if (Config.retail_stock_purchase_mgr.delRetailStockPurchase(retailstockbillid) ) {
                Config.retail_stock_mgr.updRetailStock();
                Config.configmgr.loadRetailStock();
                Config.configmgr.setCommit();
                return true;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
