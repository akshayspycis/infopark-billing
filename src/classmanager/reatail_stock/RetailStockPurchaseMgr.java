/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classmanager.reatail_stock;

import datamanager.config.Config;
import datamanager.retail_stock.RetailStockPurchase;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author akshay
 */
public class RetailStockPurchaseMgr {
     
    //method to insert purchase in database
    public boolean insRetailStockPurchase(ArrayList<RetailStockPurchase> retail_stock_purchase) {
                return insertPurchase(retail_stock_purchase,null);
    }
//===============================================================================================================
//===============================================================================================================
    
    //method to insert purchase in database
    public boolean updRetailStockPurchase(ArrayList<RetailStockPurchase> retail_stock_purchase, String retail_stock_bill_id){
        try {
            Config.sql = "delete from retail_stock_purchase where retail_stock_bill_id = '" +retail_stock_bill_id+ "'";
            Config.stmt.addBatch(Config.sql);
            return insertPurchase(retail_stock_purchase,retail_stock_bill_id);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
//===============================================================================================================
//===============================================================================================================
    
    //method to delete purchase in database
    public boolean delRetailStockPurchase(String retail_stock_bill_id) {
        try {
            Config.sql = "delete from retail_stock_purchase where retail_stock_bill_id = '" + retail_stock_bill_id + "'";
            Config.stmt.addBatch(Config.sql);
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }    
//===============================================================================================================
    
    public boolean insertPurchase(ArrayList<RetailStockPurchase> retail_stock_purchase,String retail_stock_bill_id){
        
        for (int i = 0; i < retail_stock_purchase.size(); i++) {
                RetailStockPurchase wbp = retail_stock_purchase.get(i);
                if (retail_stock_bill_id==null) {
                    Config.sql = "insert into retail_stock_purchase ("
                        + "retail_stock_bill_id,"
                        + "product,"
                        + "variety,"
                        + "unit,"
                        + "mrp,"
                        + "rate,"
                        + "quantity,"
                        + "amount,"
                        + "vat,"
                        + "vatamt) "
                        + "values ((Select max(retail_stock_bill_id) retail_stock_bill_id from retail_stock_bill),"
                        + "'"+wbp.getProduct()+"',"
                        + "'"+wbp.getVariety()+"',"
                        + "'"+wbp.getUnit()+"',"
                        + "'"+wbp.getMrp()+"',"
                        + "'"+wbp.getRate()+"',"
                        + "'"+wbp.getQuantity()+"',"
                        + "'"+wbp.getAmount()+"',"
                        + "'"+wbp.getVat()+"',"
                        + "'"+wbp.getVatamt()+"')";
                } else {
                    Config.sql = "insert into retail_stock_purchase ("
                        + "retail_stock_bill_id,"
                        + "product,"
                        + "variety,"
                        + "unit,"
                        + "mrp,"
                        + "rate,"
                        + "quantity,"
                        + "amount,"
                        + "vat,"
                        + "vatamt) "
                        + "values ('"+retail_stock_bill_id+"',"
                        + "'"+wbp.getProduct()+"',"
                        + "'"+wbp.getVariety()+"',"
                        + "'"+wbp.getUnit()+"',"
                        + "'"+wbp.getMrp()+"',"
                        + "'"+wbp.getRate()+"',"
                        + "'"+wbp.getQuantity()+"',"
                        + "'"+wbp.getAmount()+"',"
                        + "'"+wbp.getVat()+"',"
                        + "'"+wbp.getVatamt()+"')";
                }
                
            try {
                Config.stmt.addBatch(Config.sql);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
            }
        return true;
    }
//===============================================================================================================
}
