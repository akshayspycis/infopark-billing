/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classmanager.reatail_stock;

import datamanager.config.Config;
import datamanager.retail.RetailBillPurchase;
import datamanager.retail_stock.RetailStock;
import java.sql.SQLException;

/**
 *
 * @author akshay
 */
public class RetailStockMgr {
   //method to update stock
    public boolean updRetailStock() {
        try {
            Config.sql = "truncate retail_stock";
            Config.stmt.addBatch(Config.sql);
            Config.sql = "insert into retail_stock (product,variety,unit,mrp) select product, variety,unit,mrp from retail_stock_purchase group by product,variety,unit,mrp";
            Config.stmt.addBatch(Config.sql);
            Config.sql = "update retail_stock u,retail_stock s\n" +
                         "set u.quantity =\n" +
                         "(select COALESCE(sum(quantity),0) from retail_stock_purchase sp\n" +
                         "where sp.product =s.product and sp.variety =s.variety\n" +
                         "and sp.unit = s.unit and sp.mrp = s.mrp )\n" +
                         "-(select COALESCE(sum(quantity),0) from retailbill_purchase sp\n" +
                         "where sp.product =s.product and sp.variety =s.variety\n" +
                         "and sp.unit = s.unit and sp.mrp = s.mrp )\n" +
                         "where u.retail_stock_id=s.retail_stock_id";
            Config.stmt.addBatch(Config.sql);
            Config.stmt.executeBatch();
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    } 
//===============================================================================================================
    public boolean subStockArray (RetailBillPurchase rbp){
        for (int i = 0; i < Config.config_retail_stock.size(); i++) {
            RetailStock s=  Config.config_retail_stock.get(i);
            if(s.getProduct().equals(rbp.getProduct()) && s.getVariety().equals(rbp.getVariety()) && s.getUnit().equals(rbp.getUnit()) && s.getMrp().equals(rbp.getMrp()) ){
                int total=Integer.parseInt(s.getQuantity())-Integer.parseInt(rbp.getQuantity());
                s.setQuantity(String.valueOf(total));
                Config.config_retail_stock.set(i, s);
            }
        }
        return true;
    }
}
