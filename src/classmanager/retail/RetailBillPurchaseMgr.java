/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classmanager.retail;

import datamanager.config.Config;
import datamanager.retail.RetailBillPurchase;

import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author akshay
 */
public class RetailBillPurchaseMgr {
     //method to insert purchase in database
    public boolean insRetailBillPurchase(ArrayList<RetailBillPurchase> retail_bill_purchase) {
                return insertPurchase(retail_bill_purchase,null);
    }
//===============================================================================================================
//===============================================================================================================
    
    //method to insert purchase in database
    public boolean updRetailBillPurchase(ArrayList<RetailBillPurchase> retail_bill_purchase, String retailbillid){
        try {
            Config.sql = "delete from retailbill_purchase where retailbillid = '" +retailbillid+ "'";
            Config.stmt.addBatch(Config.sql);
            return insertPurchase(retail_bill_purchase,retailbillid);
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
//===============================================================================================================
//===============================================================================================================
    
    //method to delete purchase in database
    public boolean delRetailBillPurchase(String retailbillid) {
        try {
            Config.sql = "delete from retailbill_purchase where retailbillid = '" + retailbillid + "'";
            Config.stmt.addBatch(Config.sql);
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }    
//===============================================================================================================
    
    public boolean insertPurchase(ArrayList<RetailBillPurchase> retail_bill_purchase,String retailbillid){
        for (int i = 0; i < retail_bill_purchase.size(); i++) {
                RetailBillPurchase wbp = retail_bill_purchase.get(i);
                if (retailbillid==null) {
                    Config.sql = "insert into retailbill_purchase ("
                        + "retailbillid,"
                        + "product,"
                        + "variety,"
                        + "unit,"
                        + "mrp,"
                        + "quantity,"
                        + "amount)"
                        + "values ((Select max(retailbillid) retailbillid from retailbill),"
                        + "'"+wbp.getProduct()+"',"
                        + "'"+wbp.getVariety()+"',"
                        + "'"+wbp.getUnit()+"',"
                        + "'"+wbp.getMrp()+"',"
                        + "'"+wbp.getQuantity()+"',"
                        + "'"+wbp.getAmount()+"')";
                } else {
                    Config.sql = "insert into retailbill_purchase ("
                        + "retailbillid,"
                        + "product,"
                        + "variety,"
                        + "unit,"
                        + "mrp,"
                        + "quantity,"
                        + "amount)"
                        + "values ('"+retailbillid+"',"
                        + "'"+wbp.getProduct()+"',"
                        + "'"+wbp.getVariety()+"',"
                        + "'"+wbp.getUnit()+"',"
                        + "'"+wbp.getMrp()+"',"
                        + "'"+wbp.getQuantity()+"',"
                        + "'"+wbp.getAmount()+"')";
                }
                
            try {
                Config.stmt.addBatch(Config.sql);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
                Config.retail_stock_mgr.subStockArray(wbp);
            }
        return true;
    }
//===============================================================================================================
}
