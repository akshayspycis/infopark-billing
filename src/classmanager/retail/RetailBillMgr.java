/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classmanager.retail;

import datamanager.config.Config;
import datamanager.customer.CustomerProfile;
import datamanager.retail.RetailBill;
import java.sql.SQLException;

/**
 *
 * @author akshay
 */
public class RetailBillMgr {
    
    //method to insert complete bill in database
    public boolean insRetailBill(RetailBill retailbill) {        
        try {
            //to fetch customer id
            String customerid = null;
            
            if (retailbill.getCustomerprofile().getCustomerid().equals("")) {
                CustomerProfile cp = new CustomerProfile();
                cp.setOrganization(retailbill.getCustomerprofile().getOrganization());
                cp.setCustomername(retailbill.getCustomerprofile().getCustomername());
                cp.setTinno(retailbill.getCustomerprofile().getTinno());
                cp.setLocality(retailbill.getCustomerprofile().getLocality());
                
                if (Config.customerprofilemgr.insCustomerProfile(cp)) {
                    Config.sql = "select max(customerid) customerid from customerprofile";
                    Config.rs = Config.stmt.executeQuery(Config.sql);
                    while (Config.rs.next()) {
                        customerid = Config.rs.getString("customerid");
                    }
                }
                } else {
                    customerid = retailbill.getCustomerprofile().getCustomerid();
                }
        //===============================================================================================================
            //to insert bill entry
            Config.conn.setAutoCommit(false);
            Config.sql = "insert into retailbill ("
                    + "customerid,"
                    + "billno,"
                    + "date,"
                    + "purchaseamt,"
                    + "discount,"
                    + "payableamt,"
                    + "paidamt,"
                    + "balanceamt,"
                    + "vatamt,"
                    + "salesman)"
                    + "values ('"+customerid+"',"
                    + "'"+retailbill.getBillno()+"',"
                    + "'"+retailbill.getDate()+"',"
                    + "'"+retailbill.getPurchaseamt()+"',"
                    + "'"+retailbill.getDiscount()+"',"
                    + "'"+retailbill.getPayableamt()+"',"
                    + "'"+retailbill.getPaidamt()+"',"
                    + "'"+retailbill.getBalanceamt()+"',"
                    + "'"+retailbill.getVatamt()+"',"
                    + "'"+retailbill.getSalesman()+"')";
            Config.stmt.addBatch(Config.sql);
            
        //===============================================================================================================
            
            //to insert purchase entry and return entry            
            if (Config.retailbillpurchasemgr.insRetailBillPurchase(retailbill.getRetailbillpurchase()) ) {
                Config.stmt.executeBatch();
                Config.configmgr.setCommit();
                Config.retail_stock_mgr.updRetailStock();
                Config.configmgr.loadRetailStock();
                return true;
            } else {
                return false;
            }
        //===============================================================================================================
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
//===============================================================================================================
//===============================================================================================================

    //method to update complete bill in database    
    public boolean updRetailBill(RetailBill retailbill) {
        
        try {
            //to fetch customer id
            String customerid = null;
            
            if (retailbill.getCustomerprofile().getCustomerid().equals("")) {
                
                CustomerProfile cp = new CustomerProfile();
                cp.setOrganization(retailbill.getCustomerprofile().getOrganization());
                cp.setCustomername(retailbill.getCustomerprofile().getCustomername());
                cp.setTinno(retailbill.getCustomerprofile().getTinno());
                cp.setLocality(retailbill.getCustomerprofile().getLocality());
                
                if (Config.customerprofilemgr.insCustomerProfile(cp)) {
                    Config.sql = "select max(customerid) customerid from customerprofile";
                    Config.rs = Config.stmt.executeQuery(Config.sql);
                    while (Config.rs.next()) {
                        customerid = Config.rs.getString("customerid");
                    }
                }
            } else {
                customerid = retailbill.getCustomerprofile().getCustomerid();
            }
        //===============================================================================================================
            
            //to update bill entry
            Config.sql = "update retailbill set "
                    + "customerid = '"+customerid+"', "
                    + "billno = '"+retailbill.getBillno()+"', "
                    + "date = '"+retailbill.getDate()+"', "                        
                    + "purchaseamt = '"+retailbill.getPurchaseamt()+"', "
                    + "discount = '"+retailbill.getDiscount()+"', "
                    + "payableamt = '"+retailbill.getPayableamt()+"', "
                    + "paidamt = '"+retailbill.getPaidamt()+"', "
                    + "balanceamt = '"+retailbill.getBalanceamt()+"', "
                    + "vatamt = '"+retailbill.getVatamt()+"', "
                    + "salesman = '"+retailbill.getSalesman()+"' "
                    + "where retailbillid = '" + retailbill.getRetailbillid()+ "'";
            Config.stmt.addBatch(Config.sql);
        //===============================================================================================================
            
            //to update purchase entry, and return entry
            if (Config.retailbillpurchasemgr.updRetailBillPurchase(retailbill.getRetailbillpurchase(), retailbill.getRetailbillid())) {
                Config.stmt.executeBatch();
                Config.configmgr.setCommit();
                Config.retail_stock_mgr.updRetailStock();
                Config.configmgr.loadRetailStock();
                return true;
            } else {
                return false;
            }
        //===============================================================================================================            
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }        
    }
//===============================================================================================================
//===============================================================================================================

    
    //method to delete complete bill in database
    public boolean delRetailBill(String retailbillid) {
        try {
            Config.sql = "delete from retailbill where retailbillid = '" + retailbillid + "'";
            Config.stmt.addBatch(Config.sql);
            if ( Config.retailbillpurchasemgr.delRetailBillPurchase(retailbillid) && Config.retailbillreturnmgr.delRetailBillReturn(retailbillid)&&Config.retail_stock_mgr.updRetailStock()) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
