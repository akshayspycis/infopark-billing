/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classmanager.retail;

import datamanager.config.Config;
import datamanager.retail.RetailBillReturn;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author akshay
 */
public class RetailBillReturnMgr {
 //method to insert purchase in database
    public boolean insRetailBillReturn(ArrayList<RetailBillReturn> retailbillreturn, String retailbillid) {
        
        try {
                Config.sql = "insert into retailbillreturn ("
                        + "retailbillid,"
                        + "rproduct,"
                        + "rvariety,"
                        + "contentrate,"
                        + "quantity,"
                        + "amount,"
                        + "vat,"
                        + "vatamt)"
                        + "values (?,?,?,?,?,?,?,?)";
                
                Config.pstmt = Config.conn.prepareStatement(Config.sql);
            for (int i = 0; i < retailbillreturn.size(); i++) {
                RetailBillReturn wbp = retailbillreturn.get(i);
                Config.pstmt.setString(1, retailbillid);
                Config.pstmt.setString(2, wbp.getRproduct());
                Config.pstmt.setString(3, wbp.getRvariety());
                Config.pstmt.setString(4, wbp.getContentrate());
                Config.pstmt.setString(5, wbp.getQuantity());
                Config.pstmt.setString(6, wbp.getAmount());
                Config.pstmt.setString(7, wbp.getVat());
                Config.pstmt.setString(8, wbp.getVatamt());
                Config.pstmt.addBatch();
            }
            int[]arr=Config.pstmt.executeBatch();
            Config.pstmt.close();
            if(arr.length==retailbillreturn.size()) {
                return true;
            } else {
                return false;
            } 
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        
    }
//===============================================================================================================
//===============================================================================================================
    
    //method to insert purchase in database
    public boolean updRetailBillReturn(ArrayList<RetailBillReturn> retailbillreturn, String retailbillid) {
        
        try {
            Config.sql = "delete from retailbillreturn where retailbillid = '" +retailbillid+ "'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.executeUpdate();
            
            
                
                Config.sql = "insert into retailbillreturn ("
                        + "retailbillid,"
                        + "rproduct,"
                        + "rvariety,"
                        + "contentrate,"
                        + "quantity,"
                        + "amount,"
                        + "vat,"
                        + "vatamt)"
                        + "values (?,?,?,?,?,?,?,?)";
                
                Config.pstmt = Config.conn.prepareStatement(Config.sql);
            for (int i = 0; i < retailbillreturn.size(); i++) {
                RetailBillReturn wbp = retailbillreturn.get(i);
                Config.pstmt.setString(1, retailbillid);
                Config.pstmt.setString(2, wbp.getRproduct());
                Config.pstmt.setString(3, wbp.getRvariety());
                Config.pstmt.setString(4, wbp.getContentrate());
                Config.pstmt.setString(5, wbp.getQuantity());
                Config.pstmt.setString(6, wbp.getAmount());
                Config.pstmt.setString(7, wbp.getVat());
                Config.pstmt.setString(8, wbp.getVatamt());
                Config.pstmt.addBatch();
            }
            int[]arr=Config.pstmt.executeBatch();
            System.out.println(arr.length);
            Config.pstmt.close();
            if(arr.length==retailbillreturn.size()) {
                return true;
            } else {
                return false;
            } 
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        
    }
//===============================================================================================================
//===============================================================================================================
    
    //method to delete purchase in database
    public boolean delRetailBillReturn(String retailbillid) {
        try {
            Config.sql = "delete from retailbillreturn where retailbillid = '" + retailbillid + "'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int y = Config.pstmt.executeUpdate();
            
            if (y >= 0) {
                return true;
            } else {
                return false;
            } 
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }    
//===============================================================================================================
//===============================================================================================================        
}
