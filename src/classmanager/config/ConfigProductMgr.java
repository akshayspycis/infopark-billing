package classmanager.config;

import datamanager.config.Config;
import datamanager.config.ConfigProduct;

public class ConfigProductMgr {    
  
    public boolean insProduct(ConfigProduct[] product) {
        try {   
            Config.sql = "Delete from configproduct";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int x = Config.pstmt.executeUpdate();
            
            int i = 0;
            if (x>=0) {
                for (i = 0; i < product.length; i++) {
                    Config.stmt.addBatch("insert into configproduct values ('"+product[i].getProductname()+"','"+product[i].getHsn_code()+"','"+product[i].getBar_code()+"','"+product[i].getDiscount()+"')");
                }
                Config.stmt.executeBatch();
            }
            
            if (i == product.length) {
                Config.configmgr.loadProduct();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }        
}
