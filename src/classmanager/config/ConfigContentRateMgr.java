package classmanager.config;

import datamanager.config.Config;
import datamanager.config.ConfigContentRate;

public class ConfigContentRateMgr {

    public boolean insContentRate(ConfigContentRate[] contentrate) {
        try {   
            Config.sql = "Delete from configcontentrate";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int x = Config.pstmt.executeUpdate();
            
            int i = 0;
            if (x>=0) {
                for (i = 0; i < contentrate.length; i++) {
                    Config.stmt.addBatch("insert into configcontentrate values ('"+contentrate[i].getContentrate()+"')");
                }
                Config.stmt.executeBatch();
            }
            
            if (i == contentrate.length) {
                Config.configmgr.loadContentRate();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }        
    
}
