package classmanager.config;

import datamanager.config.Config;
import datamanager.config.ConfigScheme;

public class ConfigSchemeMgr {
        
    public boolean insScheme(ConfigScheme[] scheme) {
        try {   
            Config.sql = "Delete from configscheme";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int x = Config.pstmt.executeUpdate();
            
            int i = 0;
            if (x>=0) {
                for (i = 0; i < scheme.length; i++) {
                    Config.stmt.addBatch("insert into configscheme (amount,percentage) values ('"+scheme[i].getAmount()+"', '"+scheme[i].getPercentage()+"')");
                }
                Config.stmt.executeBatch();
            }
            
            if (i == scheme.length) {
                Config.configmgr.loadScheme();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
    
    

