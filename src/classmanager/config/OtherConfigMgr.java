/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package classmanager.config;

import datamanager.config.Config;
import datamanager.config.OtherConfig;

/**
 *
 * @author akshay
 */
public class OtherConfigMgr {
    
    //method to insert otherconfig in database.
    public boolean insOtherConfig(OtherConfig otherconfig) {
        try {          
            Config.sql = "delete from otherconfig ";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int x = Config.pstmt.executeUpdate();
            if (x>0) {
                    try {          
                        Config.sql = "insert into otherconfig ("                  
                        + "wholesale_stock_vat,"
                        + "retail_stock_vat,"
                        + "wholesale_discount,"
                        + "retail_discount,"
                        + "retail_billing_stock,"
                        + "wholesale_billing_stock,"
                        + "pos_58,"
                        + "pos_80,"
                        + "pos_a4,"
                        + "wholesale_bill_type)"
                        + "values (?,?,?,?,?,?,?,?,?,?)";
                        Config.pstmt = Config.conn.prepareStatement(Config.sql);
                        Config.pstmt.setString(1, otherconfig.getWholesale_stock_vat());
                        Config.pstmt.setString(2, otherconfig.getRetail_stock_vat());
                        Config.pstmt.setString(3, otherconfig.getWholesale_stock_vat());
                        Config.pstmt.setString(4, otherconfig.getRetail_discount());
                        Config.pstmt.setString(5, otherconfig.getRetail_billing_stock());
                        Config.pstmt.setString(6, otherconfig.getWholesale_billing_stock());
                        Config.pstmt.setString(7, otherconfig.getPos_58());
                        Config.pstmt.setString(8, otherconfig.getPos_80());
                        Config.pstmt.setString(9, otherconfig.getPos_a4());
                        Config.pstmt.setString(10, otherconfig.getWholesale_bill_type());
                        int y = Config.pstmt.executeUpdate();
                        if (y>0) {
                            Config.configmgr.loadOtherConfig();
                            return true;
                        } else {
                            return false;
                        }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                            return false;
                        }
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }    
    }
    //----------------------------------------------------------------------------------------------
    

}
