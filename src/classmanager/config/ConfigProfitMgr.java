package classmanager.config;

import datamanager.config.Config;
import datamanager.config.ConfigProfit;

public class ConfigProfitMgr {
    
    public boolean insProfit(ConfigProfit[] profit) {
        try {   
            Config.sql = "Delete from configprofit";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int x = Config.pstmt.executeUpdate();
            
            int i = 0;
            if (x>=0) {
                for (i = 0; i < profit.length; i++) {
                    Config.stmt.addBatch("insert into configprofit (product,flavor,profit) values ('"+profit[i].getProduct()+"', '"+profit[i].getFlavor()+"', '"+profit[i].getProfit()+"')");
                }
                Config.stmt.executeBatch();
            }
            
            if (i == profit.length) {
                Config.configmgr.loadProfit();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
