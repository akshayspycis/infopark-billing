package classmanager.config;

import classmanager.barcode_reader.barcode.ReadMgr;
import classmanager.billing.BillingDetailsMgr;
import classmanager.customer.CustomerProfileMgr;
import classmanager.payroll.EmployeeProfileMgr;
import classmanager.payroll.PayrollMgr;
import classmanager.reatail_stock.RetailStockBillMgr;
import classmanager.reatail_stock.RetailStockMgr;
import classmanager.reatail_stock.RetailStockPurchaseMgr;
import classmanager.retail.RetailBillMgr;
import classmanager.retail.RetailBillPurchaseMgr;
import classmanager.retail.RetailBillReturnMgr;
import classmanager.stock.StockBillMgr;
import classmanager.stock.StockBillPurchaseMgr;
import classmanager.stock.StockBillReturnMgr;
import classmanager.stock.StockMgr;
import classmanager.stock.TempPriceDetailsMgr;
import classmanager.supplier.SupplierProfileMgr;
import classmanager.wholesale.WholesaleBillMgr;
import classmanager.wholesale.WholesaleBillPurchaseMgr;
import classmanager.wholesale.WholesaleBillReturnMgr;
import datamanager.billing.BillingDetails;
import datamanager.config.Config;
import datamanager.config.ConfigContentRate;
import datamanager.config.ConfigFlavor;
import datamanager.config.ConfigProduct;
import datamanager.config.ConfigProfit;
import datamanager.config.ConfigRProduct;
import datamanager.config.ConfigRVariety;
import datamanager.config.ConfigScheme;
import datamanager.config.ConfigUserProfile;
import datamanager.config.OtherConfig;
import datamanager.config.RetailGst;
import datamanager.config.WholesaleGst;
import datamanager.customer.CustomerProfile;
import datamanager.payroll.EmployeeProfile;
import datamanager.retail_stock.RetailStock;
import datamanager.stock.Stock;
import datamanager.stock.TempPriceDetails;
import datamanager.supplier.SupplierProfile;
import datamanager.wholesale.WholesaleBill;
import datamanager.wholesale.WholesaleBillPurchase;
import datamanager.wholesale.WholesaleBillReturn;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import modules.account.Login;
import modules.account.ManageAccount;
import modules.config.loading.Loading;
import modules.configuration.ProductConfiguration;
import modules.customer.CustomerManagement;
import modules.customer.NewCustomer;
import modules.customer.ViewCustomer;
import modules.home.Home;
import modules.payroll.NewEmployee;
import modules.payroll.PayrollManagement;
import modules.payroll.ViewEmployee;
import modules.productlicense.ProductLicense;
import modules.retail_stock.RetailStockEntry;
import modules.retail_stock.RetailStockManagement;
import modules.retail_stock.RetailStockNewBill;
import modules.retail_stock.RetailStockViewBill;
import modules.retail_stock.SearchTableRetailStock;
import modules.retailsbill.BatchShow;
import modules.retailsbill.ItemSearchTable;
import modules.retailsbill.RetailBillEntry;
import modules.retailsbill.RetailBillEntryMedical;
import modules.retailsbill.RetailBillSummary;
import modules.retailsbill.RetailManagement;
import modules.retailsbill.RetailNewBill;
import modules.retailsbill.RetailSearchTable;
import modules.retailsbill.RetailViewBill;
import modules.search.HomeSearch;
import modules.search.Search;
import modules.stock.SearchTableStock;
import modules.stock.StockEntry;
import modules.stock.StockManagement;
import modules.stock.StockNewBill;
import modules.stock.StockPendingBill;
import modules.stock.StockViewBill;
import modules.supplier.NewSupplier;
import modules.supplier.SupplierManagement;
import modules.supplier.ViewSupplier;
import modules.wholesalebill.BillSummary;
import modules.wholesalebill.FreeSummary;
import modules.wholesalebill.SchemeSummary;
import modules.wholesalebill.SearchEmployee;
import modules.wholesalebill.SearchTable;
import modules.wholesalebill.StockShow;
import modules.wholesalebill.WholeSaleBillSummary;
import modules.wholesalebill.WholeSalePaid;
import modules.wholesalebill.WholesaleBillEntry;
import modules.wholesalebill.WholesaleNewBill;
import modules.wholesalebill.WholesalePendingBill;
import modules.wholesalebill.WholesaleViewBill;

public class ConfigMgr {    

    public boolean loadDatabase() {
        try {
            DriverManager.registerDriver(new com.mysql.jdbc.Driver());
            Properties prop = loadPropertiesFile();
            String url = prop.getProperty("MYSQLJDBC.url");
            String userName = prop.getProperty("MYSQLJDBC.username");
            String password = prop.getProperty("MYSQLJDBC.password");
            Config.conn = DriverManager.getConnection(url, userName, password);
            Config.stmt = Config.conn.createStatement();            
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    public static Properties loadPropertiesFile() throws Exception {
		Properties prop = new Properties();
		InputStream in = new FileInputStream("jdbc.properties");
		prop.load(in);
		in.close();
		return prop;
	}
    public void setCommit() {
        try {
            Config.sql = "commit";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(Home.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public boolean loadUserProfile() {
        Config.configuserprofile = new ArrayList<ConfigUserProfile>();
        try {
            Config.sql =    "select * from configuserprofile";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                ConfigUserProfile up = new ConfigUserProfile();
                up.setUserid(Config.rs.getString("userid"));
                up.setUsername(Config.rs.getString("username"));
                up.setPassword(Config.rs.getString("password"));
                up.setName(Config.rs.getString("name"));
                up.setContactno(Config.rs.getString("contactno"));
                up.setSequrityquestion(Config.rs.getString("securityquestion"));
                up.setAnswer(Config.rs.getString("answer"));                
                
                Config.configuserprofile.add(up);
            }                
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    
    public boolean loadBillingDetails() {
        Config.billing_details = new ArrayList<BillingDetails>();
        try {
            Config.sql = "select * from billing_details";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                BillingDetails up = new BillingDetails();
                up.setTin_no(Config.rs.getString("tin_no"));
                up.setContact_no(Config.rs.getString("contact_no"));
                up.setAlt_contact_no(Config.rs.getString("alt_contact_no"));
                up.setCompany_name(Config.rs.getString("company_name"));
                up.setTagline(Config.rs.getString("tagline"));
                up.setAddress(Config.rs.getString("address"));
//                up.setStart_date(Config.rs.getString("start_date"));
//                up.setRenew_date(Config.rs.getString("renew_date"));
//                up.setEnd_date(Config.rs.getString("end_date"));
//                up.setLast_date(Config.rs.getString("last_date"));
                Config.billing_details.add(up);
            }                
        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, ex.getMessage(), "", JOptionPane.ERROR_MESSAGE);
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    
    public boolean loadProduct() {
        Config.configproduct = new ArrayList<ConfigProduct>();
        try {
            Config.sql = "select * from configproduct";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                ConfigProduct p = new ConfigProduct();                
                p.setProductname(Config.rs.getString("productname"));     
                p.setHsn_code(Config.rs.getString("hsn_code"));     
                p.setBar_code(Config.rs.getString("bar_code"));     
                p.setDiscount(Config.rs.getString("discount"));     
                Config.configproduct.add(p);
            }                
        } catch (SQLException ex) {
            try {
                Config.stmt.executeUpdate("ALTER TABLE configproduct  "
                    + "ADD COLUMN hsn_code VARCHAR(200) AFTER productname,\n" 
                    +" ADD COLUMN bar_code VARCHAR(200) AFTER hsn_code,\n" 
                    +" ADD COLUMN discount VARCHAR(200) AFTER bar_code");
                loadProduct();
            } catch (SQLException e) {
//                ex.printStackTrace();
                return false;
            } 
        }
        return true;
    }
    
    public boolean loadWholesaleGst() {
        Config.wholesale_gst = new ArrayList<WholesaleGst>();
        try {
            Config.sql = "select * from wholesale_gst";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                WholesaleGst p = new WholesaleGst();                
                p.setProduct(Config.rs.getString("product"));     
                p.setMin(Config.rs.getString("min"));     
                p.setGst(Config.rs.getString("gst"));     
                Config.wholesale_gst.add(p);
            }                
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    
    public boolean loadRetailGst() {
        Config.retail_gst = new ArrayList<RetailGst>();
        try {
            Config.sql = "select * from retail_gst";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                RetailGst p = new RetailGst();                
                p.setProduct(Config.rs.getString("product"));     
                p.setMin(Config.rs.getString("min"));     
                p.setGst(Config.rs.getString("gst"));     
                Config.retail_gst.add(p);
            }                
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    
        public boolean loadRProduct() {
        Config.configrproduct = new ArrayList<ConfigRProduct>();
        try {
            Config.sql = "select * from configrproduct";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                ConfigRProduct p = new ConfigRProduct();                
                p.setProductname(Config.rs.getString("productname"));     
                
                Config.configrproduct.add(p);
            }                
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }

    public boolean loadFlavor() {
        Config.configflavor = new ArrayList<ConfigFlavor>();
        try {
            Config.sql = "select * from configflavor";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                
                ConfigFlavor f = new ConfigFlavor();
                f.setFlavorname(Config.rs.getString("flavorname"));
                f.setProductname(Config.rs.getString("productname"));
                
                Config.configflavor.add(f);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    
    public boolean loadRVariety() {
        Config.configrvariety = new ArrayList<ConfigRVariety>();
        try {
            Config.sql = "select * from configrvariety";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                
                ConfigRVariety f = new ConfigRVariety();
                f.setVarietyname(Config.rs.getString("varietyname"));
                f.setRate(Config.rs.getString("rate"));
                f.setRproductname(Config.rs.getString("rproductname"));
                f.setBarcode(Config.rs.getString("barcode"));
                f.setUnit(Config.rs.getString("unit"));
                f.setDiscount(Config.rs.getString("discount"));
                
                Config.configrvariety.add(f);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    public boolean loadStock() {
        Config.config_stock = new ArrayList<Stock>();
        try {
            Config.sql = "select * from stock";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                Stock f = new Stock();
                f.setStockid(Config.rs.getString("stockid"));
                f.setProduct(Config.rs.getString("product"));
                f.setFlavor(Config.rs.getString("flavor"));
                f.setContentrate(Config.rs.getString("contentrate"));
                f.setBatchno(Config.rs.getString("batchno"));
                f.setExpdate(Config.rs.getString("expdate"));
                f.setUnit(Config.rs.getString("unit"));
                f.setPack(Config.rs.getString("pack"));
                f.setMrp(Config.rs.getString("mrp"));
                f.setRate(Config.rs.getString("rate"));
                f.setVatpercentage(Config.rs.getString("vatpercentage"));
                f.setAvailable(Config.rs.getString("available"));
                Config.config_stock.add(f);
//                System.out.println(Config.rs.getString("product")+" " +Config.rs.getString("flavor")+Config.rs.getString("available"));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    
    public boolean loadTempPriceDetails() {
        Config.temp_price_details = new ArrayList<TempPriceDetails>();
        try {
            Config.sql = "select * from temp_price_details";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                TempPriceDetails f = new TempPriceDetails();
                f.setProduct(Config.rs.getString("product"));
                f.setFlavor(Config.rs.getString("flavor"));
                f.setContentrate(Config.rs.getString("contentrate"));
                f.setBatchno(Config.rs.getString("batchno"));
                f.setExpdate(Config.rs.getString("expdate"));
                f.setUnit(Config.rs.getString("unit"));
                f.setMrp(Config.rs.getString("mrp"));
                f.setRate(Config.rs.getString("rate"));
                Config.temp_price_details.add(f);
            }
        } catch (Exception ex) {
            if(createTable("CREATE TABLE temp_price_details ( "+
                        "product VARCHAR(200),\n" +
                        "flavor VARCHAR(200),\n" +
                        "contentrate VARCHAR(200),\n" +
                        "batchno VARCHAR(200),\n" +
                        "expdate VARCHAR(200),\n" +
                        "unit VARCHAR(200),\n" +
                        "mrp VARCHAR(200),\n" +
                        "rate VARCHAR(200)\n" +
                        ")\n" +
                        "ENGINE = InnoDB")){
                loadTempPriceDetails();
            }else{
                System.out.println("hi");
            }
                        
        }
        return true;
    }
    
    public boolean loadRetailStock() {
        Config.config_retail_stock = new ArrayList<RetailStock>();
        try {
            Config.sql = "select * from retail_stock";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                RetailStock f = new RetailStock();
                f.setRetail_stock_id(Config.rs.getString("retail_stock_id"));
                f.setProduct(Config.rs.getString("product"));
                f.setVariety(Config.rs.getString("variety"));
                f.setUnit(Config.rs.getString("unit"));
                f.setMrp(Config.rs.getString("mrp"));
                f.setQuantity(Config.rs.getString("quantity"));
                Config.config_retail_stock.add(f);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    
    public boolean loadContentRate() {
        Config.configcontentrate = new ArrayList<ConfigContentRate>();
        try {
            Config.sql = "select * from configcontentrate";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                ConfigContentRate cr = new ConfigContentRate();
                cr.setContentrate(Config.rs.getString("contentrate"));
                
                Config.configcontentrate.add(cr);
            }                
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    public boolean loadOtherConfig() {
        Config.config_other_config = new ArrayList<OtherConfig>();
        try {
            Config.sql = "select * from otherconfig";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                OtherConfig cr = new OtherConfig();
                cr.setWholesale_stock_vat(Config.rs.getString("wholesale_stock_vat"));
                cr.setRetail_stock_vat(Config.rs.getString("retail_stock_vat"));
                cr.setWholesale_discount(Config.rs.getString("wholesale_discount"));
                cr.setRetail_discount(Config.rs.getString("retail_discount"));
                cr.setRetail_billing_stock(Config.rs.getString("retail_billing_stock"));
                cr.setWholesale_billing_stock(Config.rs.getString("wholesale_billing_stock"));
                cr.setPos_58(Config.rs.getString("pos_58"));
                cr.setPos_80(Config.rs.getString("pos_80"));
                cr.setPos_a4(Config.rs.getString("pos_a4"));
                cr.setWholesale_bill_type(Config.rs.getString("wholesale_bill_type"));
                Config.config_other_config.add(cr);
            }                
        } catch (SQLException ex) {
            try {
                Config.stmt.executeUpdate("ALTER TABLE otherconfig"
                    +" ADD COLUMN wholesale_bill_type VARCHAR(200) AFTER pos_a4");
                loadOtherConfig();
            } catch (SQLException e) {
//                ex.printStackTrace();
                return false;
            } 
        }
        return true;
    }
    
    public boolean loadScheme() {
        Config.configscheme  = new ArrayList<ConfigScheme>();
        try {
            Config.sql = "select * from configscheme ";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                ConfigScheme s = new ConfigScheme();                
                s.setAmount(Config.rs.getString("amount"));     
                s.setPercentage(Config.rs.getString("percentage"));     

                Config.configscheme.add(s);
            }                
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    
    public boolean loadProfit() {
        Config.configprofit  = new ArrayList<ConfigProfit>();
        try {
            Config.sql = "select * from configprofit ";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                ConfigProfit p = new ConfigProfit();                
                p.setProduct(Config.rs.getString("product"));     
                p.setFlavor(Config.rs.getString("flavor"));     
                p.setProfit(Config.rs.getString("profit"));                

                Config.configprofit.add(p);
            }                
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    
    public boolean loadCustomerProfile() {
        Config.customerprofile = new ArrayList<CustomerProfile>();
        Config.customer = new ArrayList<String>();
        try {            
            Config.sql = "select * from customerprofile";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {                    
                
                CustomerProfile cp = new CustomerProfile();
                Config.customer.add(Config.rs.getString("customerid"));
                cp.setCustomerid(Config.rs.getString("customerid"));                
                cp.setCustomername(Config.rs.getString("customername"));
                cp.setGender(Config.rs.getString("gender"));
                cp.setContactno(Config.rs.getString("contactno"));
                cp.setOrganization(Config.rs.getString("organization"));
                cp.setTinno(Config.rs.getString("tinno"));
                cp.setAddress(Config.rs.getString("address"));
                cp.setLocality(Config.rs.getString("locality"));
                cp.setCity(Config.rs.getString("city"));
                cp.setPincode(Config.rs.getString("pincode"));
                cp.setState(Config.rs.getString("state"));
                cp.setEmail(Config.rs.getString("email"));
                cp.setFax(Config.rs.getString("fax"));
                cp.setWebsite(Config.rs.getString("website"));
                cp.setOther(Config.rs.getString("other"));                
                
                Config.customerprofile.add(cp);                
            }                
        } catch (SQLException ex) {
          ex.printStackTrace();
            return false;
        }
        return true;
    }
    
    public boolean loadSupplierProfile() {
        Config.supplierprofile = new ArrayList<SupplierProfile>();
        try {            
            Config.sql = "select * from supplierprofile";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {                    
                
                SupplierProfile cp = new SupplierProfile();
                cp.setSupplierid(Config.rs.getString("supplierid"));                
                cp.setSuppliername(Config.rs.getString("suppliername"));
                cp.setGender(Config.rs.getString("gender"));
                cp.setContactno(Config.rs.getString("contactno"));
                cp.setOrganization(Config.rs.getString("organization"));
                cp.setTinno(Config.rs.getString("tinno"));
                cp.setAddress(Config.rs.getString("address"));
                cp.setLocality(Config.rs.getString("locality"));
                cp.setCity(Config.rs.getString("city"));
                cp.setState(Config.rs.getString("state"));
                cp.setPincode(Config.rs.getString("pincode"));
                cp.setEmail(Config.rs.getString("email"));
                cp.setFax(Config.rs.getString("fax"));
                cp.setWebsite(Config.rs.getString("website"));
                cp.setOther(Config.rs.getString("other"));
                
                Config.supplierprofile.add(cp);                
            }                
        } catch (SQLException ex) {
          ex.printStackTrace();
            return false;
        }
        return true;
    }
    
    public boolean loadEmployeeProfile() {
        Config.employeeprofile = new ArrayList<EmployeeProfile>();
        try {            
            Config.sql = "select * from employeeprofile";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                
                EmployeeProfile ep = new EmployeeProfile();
                ep.setEmployeeid(Config.rs.getString("employeeid"));
                ep.setEmployeename(Config.rs.getString("employeename"));
                ep.setGender(Config.rs.getString("gender"));
                ep.setDob(Config.rs.getString("dob"));
                ep.setAge(Config.rs.getString("age"));
                ep.setFathername(Config.rs.getString("fathername"));
                ep.setMaritalstatus(Config.rs.getString("maritalstatus"));
                ep.setIdentitytype(Config.rs.getString("identitytype"));
                ep.setIdentityno(Config.rs.getString("identityno"));
                ep.setContactno(Config.rs.getString("contactno"));
                ep.setAltcontactno(Config.rs.getString("altcontactno"));
                ep.setAddress(Config.rs.getString("address"));
                ep.setLocality(Config.rs.getString("locality"));
                ep.setCity(Config.rs.getString("city"));
                ep.setPincode(Config.rs.getString("pincode"));
                ep.setState(Config.rs.getString("state"));
                ep.setEmail(Config.rs.getString("email"));
                ep.setSalary(Config.rs.getString("salary"));
                ep.setJoiningdate(Config.rs.getString("joiningdate"));
                ep.setLeavingdate(Config.rs.getString("leavingdate"));
                ep.setOther(Config.rs.getString("other"));
                
                Config.employeeprofile.add(ep);                
            }                
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
    public boolean loadClassManager() {
        try {
            Config.configproductmgr = new ConfigProductMgr();
            Config.wholesale_gst_mgr = new WholesaleGstMgr();
            Config.retail_gst_mgr = new RetailGstMgr();
            Config.configrproductmgr = new ConfigRProductMgr();
            Config.configflavormgr = new ConfigFlavorMgr();
            Config.configrvarietymgr = new ConfigRVarietyMgr();
            Config.configcontentratemgr = new ConfigContentRateMgr();
            Config.configschememgr = new ConfigSchemeMgr();
            Config.configprofitmgr = new ConfigProfitMgr();
            Config.configuserprofilemgr = new ConfigUserProfileMgr();
            
            Config.supplierprofilemgr = new SupplierProfileMgr();
            Config.customerprofilemgr = new CustomerProfileMgr();
            Config.employeeprofilemgr = new EmployeeProfileMgr();
            Config.payrollmgr = new PayrollMgr();
            
            Config.stockmgr = new StockMgr();
            Config.temp_price_details_mgr = new TempPriceDetailsMgr();
            Config.stockbillmgr = new StockBillMgr();
            Config.stockbillpurchasemgr = new StockBillPurchaseMgr();
            Config.stockbillreturnmgr = new StockBillReturnMgr();
            
            Config.wholesalebillmgr = new WholesaleBillMgr();
            Config.wholesalebillpurchasemgr = new WholesaleBillPurchaseMgr();
            Config.wholesalebillreturnmgr = new WholesaleBillReturnMgr();
            
            Config.retailbillmgr = new RetailBillMgr();
            Config.retailbillpurchasemgr = new RetailBillPurchaseMgr();
            Config.retailbillreturnmgr = new RetailBillReturnMgr();
            
            Config.retail_stock_bill_mgr = new RetailStockBillMgr();
            Config.retail_stock_purchase_mgr = new RetailStockPurchaseMgr();
            Config.retail_stock_mgr = new RetailStockMgr();

            
            Config.billing_details_mgr = new BillingDetailsMgr();
            Config.other_config_mgr = new OtherConfigMgr();
//            Config.read_mgr = new ReadMgr();
            
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }    
    
    public boolean loadForms() {
        try {
            
            Config.login = new Login(null, true);
            Config.manageaccount = new ManageAccount(null, true);
            
            Config.productlicense = new ProductLicense(null, true);
            
            Config.home = new Home();
            Config.productconfiguration = new ProductConfiguration(null, true);
            
            Config.suppliermanagement = new SupplierManagement(null, true);
            Config.newsupplier = new NewSupplier(null, true);
            Config.viewsupplier = new ViewSupplier(null, true);

            
            Config.customermanagement = new CustomerManagement(null, true);
            Config.newcustomer = new NewCustomer(null, true);
            Config.viewcustomer = new ViewCustomer(null, true);
                        
            Config.payrollmanagement = new PayrollManagement(null, true);
            Config.newemployee = new NewEmployee(null, true);
            Config.viewemployee = new ViewEmployee(null, true);            
            
            
            
            Config.stockmanagement=new StockManagement(null, true);
            Config.stocknewbill = new StockNewBill(null, true);
            Config.stockviewbill = new StockViewBill(null, true);
            Config.stockentry = new StockEntry(null, true);            
            Config.stockpendingbill = new StockPendingBill(null, true);            
            
            Config.stockshow = new StockShow(null, true);
            Config.batch_show = new BatchShow(null, true);
            
            Config.wholesalenewbill = new WholesaleNewBill(null, true);
            Config.wholesaleviewbill = new WholesaleViewBill(null, true);
            Config.wholesalebillentry = new WholesaleBillEntry(null, true);
            Config.wholesalependingbill = new WholesalePendingBill(null, true);
            Config.whole_sale_paid = new WholeSalePaid(null, true);
            
            Config.billsummary = new BillSummary(null, true);
            Config.freesummary = new FreeSummary(null, true);
            Config.schemesummary = new SchemeSummary(null, true);
            Config.wholesalebillsummary = new WholeSaleBillSummary(null, true);
            Config.retailbillsummary = new RetailBillSummary(null, true);
            Config.search_table =new SearchTable(null, true);
            Config.search_table_stock =new SearchTableStock(null, true);
            Config.item_search_table =new ItemSearchTable(null, true);
            Config.retail_search_table =new RetailSearchTable(null, true);
            Config.search =new Search(null, true);
            Config.homesearch =new HomeSearch(null, true);
            Config.search_employee =new SearchEmployee(null, true);
            Config.retailnewbill= new RetailNewBill(null, true);
            Config.retailviewbill= new RetailViewBill(null, true);
            Config.retailbillentry = new RetailBillEntry(null, true);
            Config.retailbillentry_medical = new RetailBillEntryMedical(null, true);
            Config.retailmanagement = new RetailManagement(null, true);
            Config.Loading = new Loading(null, true);
            
            
            Config.retail_stock_entry = new RetailStockEntry(null, true);
            Config.retail_stock_management = new RetailStockManagement(null, true);
            Config.retail_stock_new_bill = new RetailStockNewBill(null, true);
            Config.retail_stock_view_bill = new RetailStockViewBill(null, true);
            Config.search_table_retail_stock = new SearchTableRetailStock(null, true);
            
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }        
    }
    public boolean createTable(String sql){
        try {
                Config.stmt.executeUpdate(sql);
               return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } 
    }
      
}
