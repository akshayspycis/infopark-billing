/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classmanager.barcode_reader.barcode;

import datamanager.config.Config;
import java.awt.AWTEvent;
import java.awt.Toolkit;
import java.lang.reflect.Field;

/**
 *
 * @author akshay
 */
public class ReadMgr {
    public ReadMgr(){
        Toolkit.getDefaultToolkit().addAWTEventListener(
                new BarcodeAwareAWTEventListener(new BarcodeCapturedListener() {
                public void barcodeCaptured(String barcode) {
                    try {
                        Config.module_text.setText(barcode);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
        }), AWTEvent.KEY_EVENT_MASK);
    }
}
