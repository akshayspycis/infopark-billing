package classmanager.pdfMgr.create;

import classmanager.pdfMgr.print.PrintPdf;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Font.FontFamily;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import datamanager.config.Config;
import datamanager.wholesale.WholesaleBill;
import datamanager.wholesale.WholesaleBillPurchase;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 *
 * @author Akshay Jain
 */
public class WholesaleBill_MedicalIGst {
    
    static SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");    
    static SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy HH:MM a");   
    
    Font df0 = FontFactory.getFont("Tahoma", BaseFont.WINANSI, BaseFont.EMBEDDED, 1, Font.NORMAL);
    Font df1 = FontFactory.getFont("Tahoma", BaseFont.WINANSI, BaseFont.EMBEDDED, 8, Font.NORMAL);
    Font df2 = FontFactory.getFont("Tahoma", BaseFont.WINANSI, BaseFont.EMBEDDED, 8, Font.NORMAL);
    Font df3 = FontFactory.getFont("Tahoma", BaseFont.WINANSI, BaseFont.EMBEDDED, 8, Font.BOLD);
    Font df4 = FontFactory.getFont("Times-Roman", BaseFont.WINANSI, BaseFont.EMBEDDED, 25, Font.NORMAL);
    Font df5 = FontFactory.getFont("Times-Roman", BaseFont.WINANSI, BaseFont.EMBEDDED, 16, Font.NORMAL);
    Font df6 = FontFactory.getFont("Tahoma", BaseFont.WINANSI, BaseFont.EMBEDDED, 10, Font.NORMAL);
    
    String string;
    String st1[] = { "", "One", "Two", "Three", "Four", "Five", "Six", "Seven",
                     "Eight", "Nine", };
    String st2[] = { "Hundred", "Thousand", "Lakh", "Crore" };
    String st3[] = { "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen",
                    "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Ninteen", };
    String st4[] = { "Twenty", "Thirty", "Fourty", "Fifty", "Sixty", "Seventy",
                    "Eighty", "Ninty" };
    
    public String createPdf (WholesaleBill wholesalebill) {
        try {
            //create folder if not exits
                File file = new File("pdf\\"+sdf1.format(Calendar.getInstance().getTime()));
                if (!file.exists()) {
                    file.mkdirs();                
                }
            
            //creation of pdf contents
                String filename = "pdf\\"+sdf1.format(Calendar.getInstance().getTime())+"\\"+wholesalebill.getBillno()+".pdf";
                OutputStream pdffile = new FileOutputStream(new File(filename));
                Document document = new Document(PageSize.A4,20,20,15,0);
                PdfWriter writer = PdfWriter.getInstance(document, pdffile);           
                
                //new line
                    Paragraph clear = new Paragraph(" ", df0);

                    //header table
                        PdfPTable headertbl = new PdfPTable(3);
                        headertbl.setWidthPercentage(100);
                        headertbl.setSpacingAfter(6.0f);
                        
                        PdfPCell tin_no=null; 
                        try {
                             tin_no = new PdfPCell (new Paragraph("GSTIN :"+Config.billing_details.get(0).getTin_no(), df6));
                        } catch (Exception e) {
                             tin_no = new PdfPCell (new Paragraph("", df6));
                        }
                        tin_no.setHorizontalAlignment(Element.ALIGN_LEFT);
                        tin_no.setBorderColor(BaseColor.LIGHT_GRAY);
                        tin_no.setBorder(Rectangle.BOTTOM);
                        

                        PdfPCell c1 = new PdfPCell (new Paragraph("CASH/CREDIT MEMO", df1));
                        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                        c1.setBorderColor(BaseColor.LIGHT_GRAY);
                        c1.setBorder(Rectangle.BOTTOM);
                        
                        PdfPCell c2 = new PdfPCell (new Paragraph("INFOPARK", df1));
                        c2.setHorizontalAlignment(Element.ALIGN_RIGHT);
                        c2.setBorderColor(BaseColor.LIGHT_GRAY);
                        c2.setBorder(Rectangle.BOTTOM);
                        
                        headertbl.addCell(tin_no);
                        headertbl.addCell(c1);
                        headertbl.addCell(c2);
                        
                        
                        PdfPTable headertbl1 = new PdfPTable(1);
                        headertbl1.setWidthPercentage(100);
                        headertbl1.setSpacingAfter(6.0f);
                        
//.........................................................................................................
                        PdfPCell center = new PdfPCell (new Paragraph(""));
                        center.setBorder(0);
                        
                        PdfPTable nestedTable = new PdfPTable(1);
                        nestedTable.setWidthPercentage(100);
                        nestedTable.setSpacingAfter(6.0f);
                        PdfPCell company_name =null;
                        
                        try {
                            company_name = new PdfPCell (new Paragraph(Config.billing_details.get(0).getCompany_name(), df4));
                        } catch (Exception e) {
                            company_name = new PdfPCell (new Paragraph("", df4));
                        }
                        
                        company_name.setHorizontalAlignment(Element.ALIGN_CENTER);
                        company_name.setBorder(0);  
                        
                        PdfPCell tag_line =null;
                        try {
                            tag_line = new PdfPCell (new Paragraph(Config.billing_details.get(0).getTagline(), df6));
                        } catch (Exception e) {
                            tag_line = new PdfPCell (new Paragraph("", df6));
                        }
                        tag_line.setHorizontalAlignment(Element.ALIGN_CENTER);
                        tag_line.setBorder(Rectangle.BOTTOM);
                        
                        
                        PdfPCell address = new PdfPCell (new Paragraph(Config.billing_details.get(0).getAddress(), df6));
                        address.setHorizontalAlignment(Element.ALIGN_CENTER);
                        address.setBorder(0);
                        PdfPCell mob=null;
                        try {
                            if(!Config.billing_details.get(0).getAlt_contact_no().equals("")){
                             mob = new PdfPCell (new Paragraph("Mob.:"+Config.billing_details.get(0).getContact_no()+","+Config.billing_details.get(0).getAlt_contact_no(), df6));
                        }else{
                             mob = new PdfPCell (new Paragraph("Mob.:"+Config.billing_details.get(0).getContact_no(), df6));
                            }
                        } catch (Exception e) {
                             mob = new PdfPCell (new Paragraph("", df6));
                        }
                        mob.setHorizontalAlignment(Element.ALIGN_CENTER);
                        mob.setBorder(0);
                        
                        nestedTable.addCell(company_name);
                        nestedTable.addCell(tag_line);
                        nestedTable.addCell(address);
                        nestedTable.addCell(mob);
                        
                        
                        center.addElement(nestedTable);
                        
//.........................................................................................................                        
                        headertbl1.addCell(center);
                        
                    //customer & bill details table
                        PdfPTable tbl1 = new PdfPTable(7);
                        tbl1.setWidthPercentage(100);
                        tbl1.setSpacingAfter(6.0f);

                        PdfPCell cd1 = new PdfPCell (new Paragraph("M/S. : ", df2));
                        cd1.setBorderColor(new BaseColor (200, 200, 200));
                        
                        PdfPCell cd2 = new PdfPCell (new Paragraph(wholesalebill.getCustomerprofile().getOrganization(), df3));
                        cd2.setBorderColor(new BaseColor (200, 200, 200));
                        cd2.setColspan(4);
                        
                        PdfPCell cd3 = new PdfPCell (new Paragraph("Tin : ", df2));
                        cd3.setBorderColor(new BaseColor (200, 200, 200));
                                                
                        PdfPCell cd4 = new PdfPCell (new Paragraph(wholesalebill.getCustomerprofile().getTinno(), df3));
                        cd4.setBorderColor(new BaseColor (200, 200, 200));
                                                
                        PdfPCell cd5 = new PdfPCell (new Paragraph("Cus. Name : ", df2));
                        cd5.setBorderColor(new BaseColor (200, 200, 200));
                                                
                        PdfPCell cd6 = new PdfPCell (new Paragraph(wholesalebill.getCustomerprofile().getCustomername(), df3));
                        cd6.setBorderColor(new BaseColor (200, 200, 200));
                        cd6.setColspan(2);
                        
                        PdfPCell cd7 = new PdfPCell (new Paragraph("Contact No.. : ", df2));
                        cd7.setBorderColor(new BaseColor (200, 200, 200));
                                                
                        PdfPCell cd8 = new PdfPCell (new Paragraph(wholesalebill.getCustomerprofile().getContactno(), df3));
                        cd8.setBorderColor(new BaseColor (200, 200, 200));
                        
                        PdfPCell cd9 = new PdfPCell (new Paragraph("Sales Ex. : ", df2));
                        cd9.setBorderColor(new BaseColor (200, 200, 200));
                                                                        
                        PdfPCell cd10 = new PdfPCell (new Paragraph(wholesalebill.getSalesman(), df3));
                        cd10.setBorderColor(new BaseColor (200, 200, 200));
                        
                        
                        PdfPCell cd11 = new PdfPCell (new Paragraph("Address : ", df2));
                        cd11.setBorderColor(new BaseColor (200, 200, 200));
                        
                        PdfPCell cd12=null;
                        cd12 = new PdfPCell (new Paragraph(wholesalebill.getCustomerprofile().getAddress()+" "+wholesalebill.getCustomerprofile().getLocality()+" "+wholesalebill.getCustomerprofile().getCity()+" "+wholesalebill.getCustomerprofile().getState()+" ("+wholesalebill.getCustomerprofile().getPincode()+")", df3));
                        cd12.setBorderColor(new BaseColor (200, 200, 200));
                        cd12.setColspan(2);

                        PdfPCell cd13 = new PdfPCell (new Paragraph("Bill No. : ", df2));
                        cd13.setBorderColor(new BaseColor (200, 200, 200));
                                                
                        PdfPCell cd14 = new PdfPCell (new Paragraph(wholesalebill.getBillno(), df3));
                        cd14.setBorderColor(new BaseColor (200, 200, 200));
                                                
                        PdfPCell cd15 = new PdfPCell (new Paragraph("Date : ", df2));
                        cd15.setBorderColor(new BaseColor (200, 200, 200));
                                                
                        PdfPCell cd16 = new PdfPCell (new Paragraph(wholesalebill.getDate(), df3));
                        cd16.setBorderColor(new BaseColor (200, 200, 200));                        
                                                
                        tbl1.addCell(cd1);
                        tbl1.addCell(cd2);
                        tbl1.addCell(cd3);
                        tbl1.addCell(cd4);
                        tbl1.addCell(cd5);
                        tbl1.addCell(cd6);
                        tbl1.addCell(cd7);                    
                        tbl1.addCell(cd8);
                        tbl1.addCell(cd9);
                        tbl1.addCell(cd10);
                        tbl1.addCell(cd11);
                        tbl1.addCell(cd12);
                        tbl1.addCell(cd13);
                        tbl1.addCell(cd14);                    
                        tbl1.addCell(cd15);                    
                        tbl1.addCell(cd16);                    

                    //purchase details table
                        PdfPTable tbl2 = new PdfPTable(18);
                        tbl2.setWidthPercentage(100);
                        tbl2.setSpacingAfter(6.0f);

                       String[] hv = {"Sn.","Product Name", "HSN Code","Batch","Exp.\nDate", "MRP", "Rate", "Qty.", "Amt.","Scheme\nRate | Amount" ,"Total Amt",  "IGST\nRate | Amount","Net Amt."};
                       for (int i = 0; i < 13; i++) {
                            PdfPCell h = new PdfPCell (new Paragraph(hv[i], df3));                            
                            h.setHorizontalAlignment (Element.ALIGN_CENTER);
                            h.setBackgroundColor (new BaseColor (218, 218, 218));
                            h.setBorder(0);
                            if (i == 1) {
                                h.setColspan(4);
                                h.setHorizontalAlignment (Element.ALIGN_LEFT);
                            }
                            else if (i == 9 ||  i == 11 ) {
                                h.setColspan(2);
                            }else if (i == 12) {
                                h.setColspan(1);
                            } else {
                                h.setColspan(0);
                            }
                            tbl2.addCell(h);
                        }
                        
                        int ctr;                        
                        if (wholesalebill.getWholesalebillpurchase().size() <= 8) {
                            ctr = 8;
                        } else {
                            ctr = wholesalebill.getWholesalebillpurchase().size();
                        }

                        float totalnetamount = 0;
                        float totalvatamount = 0;
                        float vatamt = 0;
                        float netamt = 0;
                        float total;
                        for (int j = 0; j < ctr; j++) {
                            if (j<wholesalebill.getWholesalebillpurchase().size()) {                                
                                WholesaleBillPurchase wbp = wholesalebill.getWholesalebillpurchase().get(j);                                
                                total=0;
                                netamt=0;
                                vatamt=0;
                                
                                try {
                                   total = Float.parseFloat(wbp.getAmount())-Float.parseFloat(wbp.getSchemeamt());
                                } catch (Exception e) {
                                    total=0;
                                }
                                String t="";
                                try {
                                    if(total!=0){
                                        t=new DecimalFormat("#.##").format(total);
                                    }else{
                                        t="";
                                    }
                                    
                                } catch (Exception e) {
                                    t="";
                                }
                                try {
                                    netamt = Float.parseFloat(wbp.getVatamt()) + total;                                    
                                } catch (Exception e) {
                                    netamt=0;
                                }
                                String n="";
                                try {
                                    if (netamt!=0) {
                                        n=new DecimalFormat("#.##").format(netamt);
                                    } else {
                                        n="";
                                    }
                                    
                                } catch (Exception e) {
                                    n="";
                                }
                                String hsn_code="";
                                for (int i = 0; i < Config.configproduct.size(); i++) {
                                    if(Config.configproduct.get(i).getProductname().equals(wbp.getProduct())){
                                        hsn_code=Config.configproduct.get(i).getHsn_code();
                                        break;
                                    }
                                }
                                String[] cv = {
                                    String.valueOf(j+1),
                                    wbp.getProduct()+" "+wbp.getFlavor()+" "+wbp.getPack()+" / "+wbp.getContentrate()+"  "+wbp.getUnit(), 
                                    hsn_code, 
                                    wbp.getBatchno(), 
                                    wbp.getExpdate(), 
                                    wbp.getMrp(), 
                                    wbp.getRate(),
                                    wbp.getQuantity(),
                                    wbp.getAmount(),
                                    wbp.getScheme(),
                                    wbp.getSchemeamt(),
                                    t,
                                    wbp.getVat(),
                                    wbp.getVatamt(),
                                    n
                                };
                                for (int i = 0; i < 15; i++) {
                                    PdfPCell c = new PdfPCell (new Paragraph(cv[i], df1));
                                    c.setHorizontalAlignment (Element.ALIGN_CENTER);
                                    c.setVerticalAlignment(Element.ALIGN_CENTER);
                                    if (j==0) {
                                        c.setBorder(Rectangle.TOP);
                                        c.setBorderColor(BaseColor.LIGHT_GRAY);
                                    } else {
                                        c.setBorder(0);
                                    }                                    
                                    if (i == 1) {
                                        c.setHorizontalAlignment (Element.ALIGN_LEFT);
                                        c.setColspan(4);
                                    }
                                    else if (i == 11 || i == 12) {
                                        c.setColspan(1);
                                    } else {
                                        c.setColspan(0);
                                    }
                                    tbl2.addCell(c);
                                }
                                try {
                                    totalvatamount = totalvatamount + Float.parseFloat(wbp.getVatamt());
                                } catch (Exception e) {
                                }
                                
                                totalnetamount = totalnetamount + netamt;

                            } else {
                                String[] cv = {"", "", "", "", "", "", "", "", "", "","","","","",""};
                                for (int i = 0; i < 15; i++) {
                                    PdfPCell c = new PdfPCell (new Paragraph(cv[i], df1));
                                    c.setHorizontalAlignment (Element.ALIGN_CENTER);
                                    c.setVerticalAlignment(Element.ALIGN_CENTER);
                                    c.setNoWrap(false);
                                    if (j==0) {
                                        c.setBorder(Rectangle.TOP);
                                        c.setBorderColor(BaseColor.LIGHT_GRAY);
                                    } else {
                                        c.setBorder(0);
                                    }
                                    c.setFixedHeight(20);
                                    if (i == 1) {
                                        c.setColspan(4);
                                    }
                                    else if (i == 13 || i == 14) {
                                        c.setColspan(1);
                                    } else {
                                        c.setColspan(0);
                                    }
                                    tbl2.addCell(c);
                                }
                            }
                        }

                        String[] fv = {"", "Gra. Total", "", "","","", "", "", "","","","","","",new DecimalFormat("#.##").format(totalnetamount)};
                        for (int i = 0; i < 15; i++) {
                            PdfPCell l = new PdfPCell (new Paragraph(fv[i], df3));
                            l.setHorizontalAlignment (Element.ALIGN_CENTER);
                            l.setBorderColor(BaseColor.LIGHT_GRAY);
                            l.setBackgroundColor (new BaseColor (218, 218, 218));
                            l.setBorder(Rectangle.TOP);
                            if (i == 1) {
                                l.setColspan(4);
                            }else if (i == 13 || i == 14) {
                                l.setColspan(1);
                            } else {
                                l.setColspan(0);
                            }
                            tbl2.addCell(l);
                        }                    

                    //bill details table
                        PdfPTable tbl3 = new PdfPTable(6);
                        tbl3.setWidthPercentage(100);
                        tbl3.setSpacingAfter(6.0f);

                        PdfPCell bd1 = new PdfPCell (new Paragraph("Purchase Amount : ", df2));
                        bd1.setBorderColor(new BaseColor (200, 200, 200));                    
                        tbl3.addCell(bd1);

                        PdfPCell bd2 = new PdfPCell (new Paragraph(wholesalebill.getPurchaseamt(), df3));
                        bd2.setBorderColor(new BaseColor (200, 200, 200));                    
                        tbl3.addCell(bd2);

                        PdfPCell bd3 = new PdfPCell (new Paragraph("Scheme Deduction : ", df2));
                        bd3.setBorderColor(new BaseColor (200, 200, 200));                    
                        tbl3.addCell(bd3);

                        PdfPCell bd4 = new PdfPCell (new Paragraph(wholesalebill.getSchemeamt(), df3));
                        bd4.setBorderColor(new BaseColor (200, 200, 200));                    
                        tbl3.addCell(bd4);

                        PdfPCell bd5 = new PdfPCell (new Paragraph("GST Amount : ", df2));
                        bd5.setBorderColor(new BaseColor (200, 200, 200));                    
                        tbl3.addCell(bd5);

                        PdfPCell bd6 = new PdfPCell (new Paragraph("IGST       : "+new DecimalFormat("#.##").format(Float.parseFloat(wholesalebill.getVatamt())), df3));
                        bd6.setBorderColor(new BaseColor (200, 200, 200));                    
                        tbl3.addCell(bd6);

                        PdfPCell bd7 = new PdfPCell (new Paragraph("Discount : ", df2));
                        bd7.setBorderColor(new BaseColor (200, 200, 200));                    
                        tbl3.addCell(bd7);

                        PdfPCell bd8 = new PdfPCell (new Paragraph(wholesalebill.getDiscount(), df3));
                        bd8.setBorderColor(new BaseColor (200, 200, 200));                    
                        tbl3.addCell(bd8);

                        PdfPCell bd9 = new PdfPCell (new Paragraph("Payable Amount : ", df2));
                        bd9.setBorderColor(new BaseColor (200, 200, 200));                    
                        tbl3.addCell(bd9);

                        PdfPCell bd10 = new PdfPCell (new Paragraph(new DecimalFormat("#").format(Float.parseFloat(wholesalebill.getPayableamt())), df3));
                        bd10.setBorderColor(new BaseColor (200, 200, 200));                    
                        tbl3.addCell(bd10);

                        PdfPCell bd11 = new PdfPCell (new Paragraph("Paid Amount : ", df2));
                        bd11.setBorderColor(new BaseColor (200, 200, 200));                    
                        tbl3.addCell(bd11);

                        PdfPCell bd12 = new PdfPCell (new Paragraph(new DecimalFormat("#").format(Float.parseFloat(wholesalebill.getPaidamt())), df3));
                        bd12.setBorderColor(new BaseColor (200, 200, 200));                    
                        tbl3.addCell(bd12);

                        PdfPCell bd13 = new PdfPCell (new Paragraph("In Words : ", df2));
                        bd13.setBorderColor(new BaseColor (200, 200, 200));                    
                        tbl3.addCell(bd13);

                        int i = (int) Float.parseFloat(wholesalebill.getPaidamt());
                        String str = convert(i).trim();
                        PdfPCell bd14;
                        if (str.equals("")) {
                            bd14 = new PdfPCell (new Paragraph("NIL", df3));
                        } else {
                            bd14 = new PdfPCell (new Paragraph(str + " Only", df3));
                        }

                        bd14.setBorderColor(new BaseColor (200, 200, 200));
                        bd14.setColspan(3);
                        tbl3.addCell(bd14);

                        PdfPCell bd15 = new PdfPCell (new Paragraph("Balance Amount : ", df2));
                        bd15.setBorderColor(new BaseColor (200, 200, 200));                    
                        tbl3.addCell(bd15);

                        PdfPCell bd16 = new PdfPCell (new Paragraph(new DecimalFormat("#").format(Float.parseFloat(wholesalebill.getBalanceamt())), df3));
                        bd16.setBorderColor(new BaseColor (200, 200, 200));                    
                        tbl3.addCell(bd16);                
                    
                    //footer table
                        PdfPTable tbl4 = new PdfPTable(3);
                        tbl4.setWidthPercentage(100);                        

                        PdfPCell fd1 = new PdfPCell (new Paragraph("NOTE : ", df3));
                        fd1.setBorder(0);
                        fd1.setColspan(2);

                        PdfPCell fd2 = new PdfPCell (new Paragraph(" ", df2));
                        fd2.setHorizontalAlignment (Element.ALIGN_CENTER);
                        fd2.setBorder(0);

                        PdfPCell fd3 = new PdfPCell (new Paragraph("1. The balance of bill amount must be paid within 15 days from billing date.", df2));
                        fd3.setBorder(0);
                        fd3.setColspan(2);

                        PdfPCell fd4 = new PdfPCell (new Paragraph(Config.billing_details.get(0).getCompany_name(), df3));
                        fd4.setHorizontalAlignment (Element.ALIGN_CENTER);
                        fd4.setBorder(0);

                        PdfPCell fd5 = new PdfPCell (new Paragraph("2. All disputes, legal matters, court matters, if any, shall be subject to Bhopal jurisdiction only.", df2));
                        fd5.setBorder(0);
                        fd5.setColspan(2);

                        PdfPCell fd6 = new PdfPCell (new Paragraph(" ", df2));
                        fd6.setHorizontalAlignment (Element.ALIGN_CENTER);
                        fd6.setBorder(0);

                        PdfPCell fd7 = new PdfPCell (new Paragraph("3. This is system generated invoice & doesn't required seal or signature. (E & OE.)", df2));
                        fd7.setBorder(0);
                        fd7.setColspan(2);

                        PdfPCell fd8 = new PdfPCell (new Paragraph("Authorised Signatory", df2));
                        fd8.setHorizontalAlignment (Element.ALIGN_CENTER);
                        fd8.setBorder(0);                        

                        tbl4.addCell(fd1);
                        tbl4.addCell(fd2);
                        tbl4.addCell(fd3);
                        tbl4.addCell(fd4);
                        tbl4.addCell(fd5);
                        tbl4.addCell(fd6);
                        tbl4.addCell(fd7);
                        tbl4.addCell(fd8);                        
                            
                        //writer.setPageEvent(new Watermark());
                        
            //Now Insert Every Thing Into PDF Document
                document.open();//PDF document opened........			       

                document.add(headertbl);                
                document.add(headertbl1);                
                document.add(tbl1);
                document.add(tbl2);
                document.add(tbl3);
                document.add(tbl4);                
                document.close();
                
            //closing of pdf
                pdffile.close();
                
            return filename;
            
        } catch (Exception e) {
            e.printStackTrace();
            return null;            
        }
    }
    
    public boolean printPdf(String filename) {
        try {
            FileInputStream fis = new FileInputStream(filename);
            PrintPdf printPDFFile = new PrintPdf(fis, "Super Tea Center");
            printPDFFile.print();
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }   
    
    public String convert(int number) {
                int n = 1;
                int word;
                string = "";
                while (number != 0) {
                        switch (n) {
                        case 1:
                                word = number % 100;
                                pass(word);
                                if (number > 100 && number % 100 != 0) {
                                        show("and ");
                                }
                                number /= 100;
                                break;

                        case 2:
                                word = number % 10;
                                if (word != 0) {
                                        show(" ");
                                        show(st2[0]);
                                        show(" ");
                                        pass(word);
                                }
                                number /= 10;
                                break;

                        case 3:
                                word = number % 100;
                                if (word != 0) {
                                        show(" ");
                                        show(st2[1]);
                                        show(" ");
                                        pass(word);
                                }
                                number /= 100;
                                break;

                        case 4:
                                word = number % 100;
                                if (word != 0) {
                                        show(" ");
                                        show(st2[2]);
                                        show(" ");
                                        pass(word);
                                }
                                number /= 100;
                                break;

                        case 5:
                                word = number % 100;
                                if (word != 0) {
                                        show(" ");
                                        show(st2[3]);
                                        show(" ");
                                        pass(word);
                                }
                                number /= 100;
                                break;
                        }
                        n++;
                }
                return string;
        }

    public void pass(int number) {
            int word, q;
            if (number < 10) {
                    show(st1[number]);
            }
            if (number > 9 && number < 20) {
                    show(st3[number - 10]);
            }
            if (number > 19) {
                    word = number % 10;
                    if (word == 0) {
                            q = number / 10;
                            show(st4[q - 2]);
                    } else {
                            q = number / 10;
                            show(st1[word]);
                            show(" ");
                            show(st4[q - 2]);
                    }
            }
    }

    public void show(String s) {
            String st;
            st = string;
            string = s;
            string += st;
    }    
    
    /**
     * Inner class to add a watermark to every page.
     */
    class Watermark extends PdfPageEventHelper { 
        Font FONT = new Font(FontFamily.HELVETICA, 52, Font.BOLD, new GrayColor(0.75f)); 
        public void onEndPage(PdfWriter writer, Document document) {
            try {
//                ColumnText.showTextAligned(writer.getDirectContentUnder(),Element.ALIGN_CENTER, new Phrase("SUPER TEA CENTER", FONT),297.5f, 631, writer.getPageNumber() % 2 == 1 ? 0 : -0);
                Image background = Image.getInstance("lib\\stc.png");
//                 This scales the image to the page,
//                 use the image's width & height if you don't want to scale.
                writer.getDirectContentUnder().addImage(background, 400, 0, 0, 220, 100, 500);
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }
}
