package classmanager.pdfMgr.create;

import classmanager.pdfMgr.print.PrintPdf;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import datamanager.wholesale.summ;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class BillSummary_pdf {    
    
    Font df0 = FontFactory.getFont("Tahoma", BaseFont.WINANSI, BaseFont.EMBEDDED, 3, Font.NORMAL);
    Font df1 = FontFactory.getFont("Tahoma", BaseFont.WINANSI, BaseFont.EMBEDDED, 8, Font.NORMAL);
    Font df2 = FontFactory.getFont("Tahoma", BaseFont.WINANSI, BaseFont.EMBEDDED, 10, Font.NORMAL);
    Font df3 = FontFactory.getFont("Tahoma", BaseFont.WINANSI, BaseFont.EMBEDDED, 11, Font.BOLD);
    
    String string;    
    
    public String createPdf (ArrayList<summ> summlist) {
        try {
            //create folder if not exits
                File file = new File("temp");
                if (!file.exists()) {
                    file.mkdirs();                
                }
            
            //creation of pdf contents
                String filename = "temp\\temp1.pdf";
                OutputStream pdffile = new FileOutputStream(new File(filename));
                Document document = new Document(PageSize.A4,20,20,15,0);
                PdfWriter.getInstance(document, pdffile);           
                
                    //header table
                        PdfPTable headertbl = new PdfPTable(1);
                        headertbl.setWidthPercentage(100);
                        headertbl.setSpacingAfter(6.0f);

                        PdfPCell c1 = new PdfPCell (new Paragraph("BILL SUMMARY", df3));
                        c1.setHorizontalAlignment(Element.ALIGN_CENTER);
                        c1.setBorderColor(BaseColor.LIGHT_GRAY);
                        c1.setBorder(Rectangle.BOTTOM);
                        
                        headertbl.addCell(c1);

                    //purchase details table
                        PdfPTable tbl2 = new PdfPTable(7);
                        tbl2.setWidthPercentage(100);
                        tbl2.setSpacingAfter(6.0f);

                        String[] hv = {"Product", "Flavor", "C. R."," Unit " ,"Batch No.", "Exp. Date", "Quantity"};
                        for (int i = 0; i < 7; i++) {
                            PdfPCell h = new PdfPCell (new Paragraph(hv[i], df3));                            
                            h.setHorizontalAlignment (Element.ALIGN_CENTER);
                            h.setBackgroundColor (new BaseColor (218, 218, 218));
                            h.setBorder(0);                            
                            tbl2.addCell(h);
                        }
                        int count=0;
                        for (int j = 0; j < summlist.size(); j++) {
                            summ obj = summlist.get(j);
                            count=count+Integer.parseInt(obj.getQuantity());
                            String[] cv = {
                                obj.getProduct(),
                                obj.getFlavor(),
                                obj.getCr(),
                                obj.getUnit(),
                                obj.getBatchno(), 
                                obj.getExpdate(), 
                                obj.getQuantity()                                                                   
                            };
                            for (int i = 0; i < 7; i++) {
                                PdfPCell c = new PdfPCell (new Paragraph(cv[i], df1));
                                c.setHorizontalAlignment (Element.ALIGN_CENTER);
                                c.setVerticalAlignment(Element.ALIGN_CENTER);
                                c.setBorderColor(new BaseColor (240, 240, 240));
                                c.setFixedHeight(18);                                    
                                tbl2.addCell(c);
                            }                            
                        }                    
            //footer in PDF Document
                String[] fv = {"", "Total Quantity ","","" ,"","",String.valueOf(count)};
                        for (int i = 0; i < 7; i++) {
                            PdfPCell l = new PdfPCell (new Paragraph(fv[i], df3));
                            l.setHorizontalAlignment (Element.ALIGN_CENTER);
                            l.setBorderColor(BaseColor.LIGHT_GRAY);
                            l.setBackgroundColor (new BaseColor (218, 218, 218));
                            l.setBorder(Rectangle.TOP);
                            if (i == 6) {
                                l.setColspan(2);
                            }else {
                                l.setColspan(1);
                            }
                            
                            tbl2.addCell(l);
                        }                    
                        
            //Now Insert Every Thing Into PDF Document
                document.open();//PDF document opened........			       
                document.add(headertbl);                
                document.add(tbl2);                
                document.close();
             
            //closing of pdf
                pdffile.close();

            return filename;
            
        } catch (Exception e) {
            e.printStackTrace();
            return null;            
        }
    }
    
    public boolean printPdf(String filename) {
        try {
            System.out.println(filename);
            FileInputStream fis = new FileInputStream(filename);
            PrintPdf printPDFFile = new PrintPdf(fis, "Super Tea Center");
            printPDFFile.print();
            
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    } 
}
