/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classmanager.pdfMgr.create;

import classmanager.pdfMgr.print.PrintPdf;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Document;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.GrayColor;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;
import datamanager.config.Config;
import datamanager.retail.RetailBill;
import datamanager.retail.RetailBillPurchase;
import datamanager.wholesale.WholesaleBill;
import datamanager.wholesale.WholesaleBillPurchase;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
/**
 *
 * @author akshay
 */
public class PosPdf80 {
    
    static SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");    
    static SimpleDateFormat sdf2 = new SimpleDateFormat("dd-MM-yyyy HH:MM a");   
    
    Font df0 = FontFactory.getFont("Tahoma", BaseFont.WINANSI, BaseFont.EMBEDDED, 1, Font.NORMAL);
    Font df1 = FontFactory.getFont("Tahoma", BaseFont.WINANSI, BaseFont.EMBEDDED, 2, Font.NORMAL);
    Font df2 = FontFactory.getFont("Tahoma", BaseFont.WINANSI, BaseFont.EMBEDDED, 1, Font.NORMAL);
    Font df3 = FontFactory.getFont("Tahoma", BaseFont.WINANSI, BaseFont.EMBEDDED, 1, Font.NORMAL);
    
    String string;
    
    public String createPdf (RetailBill retailbill) {
        try {
            //create folder if not exits
                File file = new File("temp\\"+sdf1.format(Calendar.getInstance().getTime()));
                if (!file.exists()) {
                    file.mkdirs();
                }
            
           //creation of pdf contents
                String filename = "temp\\"+sdf1.format(Calendar.getInstance().getTime())+"\\"+retailbill.getBillno()+".pdf";
                OutputStream pdffile = new FileOutputStream(new File(filename));
                Document document = new Document(PageSize.B10,0,0,0,0);
                PdfWriter writer = PdfWriter.getInstance(document, pdffile);           
                    Paragraph clear = new Paragraph(" ", df0);
                    //header table
                        PdfPTable headertbl = new PdfPTable(1);
                        headertbl.setWidthPercentage(100);
                        PdfPCell tin_no=null; 
                        try {
                             tin_no = new PdfPCell (new Paragraph("GSTIN :"+Config.billing_details.get(0).getTin_no(), df2));
                        } catch (Exception e) {
                             tin_no = new PdfPCell (new Paragraph("", df3));
                        }
                        tin_no.setHorizontalAlignment(Element.ALIGN_LEFT);
                        tin_no.setBorder(0);
                        tin_no.setPadding(0);
                        tin_no.setPaddingTop(1);

                        headertbl.addCell(tin_no);
                        
                        
                        PdfPTable headertbl1 = new PdfPTable(1);
                        headertbl1.setWidthPercentage(100);
                       
                        //.........................................................................................................
                        PdfPCell center = new PdfPCell (new Paragraph(""));
                        center.setBorder(0);
                        
                        PdfPTable nestedTable = new PdfPTable(1);
                        nestedTable.setWidthPercentage(100);
                  
                        PdfPCell company_name =null;
                        
                        try {
                            company_name = new PdfPCell (new Paragraph(Config.billing_details.get(0).getCompany_name(), df1));
                        } catch (Exception e) {
                            company_name = new PdfPCell (new Paragraph("", df1));
                        }
                        company_name.setHorizontalAlignment(Element.ALIGN_LEFT);
                        company_name.setBorder(0);  
                        company_name.setPadding(0);
                        PdfPCell address = new PdfPCell (new Paragraph(Config.billing_details.get(0).getAddress(), df3));
                        address.setHorizontalAlignment(Element.ALIGN_LEFT);
                        address.setBorder(0);
                        address.setPadding(0);
                        PdfPCell mob=null;
                        try {
                            if(!Config.billing_details.get(0).getAlt_contact_no().equals("")){
                                mob = new PdfPCell (new Paragraph("Mob.:"+Config.billing_details.get(0).getContact_no()+","+Config.billing_details.get(0).getAlt_contact_no(), df3));
                            }else{
                                mob = new PdfPCell (new Paragraph("Mob.:"+Config.billing_details.get(0).getContact_no(), df3));
                            }
                        } catch (Exception e) {
                                mob = new PdfPCell (new Paragraph("", df3));
                        }
                        mob.setHorizontalAlignment(Element.ALIGN_LEFT);
                        mob.setBorder(0);
                        mob.setPadding(0);
                        
                        nestedTable.addCell(company_name);
                        nestedTable.addCell(address);
                        nestedTable.addCell(mob);
                        center.addElement(nestedTable);
//.........................................................................................................                        
                        headertbl1.addCell(center);
                    //customer & bill details table
                        PdfPTable tbl1 = new PdfPTable(2);
                        tbl1.setWidthPercentage(100);
                        PdfPCell cd5 = new PdfPCell (new Paragraph("Name : ", df2));
                        cd5.setBorder(0);
                        cd5.setPadding(0);
                        PdfPCell cd6 = new PdfPCell (new Paragraph(retailbill.getCustomerprofile().getCustomername(), df3));
                        cd6.setBorder(0);
                        cd6.setPadding(0);
                        PdfPCell cd7 = new PdfPCell (new Paragraph("Contact: ", df2));
                        cd7.setBorder(0);
                        cd7.setPadding(0);
                        PdfPCell cd8 = new PdfPCell (new Paragraph(retailbill.getCustomerprofile().getContactno(), df3));
                        cd8.setBorder(0);
                        cd8.setPadding(0);
                        PdfPCell cd13 = new PdfPCell (new Paragraph("Bill No. : ", df2));
                        cd13.setBorder(0);                     
                        cd13.setPadding(0);
                        PdfPCell cd14 = new PdfPCell (new Paragraph(retailbill.getBillno(), df3));
                        cd14.setBorder(0);                 
                        cd14.setPadding(0);
                        PdfPCell cd15 = new PdfPCell (new Paragraph("Date : ", df2));
                        cd15.setBorder(0);
                        cd15.setPadding(0);
                        PdfPCell cd16 = new PdfPCell (new Paragraph(retailbill.getDate(), df3));
                        cd16.setBorder(0);                  
                        cd16.setPadding(0); 
                        tbl1.addCell(cd5);
                        tbl1.addCell(cd6);
                        tbl1.addCell(cd7);                    
                        tbl1.addCell(cd8);
                        tbl1.addCell(cd13);
                        tbl1.addCell(cd14);                    
                        tbl1.addCell(cd15);                    
                        tbl1.addCell(cd16);                    
                        PdfPTable tbl2 = new PdfPTable(4);
                        tbl2.setWidthPercentage(100);
                        String[] hv = { "Name", "Rate", "Qty.","Amount"};
                        for (int i = 0; i < 4; i++) {
                            PdfPCell h = new PdfPCell (new Paragraph(hv[i], df3));                            
                            h.setHorizontalAlignment (Element.ALIGN_LEFT);
                            h.setBorder(0);
                            h.setPaddingLeft(0);
                            h.setPaddingRight(0);
                            h.setPaddingTop(0.8f);
                            h.setPaddingBottom(0.8f);
                            tbl2.addCell(h);
                        }
                        
                        int ctr = retailbill.getRetailbillpurchase().size();
                        for (int j = 0; j < ctr; j++) {                               
                                RetailBillPurchase wbp = retailbill.getRetailbillpurchase().get(j);                                
                                String[] cv = {
                                    wbp.getProduct()+" "+wbp.getVariety(), 
                                    wbp.getMrp(),
                                    wbp.getQuantity()+" "+wbp.getUnit(),
                                    new DecimalFormat("#.##").format(Float.parseFloat(wbp.getAmount()))
                                };
                                for (int i = 0; i < 4; i++) {
                                    PdfPCell c = new PdfPCell (new Paragraph(cv[i], df3));
                                    c.setHorizontalAlignment (Element.ALIGN_LEFT);
                                    c.setVerticalAlignment(Element.ALIGN_CENTER);
                                    c.setPaddingLeft(0);
                                    c.setPaddingRight(0);
                                    c.setPaddingBottom(0);
                                    c.setPaddingTop(0.5f);
                                    c.setBorder(0);
                                    tbl2.addCell(c);
                                }
                        }
                        String[] fv = {"Total","","",new DecimalFormat("#.##").format(Float.parseFloat(retailbill.getPurchaseamt()))};
                        for (int i = 0; i < 4; i++) {
                            PdfPCell l = new PdfPCell (new Paragraph(fv[i], df3));
                            l.setHorizontalAlignment (Element.ALIGN_LEFT);
                            l.setBorder(0);
                            l.setPaddingLeft(0);
                            l.setPaddingRight(0);
                            l.setPaddingTop(0.7f);
                            l.setPaddingBottom(0.7f);
                            tbl2.addCell(l);
                        }                    

                    //bill details table
                        PdfPTable tbl3 = new PdfPTable(2);
                        tbl3.setWidthPercentage(100);
                        float vat=0f;
                        float dis=0f;
                        try {
                            if(Float.parseFloat(retailbill.getVatamt())>0){
                                vat=(Float.parseFloat(retailbill.getVatamt())*100)/Float.parseFloat(retailbill.getPurchaseamt());
                            }
                            dis=(Float.parseFloat(retailbill.getDiscount())*100)/Float.parseFloat(retailbill.getPurchaseamt());
                         } catch (Exception e) {
                            vat=0f;
                            dis=0f;
                         }
                        
                        PdfPCell bd1 = new PdfPCell (new Paragraph("GST Amt.("+new DecimalFormat("#.#").format(vat)+"%): ", df2));
                        bd1.setBorder(0);   
                        bd1.setPadding(0);
                        tbl3.addCell(bd1);
                        
                        PdfPCell bd2 = new PdfPCell (new Paragraph(new DecimalFormat("#.##").format(Float.parseFloat(retailbill.getVatamt())), df3));
                        bd2.setBorder(0);            
                        bd2.setPadding(0);
                        tbl3.addCell(bd2);

                        PdfPCell bd7 = new PdfPCell (new Paragraph("Discount : ("+new DecimalFormat("#.#").format(dis)+"%): ", df2));
                        bd7.setBorder(0);            
                        bd7.setPadding(0);
                        tbl3.addCell(bd7);

                        PdfPCell bd8 = new PdfPCell (new Paragraph(new DecimalFormat("#.##").format(Float.parseFloat(retailbill.getDiscount())), df3));
                        bd8.setBorder(0);            
                        bd8.setPadding(0);
                        tbl3.addCell(bd8);

                        PdfPCell bd9 = new PdfPCell (new Paragraph("Payable Amount : ", df2));
                        bd9.setBorder(0);            
                        bd9.setPadding(0);
                        tbl3.addCell(bd9);

                        PdfPCell bd10 = new PdfPCell (new Paragraph(new DecimalFormat("#").format(Float.parseFloat(retailbill.getPayableamt())), df3));
                        bd10.setBorder(0);            
                        bd10.setPadding(0);
                        tbl3.addCell(bd10);
                        
                        PdfPCell bd11 = new PdfPCell (new Paragraph("Paid Amount : ", df2));
                        bd11.setBorder(0);            
                        bd11.setPadding(0);
                        tbl3.addCell(bd11);

                        PdfPCell bd112 = new PdfPCell (new Paragraph(new DecimalFormat("#").format(Float.parseFloat(retailbill.getPaidamt())), df3));
                        bd112.setBorder(0);            
                        bd112.setPadding(0);
                        tbl3.addCell(bd112);
    
                       // writer.setPageEvent(new Watermark());
                        
            //Now Insert Every Thing Into PDF Document
                document.open();//PDF document opened........			       
                PdfPTable main = new PdfPTable(3);
                main.setWidthPercentage(100);
                        
                        PdfPTable main_1 = new PdfPTable(1);
                        main_1.setWidthPercentage(70);
                        main_1.addCell(new PdfPCell(headertbl)).setBorder(Rectangle.NO_BORDER);
                        main_1.addCell(new PdfPCell(headertbl1)).setBorder(Rectangle.NO_BORDER);
                        main_1.addCell(new PdfPCell(tbl1)).setBorder(Rectangle.NO_BORDER);
                        main_1.addCell(new PdfPCell(tbl2)).setBorder(Rectangle.NO_BORDER);
                        main_1.addCell(new PdfPCell(tbl3)).setBorder(Rectangle.NO_BORDER);
                        main.addCell(new PdfPCell(main_1)).setBorder(Rectangle.NO_BORDER);;
                        main.addCell(new PdfPCell()).setBorder(Rectangle.NO_BORDER);
                        main.addCell(new PdfPCell()).setBorder(Rectangle.NO_BORDER);
                        document.add(main);
//                        
//
//                document.add(headertbl);                
//                document.add(headertbl1);                
//                document.add(tbl1);
//                document.add(tbl2);
//                document.add(tbl3);
                
                
                
                document.close();
                
            //closing of pdf
                pdffile.close();
                
            return filename;
            
        } catch (Exception e) {
            e.printStackTrace();
            return null;            
        }
    }
    
    public boolean printPdf(String filename) {
        try {
            System.out.println(filename);
            FileInputStream fis = new FileInputStream(filename);
            PrintPdf printPDFFile = new PrintPdf(fis, "Billing");
            printPDFFile.print();
            
            
            
//            ProcessBuilder builder = new ProcessBuilder("cmd.exe", "/c", "clprint.exe /print /pdffile:\""+filename+"\"");            
//            builder.redirectErrorStream(true);
//            Process p = builder.start();
//        BufferedReader r = new BufferedReader(new InputStreamReader(p.getInputStream()));
//        String line;
//        while (true) {
//            line = r.readLine();
//            if (line == null) { break; }
//            System.out.println(line);
//        }
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }   
    
}
