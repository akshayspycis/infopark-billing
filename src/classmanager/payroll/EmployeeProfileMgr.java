package classmanager.payroll;

import datamanager.config.Config;
import datamanager.payroll.EmployeeProfile;


/**
 *
 * @author akshay
 */
public class EmployeeProfileMgr {

     // method to insert employee profile in database.
     public boolean insEmployeeProfile(EmployeeProfile employeeprofile) {
        try {          
            Config.sql = "insert into employeeprofile ("
                  
                    + "employeename,"                    
                    + "gender ,"
                    + "dob,"                    
                    + "age,"                    
                    + "fathername,"
                    + "maritalstatus,"                    
                    + "identitytype,"
                    + "identityno,"
                    + "contactno,"                    
                    + "altcontactno,"
                    + "address,"
                    + "locality,"
                    + "city,"
                    + "pincode,"
                    + "state,"
                    + "email,"
                    + "salary,"
                    + "joiningdate,"
                    + "leavingdate,"
                    + "other)"
                    
                    + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            Config.pstmt.setString(1, employeeprofile.getEmployeename());
            Config.pstmt.setString(2, employeeprofile.getGender());
            Config.pstmt.setString(3, employeeprofile.getDob());
            Config.pstmt.setString(4, employeeprofile.getAge());
            Config.pstmt.setString(5, employeeprofile.getFathername());
            Config.pstmt.setString(6, employeeprofile.getMaritalstatus());
            Config.pstmt.setString(7, employeeprofile.getIdentitytype());
            Config.pstmt.setString(8, employeeprofile.getIdentityno());
            Config.pstmt.setString(9, employeeprofile.getContactno());
            Config.pstmt.setString(10, employeeprofile.getAltcontactno());
            Config.pstmt.setString(11, employeeprofile.getAddress());
            Config.pstmt.setString(12, employeeprofile.getLocality());            
            Config.pstmt.setString(13, employeeprofile.getCity());            
            Config.pstmt.setString(14, employeeprofile.getPincode());            
            Config.pstmt.setString(15, employeeprofile.getState());            
            Config.pstmt.setString(16, employeeprofile.getEmail());            
            Config.pstmt.setString(17, employeeprofile.getSalary());            
            Config.pstmt.setString(18, employeeprofile.getJoiningdate());            
            Config.pstmt.setString(19, employeeprofile.getLeavingdate());
            Config.pstmt.setString(20, employeeprofile.getOther());
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadEmployeeProfile();
                return true;                
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
    //----------------------------------------------------------------------------------------------
    
   //method to update employee profile in database 
   public boolean updEmployeeProfile(EmployeeProfile employeeprofile) {
        try {
            Config.sql = "update employeeprofile set "
                    + "employeename = ?, "
                    + "gender = ?, "
                    + "dob = ?, "
                    + "age = ?, "
                    + "fathername = ?, "
                    + "maritalstatus = ?, "
                    + "identitytype = ?, "
                    + "identityno = ?, "
                    + "contactno = ?, "
                    + "altcontactno = ?, "
                    + "address = ?, "
                    + "locality = ?, "
                    + "city = ?, "
                    + "pincode = ?, "
                    + "state = ?, "
                    + "email = ?, "
                    + "salary = ?, "
                    + "joiningdate = ?, "
                    + "leavingdate = ?, "
                    + "other = ? "
                  
                    + " where employeeid = '"+employeeprofile.getEmployeeid()+"'";
            
            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, employeeprofile.getEmployeename());
            Config.pstmt.setString(2, employeeprofile.getGender());
            Config.pstmt.setString(3, employeeprofile.getDob());
            Config.pstmt.setString(4, employeeprofile.getAge());            
            Config.pstmt.setString(5, employeeprofile.getFathername());
            Config.pstmt.setString(6, employeeprofile.getMaritalstatus());
            Config.pstmt.setString(7, employeeprofile.getIdentitytype());
            Config.pstmt.setString(8, employeeprofile.getIdentityno());
            Config.pstmt.setString(9, employeeprofile.getContactno());            
            Config.pstmt.setString(10, employeeprofile.getAltcontactno());
            Config.pstmt.setString(11, employeeprofile.getAddress());
            Config.pstmt.setString(12, employeeprofile.getLocality());            
            Config.pstmt.setString(13, employeeprofile.getCity());            
            Config.pstmt.setString(14, employeeprofile.getPincode());            
            Config.pstmt.setString(15, employeeprofile.getState());            
            Config.pstmt.setString(16, employeeprofile.getEmail());            
            Config.pstmt.setString(17, employeeprofile.getSalary());            
            Config.pstmt.setString(18, employeeprofile.getJoiningdate());            
            Config.pstmt.setString(19, employeeprofile.getLeavingdate());            
            Config.pstmt.setString(20, employeeprofile.getOther());            
           
            int x = Config.pstmt.executeUpdate();
            
            if (x>0) {
                Config.configmgr.loadEmployeeProfile();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
   //----------------------------------------------------------------------------------------------
   
   //method to delete coustomerprofile in database
   public boolean delEmployeeProfile(String employeeid)
   {
        try {          
            Config.sql = "delete from employeeprofile where employeeid = '"+employeeid+"'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            
            int x = Config.pstmt.executeUpdate();

            if (x>0) {
                Config.configmgr.loadEmployeeProfile();
                return true;
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
   }
}
