package classmanager.stock;

import datamanager.config.Config;
import datamanager.stock.StockBillReturn;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author AMS
 */
public class StockBillReturnMgr {

    //method to insert return in database
    public boolean insStockBillReturn(ArrayList<StockBillReturn> stockbillreturn, String stockbillid) {
        
        try {
            int y = 0;
            for (int i = 0; i < stockbillreturn.size(); i++) {
                StockBillReturn sbr = stockbillreturn.get(i);
                
                Config.sql = "insert into stockbillreturn ("
                        + "stockbillid,"
                        + "product,"
                        + "flavor,"
                        + "contentrate,"
                        + "batchno,"
                        + "expdate,"
                        + "unit,"
                        + "mrp,"
                        + "quantity,"
                        + "pack,"
                        + "rate,"
                        + "amount,"
                        + "vatpercentage,"
                        + "scheme)"
                        + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                Config.pstmt = Config.conn.prepareStatement(Config.sql);

                Config.pstmt.setString(1, stockbillid);
                Config.pstmt.setString(2, sbr.getProduct());
                Config.pstmt.setString(3, sbr.getFlavor());
                Config.pstmt.setString(4, sbr.getContentrate());
                Config.pstmt.setString(5, sbr.getBatchno());
                Config.pstmt.setString(6, sbr.getExpdate());
                Config.pstmt.setString(7, sbr.getUnit());
                Config.pstmt.setString(8, sbr.getMrp());
                Config.pstmt.setString(9, sbr.getQuantity());
                Config.pstmt.setString(10, sbr.getPack());
                Config.pstmt.setString(11, sbr.getRate());
                Config.pstmt.setString(12, sbr.getAmount());
                Config.pstmt.setString(13, sbr.getVatpercentage());
                Config.pstmt.setString(14, sbr.getScheme());

                Config.pstmt.executeUpdate();
                y++;                
            }
            
            if (stockbillreturn.size() == y) {                
                return true;
            } else {
                return false;
            } 
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }        
    }
//===============================================================================================================
//===============================================================================================================
        
    //method to update return in database
    public boolean updStockBillReturn(ArrayList<StockBillReturn> stockbillreturn, String stockbillid) {
        
        try {
            Config.sql = "delete from stockbillreturn where stockbillid = '" + stockbillid + "'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.executeUpdate();
            
            int y = 0;
            for (int i = 0; i < stockbillreturn.size(); i++) {
                StockBillReturn sbr = stockbillreturn.get(i);
                
                Config.sql = "insert into stockbillreturn ("
                        + "stockbillid,"
                        + "product,"
                        + "flavor,"
                        + "contentrate,"
                        + "batchno,"
                        + "expdate,"
                        + "unit,"
                        + "mrp,"
                        + "quantity,"
                        + "pack,"
                        + "rate,"
                        + "amount,"
                        + "vatpercentage,"
                        + "scheme)"
                        + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                Config.pstmt = Config.conn.prepareStatement(Config.sql);

                Config.pstmt.setString(1, stockbillid);
                Config.pstmt.setString(2, sbr.getProduct());
                Config.pstmt.setString(3, sbr.getFlavor());
                Config.pstmt.setString(4, sbr.getContentrate());
                Config.pstmt.setString(5, sbr.getBatchno());
                Config.pstmt.setString(6, sbr.getExpdate());
                Config.pstmt.setString(7, sbr.getUnit());
                Config.pstmt.setString(8, sbr.getMrp());
                Config.pstmt.setString(9, sbr.getQuantity());
                Config.pstmt.setString(10, sbr.getPack());
                Config.pstmt.setString(11, sbr.getRate());
                Config.pstmt.setString(12, sbr.getAmount());
                Config.pstmt.setString(13, sbr.getVatpercentage());
                Config.pstmt.setString(14, sbr.getScheme());

                Config.pstmt.executeUpdate();
                y++;                
            }
            
            if (stockbillreturn.size() == y) {                
                return true;
            } else {
                return false;
            } 
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }        
    }
//===============================================================================================================
//===============================================================================================================
    
    //method to delete return in database
    public boolean delStockBillReturn(String stockbillid) {
        try {
            Config.sql = "delete from stockbillreturn where stockbillid = '" + stockbillid + "'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int z = Config.pstmt.executeUpdate();
            
            if (z >= 0) {                
                return true;                
            } else {
                return false;
            } 
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
