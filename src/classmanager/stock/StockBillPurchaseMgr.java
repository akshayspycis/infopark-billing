package classmanager.stock;

import datamanager.config.Config;
import datamanager.stock.StockBillPurchase;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author AMS
 */
public class StockBillPurchaseMgr {

    //method to insert purchase in database
    public boolean insStockBillPurchase(ArrayList<StockBillPurchase> stockbillpurchase, String stockbillid) {
        
        try {
            int y = 0;
            for (int i = 0; i < stockbillpurchase.size(); i++) {
                StockBillPurchase sbp = stockbillpurchase.get(i);
                
                Config.sql = "insert into stockbillpurchase ("
                        + "stockbillid,"
                        + "product,"
                        + "flavor,"
                        + "contentrate,"
                        + "batchno,"
                        + "expdate,"
                        + "unit,"
                        + "mrp,"
                        + "quantity,"
                        + "pack,"
                        + "rate,"
                        + "amount,"
                        + "vatpercentage,"
                        + "scheme)"
                        + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                Config.pstmt = Config.conn.prepareStatement(Config.sql);

                Config.pstmt.setString(1, stockbillid);
                Config.pstmt.setString(2, sbp.getProduct());
                Config.pstmt.setString(3, sbp.getFlavor());
                Config.pstmt.setString(4, sbp.getContentrate());
                Config.pstmt.setString(5, sbp.getBatchno());
                Config.pstmt.setString(6, sbp.getExpdate());
                Config.pstmt.setString(7, sbp.getUnit());
                Config.pstmt.setString(8, sbp.getMrp());
                Config.pstmt.setString(9, sbp.getQuantity());
                Config.pstmt.setString(10, sbp.getPack());
                Config.pstmt.setString(11, sbp.getRate());
                Config.pstmt.setString(12, sbp.getAmount());
                Config.pstmt.setString(13, sbp.getVatpercentage());
                Config.pstmt.setString(14, sbp.getScheme());

                Config.pstmt.executeUpdate();
                y++;                
            }
            
            if (stockbillpurchase.size() == y) {                
                return true;
            } else {
                return false;
            } 
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        
    }
//===============================================================================================================
//===============================================================================================================
    
    //method to insert purchase in database
    public boolean updStockBillPurchase(ArrayList<StockBillPurchase> stockbillpurchase, String stockbillid) {
        
        try {
            try {
                Config.sql = "delete from stockbillpurchase where stockbillid = '" + stockbillid + "'";
                Config.pstmt = Config.conn.prepareStatement(Config.sql);
                Config.pstmt.executeUpdate();
            } catch (Exception e) {}            
            
            int y = 0;
            for (int i = 0; i < stockbillpurchase.size(); i++) {
                StockBillPurchase sbp = stockbillpurchase.get(i);
                
                Config.sql = "insert into stockbillpurchase ("
                        + "stockbillid,"
                        + "product,"
                        + "flavor,"
                        + "contentrate,"
                        + "batchno,"
                        + "expdate,"
                        + "unit,"
                        + "mrp,"
                        + "quantity,"
                        + "pack,"
                        + "rate,"
                        + "amount,"
                        + "vatpercentage,"
                        + "scheme)"
                        + "values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                Config.pstmt = Config.conn.prepareStatement(Config.sql);

                Config.pstmt.setString(1, stockbillid);
                Config.pstmt.setString(2, sbp.getProduct());
                Config.pstmt.setString(3, sbp.getFlavor());
                Config.pstmt.setString(4, sbp.getContentrate());
                Config.pstmt.setString(5, sbp.getBatchno());
                Config.pstmt.setString(6, sbp.getExpdate());
                Config.pstmt.setString(7, sbp.getUnit());
                Config.pstmt.setString(8, sbp.getMrp());
                Config.pstmt.setString(9, sbp.getQuantity());
                Config.pstmt.setString(10, sbp.getPack());
                Config.pstmt.setString(11, sbp.getRate());
                Config.pstmt.setString(12, sbp.getAmount());
                Config.pstmt.setString(13, sbp.getVatpercentage());
                Config.pstmt.setString(14, sbp.getScheme());

                Config.pstmt.executeUpdate();
                y++;                
            }
            
            if (stockbillpurchase.size() == y) {                
                return true;
            } else {
                return false;
            } 
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        
    }
//===============================================================================================================
//===============================================================================================================
    
    //method to delete purchase in database
    public boolean delStockBillPurchase(String stockbillid) {
        try {
            Config.sql = "delete from stockbillpurchase where stockbillid = '" + stockbillid + "'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int y = Config.pstmt.executeUpdate();
            
            if (y >= 0) {                
                return true;
            } else {
                return false;
            } 
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }    
//===============================================================================================================
//===============================================================================================================
}
