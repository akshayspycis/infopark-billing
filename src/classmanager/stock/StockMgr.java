package classmanager.stock;
import datamanager.config.Config;
import datamanager.stock.Stock;
import datamanager.wholesale.WholesaleBillPurchase;
import java.sql.ResultSet;
import java.sql.SQLException;

public class StockMgr {
    //method to update stock
    public boolean updStock() {
        try {
            Config.sql = "truncate stock";
            Config.stmt.addBatch(Config.sql);
            Config.sql = "insert into stock (product, flavor, contentrate, batchno, expdate, unit, pack, mrp, rate, vatpercentage) select product, flavor, contentrate, batchno, expdate, unit, pack, mrp, rate, vatpercentage from stockbillpurchase group by product, flavor, contentrate, batchno,mrp";
            Config.stmt.addBatch(Config.sql);
            Config.sql = "update stock u,stock s\n" +
                            "set u.available =\n" +
                            "(select COALESCE(sum(quantity),0) from stockbillpurchase sp\n" +
                            "where sp.product =s.product and sp.flavor =s.flavor\n" +
                            "and sp.contentrate = s.contentrate and sp.batchno = s.batchno and sp.mrp=s.mrp)\n" +
                            "+(select COALESCE(sum(quantity),0) from stockbillreturn sp\n" +
                            "where sp.product =s.product and sp.flavor =s.flavor\n" +
                            "and sp.contentrate = s.contentrate and sp.batchno = s.batchno and sp.mrp=s.mrp)\n" +
                            "-(select COALESCE(sum(quantity),0) from wholesalebillpurchase sp\n" +
                            "where sp.product =s.product and sp.flavor =s.flavor\n" +
                            "and sp.contentrate = s.contentrate and sp.batchno = s.batchno and sp.mrp=s.mrp)\n" +
                            "+(select COALESCE(sum(quantity),0) from wholesalebillreturn sp\n" +
                            "where sp.product =s.product and sp.flavor =s.flavor\n" +
                            "and sp.contentrate = s.contentrate and sp.batchno = s.batchno and sp.mrp=s.mrp)\n" +
                            "where u.stockid=s.stockid";
            Config.stmt.addBatch(Config.sql);
            Config.stmt.executeBatch();
            return true;
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    public boolean subStockArray (WholesaleBillPurchase wbp){
        for (int i = 0; i < Config.config_stock.size(); i++) {
            Stock s=  Config.config_stock.get(i);
            if(s.getProduct().equals(wbp.getProduct()) && s.getFlavor().equals(wbp.getFlavor()) && s.getContentrate().equals(wbp.getContentrate()) && s.getBatchno().equals(wbp.getBatchno()) && s.getMrp().equals(wbp.getMrp()) ){
                int total=Integer.parseInt(s.getAvailable())-Integer.parseInt(wbp.getQuantity());
                s.setAvailable(String.valueOf(total));
                Config.config_stock.set(i, s);
            }
        }
        return true;
    }
    public boolean addStockArray (WholesaleBillPurchase wbp){
        for (int i = 0; i < Config.config_stock.size(); i++) {
            Stock s=  Config.config_stock.get(i);
            if(s.getProduct().equals(wbp.getProduct()) && s.getFlavor().equals(wbp.getFlavor()) && s.getContentrate().equals(wbp.getContentrate()) && s.getBatchno().equals(wbp.getBatchno()) && s.getMrp().equals(wbp.getMrp()) ){
                int total=Integer.parseInt(s.getAvailable())-Integer.parseInt(wbp.getQuantity());
                s.setAvailable(String.valueOf(total));
                Config.config_stock.set(i, s);
            }
        }
        return true;
    }
    
    
//===============================================================================================================
}