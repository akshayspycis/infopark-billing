package classmanager.stock;

import datamanager.config.Config;
import datamanager.stock.StockBill;
import datamanager.supplier.SupplierProfile;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class StockBillMgr {

    //method to insert complete bill in database
    public boolean insStockBill(StockBill stockbill) {
        
        try {
            //to fetch supplier id
            String supplierid = null;            
            System.out.println( Config.sdf.format(Config.cal.getTime()) );    
            if (stockbill.getSupplierprofile().getSupplierid().equals("")) {                
                SupplierProfile sp = new SupplierProfile();
                sp.setOrganization(stockbill.getSupplierprofile().getOrganization());
                sp.setSuppliername(stockbill.getSupplierprofile().getSuppliername());
                sp.setTinno(stockbill.getSupplierprofile().getTinno());
                
                if (Config.supplierprofilemgr.insSupplierProfile(sp)) {
                    Config.sql = "select max(supplierid) supplierid from supplierprofile";
                    Config.rs = Config.stmt.executeQuery(Config.sql);
                    while (Config.rs.next()) {
                        supplierid = Config.rs.getString("supplierid");
                    }
                }
            } else {
                supplierid = stockbill.getSupplierprofile().getSupplierid();
            }
        //===============================================================================================================
                    
            //to insert bill entry
            Config.sql = "insert into stockbill ("
                    + "supplierid,"
                    + "billno,"
                    + "date,"                        
                    + "salesman,"
                    + "purchaseamt,"
                    + "schemeamt,"
                    + "vatamt,"
                    + "discountamt,"
                    + "payableamt,"
                    + "paidamt,"
                    + "balanceamt)"
                    + "values (?,?,?,?,?,?,?,?,?,?,?)";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            Config.pstmt.setString(1, supplierid);
            Config.pstmt.setString(2, stockbill.getBillno());
            Config.pstmt.setString(3, stockbill.getDate());                
            Config.pstmt.setString(4, stockbill.getSalesman());
            Config.pstmt.setString(5, stockbill.getPurchaseamt());
            Config.pstmt.setString(6, stockbill.getSchemeamt());
            Config.pstmt.setString(7, stockbill.getVatamt());
            Config.pstmt.setString(8, stockbill.getDiscountamt());
            Config.pstmt.setString(9, stockbill.getPayableamt());
            Config.pstmt.setString(10, stockbill.getPaidamt());
            Config.pstmt.setString(11, stockbill.getBalanceamt());

            int x = Config.pstmt.executeUpdate();            
        //===============================================================================================================
            
            //to fetch stockbillid
            String stockbillid = null;
            Config.sql = "Select max(stockbillid) stockbillid from stockbill";
            Config.rs = Config.stmt.executeQuery(Config.sql);
            if (Config.rs.next()) {
                stockbillid = Config.rs.getString("stockbillid");
            }            
        //===============================================================================================================

            //to insert purchase entry and return entry            
            if (x > 0 && Config.stockbillpurchasemgr.insStockBillPurchase(stockbill.getStockbillpurchase(),stockbillid) && Config.stockbillreturnmgr.insStockBillReturn(stockbill.getStockbillreturn(), stockbillid)) {
                Config.stockmgr.updStock();
                System.out.println( "upa"+Config.sdf.format(Config.cal.getTime()) );    
                Config.configmgr.loadStock();
                System.out.println( "load"+Config.sdf.format(Config.cal.getTime()) );    
                return true;
            } else {
                return false;
            }
        //===============================================================================================================
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
    }
//===============================================================================================================
//===============================================================================================================
    
    
    //method to update complete bill in database
    public boolean updStockBill(StockBill stockbill) {
        try {            
            //to fetch supplier id
            String supplierid = null;            
            if (stockbill.getSupplierprofile().getSupplierid().equals("")) {                
                SupplierProfile sp = new SupplierProfile();
                sp.setOrganization(stockbill.getSupplierprofile().getOrganization());
                sp.setSuppliername(stockbill.getSupplierprofile().getSuppliername());
                sp.setTinno(stockbill.getSupplierprofile().getTinno());
                
                if (Config.supplierprofilemgr.insSupplierProfile(sp)) {
                    Config.sql = "select max(supplierid) supplierid from supplierprofile";
                    Config.rs = Config.stmt.executeQuery(Config.sql);
                    while (Config.rs.next()) {
                        supplierid = Config.rs.getString("supplierid");
                    }
                }
            } else {
                supplierid = stockbill.getSupplierprofile().getSupplierid();
            }
        //===============================================================================================================
            
            //to update bill entry
            Config.sql = "update stockbill set "
                    + "supplierid = ?, "
                    + "billno = ?, "
                    + "date = ?, "                        
                    + "salesman = ?, "
                    + "purchaseamt = ?, "
                    + "schemeamt = ?, "
                    + "vatamt = ?, "
                    + "discountamt = ?, "
                    + "payableamt = ?, "
                    + "paidamt = ?, "
                    + "balanceamt = ? where stockbillid = '" + stockbill.getStockbillid() + "'";

            Config.pstmt = Config.conn.prepareStatement(Config.sql);

            Config.pstmt.setString(1, supplierid);
            Config.pstmt.setString(2, stockbill.getBillno());
            Config.pstmt.setString(3, stockbill.getDate());                
            Config.pstmt.setString(4, stockbill.getSalesman());
            Config.pstmt.setString(5, stockbill.getPurchaseamt());
            Config.pstmt.setString(6, stockbill.getSchemeamt());
            Config.pstmt.setString(7, stockbill.getVatamt());
            Config.pstmt.setString(8, stockbill.getDiscountamt());
            Config.pstmt.setString(9, stockbill.getPayableamt());
            Config.pstmt.setString(10, stockbill.getPaidamt());
            Config.pstmt.setString(11, stockbill.getBalanceamt());

            int x = Config.pstmt.executeUpdate();
        //===============================================================================================================
            
            //to update purchase entry, and return entry            
            if (x > 0 && Config.stockbillpurchasemgr.updStockBillPurchase(stockbill.getStockbillpurchase(), stockbill.getStockbillid()) && Config.stockbillreturnmgr.updStockBillReturn(stockbill.getStockbillreturn(), stockbill.getStockbillid())) {
                Config.stockmgr.updStock();
                Config.configmgr.loadStock();
                return true;
            } else {
                return false;
            }
        //===============================================================================================================
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }        
    }
//===============================================================================================================
//===============================================================================================================
    
    
    //method to delete complete bill in database
    public boolean delStockBill(String stockbillid) {
        try {
            Config.sql = "delete from stockbill where stockbillid = '" + stockbillid + "'";
            Config.pstmt = Config.conn.prepareStatement(Config.sql);
            int x = Config.pstmt.executeUpdate();            

            if (x >= 0 && Config.stockbillpurchasemgr.delStockBillPurchase(stockbillid) && Config.stockbillreturnmgr.delStockBillReturn(stockbillid)) {
                Config.stockmgr.updStock();
                Config.configmgr.loadStock();
                return true;
            } else {
                return false;
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
