package classmanager.stock;
/**
 *
 * @author akshay
 */
import datamanager.config.Config;
import datamanager.stock.TempPriceDetails;
import java.sql.SQLException;
import java.util.ArrayList;
/**
 *
 * @author Akshay
 */
public class TempPriceDetailsMgr {
    //method to insert purchase in database
    public boolean checkPriceDetails(TempPriceDetails temp_price_details){
        int i=0;
        for (i = 0; i < Config.temp_price_details.size(); i++) {
            TempPriceDetails  rs=Config.temp_price_details.get(i);
            if(rs.getProduct().equals(temp_price_details.getProduct())
                    &&rs.getFlavor().equals(temp_price_details.getFlavor()) 
                    &&rs.getBatchno().equals(temp_price_details.getBatchno())
            ){
                if(!updTempPriceDetails(temp_price_details,i))return false;
                break;
            }
        }
        if(i==Config.temp_price_details.size()){
               if(!insTempPriceDetails(temp_price_details))return false;
        }
        return true;
    }
    public boolean insTempPriceDetails(TempPriceDetails temp_price_details) {
        try {
                Config.sql = "insert into temp_price_details ("
                        + "product,"
                        + "flavor,"
                        + "contentrate,"
                        + "batchno,"
                        + "expdate,"
                        + "unit,"
                        + "mrp,"
                        + "rate)"
                        + "values (?,?,?,?,?,?,?,?)";
                Config.pstmt = Config.conn.prepareStatement(Config.sql);
                Config.pstmt.setString(1, temp_price_details.getProduct());
                Config.pstmt.setString(2, temp_price_details.getFlavor());
                Config.pstmt.setString(3, temp_price_details.getContentrate());
                Config.pstmt.setString(4, temp_price_details.getBatchno());
                Config.pstmt.setString(5, temp_price_details.getExpdate());
                Config.pstmt.setString(6, temp_price_details.getUnit());
                Config.pstmt.setString(7, temp_price_details.getMrp());
                Config.pstmt.setString(8, temp_price_details.getRate());
            if (Config.pstmt.executeUpdate()> 0) {                
                insTempPriceDetailsInArrayList(temp_price_details);
                return true;
            } else {
                return false;
            } 
            
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
        
    }
//===============================================================================================================
//===============================================================================================================
    
    //method to insert purchase in database
    public boolean updTempPriceDetails(TempPriceDetails temp_price_details,int index) {
        try {
                Config.sql = "update temp_price_details set "
                        + "expdate=?,"
                        + "unit=?,"
                        + "mrp=?"
                        + " where product=? and flavor=? and batchno=?";
                Config.pstmt = Config.conn.prepareStatement(Config.sql);
                Config.pstmt.setString(1, temp_price_details.getExpdate());
                Config.pstmt.setString(2, temp_price_details.getUnit());
                Config.pstmt.setString(3, temp_price_details.getMrp());
                Config.pstmt.setString(4, temp_price_details.getProduct());
                Config.pstmt.setString(5, temp_price_details.getFlavor());
                Config.pstmt.setString(6, temp_price_details.getBatchno());
                System.out.println(Config.pstmt.toString());
            if (Config.pstmt.executeUpdate() >0) {                
                updTempPriceDetailsInArrayList(temp_price_details,index);
                return true;
            } else {
                return false;
            } 
        } catch (SQLException ex) {
            ex.printStackTrace();
            return false;
        }
    }
    public boolean insTempPriceDetailsInArrayList(TempPriceDetails temp_price_details) {
        Config.temp_price_details.add(temp_price_details);
        return true;
    }
    public boolean updTempPriceDetailsInArrayList(TempPriceDetails temp_price_details,int index) {
        Config.temp_price_details.set(index,temp_price_details);
        return true;
    }
}
