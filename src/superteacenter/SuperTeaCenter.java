package superteacenter;

import classmanager.config.ConfigMgr;
import datamanager.billing.BillingDetails;
import datamanager.config.Config;
import static java.lang.Thread.sleep;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 * @author AMS
 */
public class SuperTeaCenter {

    static banner banner;
    
    public SuperTeaCenter() {
        banner = new banner();
        Config.configmgr = new ConfigMgr();
    }    
    
    private void showbanner() {        
        banner.setVisible(true);
        banner.setBannerLabel("Checking system configuration...");
    }
    
    private void connserver() {
        banner.setBannerLabel("Connecting to server...");        
    }
    
    private void conndatabase() {
        banner.setBannerLabel("Checking databse connection...");
        if (!Config.configmgr.loadDatabase()) {
            JOptionPane.showMessageDialog(banner,"Database connectivity problem, product can not be launch.","Error : 10068.",JOptionPane.ERROR_MESSAGE);            
            System.exit(0);
        }
    }
    
    private void initcomponent() {
        banner.setBannerLabel("Initialising components...");        
        int x=0;
        if (Config.configmgr.loadUserProfile()){x++;} else {System.out.println("Error : 1, UserProfile loading problem.");}
        if (Config.configmgr.loadProduct()){x++;} else {System.out.println("Error : 2, Product loading problem.");}
        if (Config.configmgr.loadRProduct()){x++;} else {System.out.println("Error : 3, Product loading problem.");}
        if (Config.configmgr.loadFlavor()){x++;} else {System.out.println("Error : 4, Flavor loading problem.");}
        if (Config.configmgr.loadRVariety()){x++;} else {System.out.println("Error : 5, Product loading problem.");}
        if (Config.configmgr.loadContentRate()){x++;} else {System.out.println("Error : 6, Content Rate loading problem.");}
        if (Config.configmgr.loadScheme()){x++;} else {System.out.println("Error : 7, Scheme loading problem.");}
        if (Config.configmgr.loadProfit()){x++;} else {System.out.println("Error : 8, Profit loading problem.");}        
        if (Config.configmgr.loadCustomerProfile()){x++;} else {System.out.println("Error : 9, CustomerProfile loading problem.");}
        if (Config.configmgr.loadEmployeeProfile()){x++;} else {System.out.println("Error : 10, EmployeeProfile loading problem.");}
        if (Config.configmgr.loadSupplierProfile()){x++;} else {System.out.println("Error : 11, SupplierProfile loading problem.");}
        if (Config.configmgr.loadOtherConfig()){x++;} else {System.out.println("Error : 12, other configration loading problem.");}
        if (Config.configmgr.loadClassManager()){x++;} else {System.out.println("Error : 13, ClassManager loading problem.");}
        if (Config.configmgr.loadForms()){x++;} else {System.out.println("Error : 14, Forms loading problem.");}
        if (Config.stockmgr.updStock()) {x++;} else {System.out.println("Error : 15, Stock update problem.");}
        if (Config.configmgr.loadStock()){x++;} else {System.out.println("Error : 16, Stock loading problem.");}
        if (Config.configmgr.loadTempPriceDetails()){x++;} else {System.out.println("Error : 17, Temp Price loading problem.");}
        if (Config.retail_stock_mgr.updRetailStock()) {x++;} else {System.out.println("Error : 18, Retail Stock update problem.");}
        if (Config.configmgr.loadRetailStock()){x++;} else {System.out.println("Error : 19, Retail Stock loading problem.");}
        if (Config.configmgr.loadBillingDetails()) {x++;} else {System.out.println("Error : 20, Billing details problem.");}
        if (Config.configmgr.loadWholesaleGst()) {x++;} else {System.out.println("Error : 21, Gst details problem.");}
        if (Config.configmgr.loadRetailGst()) {x++;} else {System.out.println("Error : 22, RGst details problem.");}
//        BillingDetails b=new BillingDetails();
//        Calendar cal = Calendar.getInstance();
//        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
//        String strDate = sdf.format(cal.getTime());
//        if(Config.billing_details.get(0).getStart_date().equals("")){
//            b.setStart_date(strDate);
//            b.setRenew_date(strDate);
//            cal.add(Calendar.DATE, 10);
//            b.setEnd_date(sdf.format(cal.getTime()));
//            b.setLast_date(strDate);
//            Config.billing_details_mgr.updBillingDate(b);
//            
//        }
//        try {
//            Date start=sdf.parse(Config.billing_details.get(0).getStart_date());
//            Date last=sdf.parse(Config.billing_details.get(0).getLast_date());
//            Date end=sdf.parse(Config.billing_details.get(0).getEnd_date());
//            Date renew=sdf.parse(Config.billing_details.get(0).getRenew_date());
//            
//            if((sdf.parse(strDate).after(start)) || strDate.equals(Config.billing_details.get(0).getStart_date())  && (sdf.parse(strDate).after(renew) || (strDate.equals(Config.billing_details.get(0).getRenew_date()))) ){
//                if(sdf.parse(strDate).after(last) || strDate.equals(Config.billing_details.get(0).getLast_date())){
//                    if((end.after(sdf.parse(strDate))) || strDate.equals(Config.billing_details.get(0).getEnd_date())){
//                        long diff = end.getTime()- sdf.parse(strDate).getTime() ;
//                        long day = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS) ;
//                        if(day<30){
//                            JOptionPane.showMessageDialog(banner,"You have approx "+day+" days left on your current subscription and have purchased and Please contact your system administrator.","Alert",JOptionPane.NO_OPTION);
//                        }
                        if (x!=22) {
                            JOptionPane.showMessageDialog(banner,"Product initialisation problem, product can not be launch."+x,"Error.",JOptionPane.ERROR_MESSAGE);
                            System.exit(0);
                        }
//                    }else{
//                        JOptionPane.showMessageDialog(banner,"Product has expired and cannot be used to activate this product. Please contact your system administrator.","Error.",JOptionPane.ERROR_MESSAGE);
//                        System.exit(0);
//                    }
//                }else{
//                    JOptionPane.showMessageDialog(banner,"Your clock is behind","Error.",JOptionPane.ERROR_MESSAGE);
//                    System.exit(0);
//                }
//                
//            }else{
//                JOptionPane.showMessageDialog(banner,"Your clock is behind","Error.",JOptionPane.ERROR_MESSAGE);
//                System.exit(0);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }
    
    private void hidebanner() {
        banner.dispose();
        Config.login.onloadReset();
        Config.login.setVisible(true);
        
    }
    
    public static void main(String[] args) {
        try {        
            UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SuperTeaCenter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            Logger.getLogger(SuperTeaCenter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            Logger.getLogger(SuperTeaCenter.class.getName()).log(Level.SEVERE, null, ex);
        } catch (UnsupportedLookAndFeelException ex) {
            Logger.getLogger(SuperTeaCenter.class.getName()).log(Level.SEVERE, null, ex);
        }               
        
        final SuperTeaCenter obj = new SuperTeaCenter();
        Thread t1 = new Thread() {
           public void run() {
                   loadAutoData();    
            }
           private void loadAutoData() {
                       try {            
            
            obj.showbanner();        
            Thread.sleep(200);
            obj.connserver();
            Thread.sleep(200);
            obj.conndatabase();
            //Thread.sleep(200);
            obj.initcomponent();
            //Thread.sleep(200);
            obj.hidebanner();            
        } catch (InterruptedException ex) {
            Logger.getLogger(SuperTeaCenter.class.getName()).log(Level.SEVERE, null, ex);
        } 

           }
        };

        
        
        Thread t2 = new Thread() {
           public void run() {
               int i=1;
           while(true){
                   try {
                       sleep(100);
                   } catch (InterruptedException ex) {
                       Logger.getLogger(SuperTeaCenter.class.getName()).log(Level.SEVERE, null, ex);
                   }
                   if(!Config.check){
                        banner.setBanner(i);
                   }else{
                        banner.close();
                     break;  
                   }    
                   if(i==23){
                       i=1;
                   }else{
                       i++;
                   }
                }
            }
           
        };
        t1.start();
        t2.start();

    }
}
