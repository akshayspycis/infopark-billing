package superteacenter;

import java.awt.Toolkit;

/**
 *
 * @author AMS
 */
public class banner extends javax.swing.JFrame {
    /**
     * Creates new form banner
     */
    public banner() {
        initComponents();        
        this.setLocationRelativeTo(null);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/icon.png")));
    }
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lblShow = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        getContentPane().setLayout(null);

        lblShow.setForeground(new java.awt.Color(255, 255, 255));
        lblShow.setText("...");
        getContentPane().add(lblShow);
        lblShow.setBounds(410, 230, 170, 20);

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/banner.png"))); // NOI18N
        getContentPane().add(jLabel1);
        jLabel1.setBounds(0, 0, 590, 255);

        setSize(new java.awt.Dimension(590, 255));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
   
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel lblShow;
    // End of variables declaration//GEN-END:variables
    
    void setBannerLabel(String lable) {
        lblShow.setText(lable);
    }
    void close(){
        dispose();
    }
    void setBanner(int i){
        try {
            String str ="/images/"+i+".png";
            jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/"+i+".png"))); // NOI18N
        } catch (Exception e) {
        }
        
    }
}
