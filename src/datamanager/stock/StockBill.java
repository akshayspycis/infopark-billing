package datamanager.stock;

import datamanager.supplier.SupplierProfile;
import java.util.ArrayList;

public class StockBill {
    
    String stockbillid = null;    
    SupplierProfile supplierprofile = null;    
    String billno = null;
    String date = null;    
    String salesman = null;    
    String purchaseamt = null;    
    String schemeamt = null;
    String vatamt = null;
    String discountamt = null;
    String payableamt = null;
    String paidamt = null;
    String balanceamt = null;    
    ArrayList<StockBillPurchase> stockbillpurchase = null;
    ArrayList<StockBillReturn> stockbillreturn = null;

    public String getStockbillid() {
        return stockbillid;
    }

    public void setStockbillid(String stockbillid) {
        this.stockbillid = stockbillid;
    }

    public SupplierProfile getSupplierprofile() {
        return supplierprofile;
    }

    public void setSupplierprofile(SupplierProfile supplierprofile) {
        this.supplierprofile = supplierprofile;
    }

    public String getBillno() {
        return billno;
    }

    public void setBillno(String billno) {
        this.billno = billno;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSalesman() {
        return salesman;
    }

    public void setSalesman(String salesman) {
        this.salesman = salesman;
    }

    public String getPurchaseamt() {
        return purchaseamt;
    }

    public void setPurchaseamt(String purchaseamt) {
        this.purchaseamt = purchaseamt;
    }

    public String getSchemeamt() {
        return schemeamt;
    }

    public void setSchemeamt(String schemeamt) {
        this.schemeamt = schemeamt;
    }

    public String getVatamt() {
        return vatamt;
    }

    public void setVatamt(String vatamt) {
        this.vatamt = vatamt;
    }

    public String getDiscountamt() {
        return discountamt;
    }

    public void setDiscountamt(String discountamt) {
        this.discountamt = discountamt;
    }

    public String getPayableamt() {
        return payableamt;
    }

    public void setPayableamt(String payableamt) {
        this.payableamt = payableamt;
    }

    public String getPaidamt() {
        return paidamt;
    }

    public void setPaidamt(String paidamt) {
        this.paidamt = paidamt;
    }

    public String getBalanceamt() {
        return balanceamt;
    }

    public void setBalanceamt(String balanceamt) {
        this.balanceamt = balanceamt;
    }

    public ArrayList<StockBillPurchase> getStockbillpurchase() {
        return stockbillpurchase;
    }

    public void setStockbillpurchase(ArrayList<StockBillPurchase> stockbillpurchase) {
        this.stockbillpurchase = stockbillpurchase;
    }

    public ArrayList<StockBillReturn> getStockbillreturn() {
        return stockbillreturn;
    }

    public void setStockbillreturn(ArrayList<StockBillReturn> stockbillreturn) {
        this.stockbillreturn = stockbillreturn;
    }
}
