package datamanager.stock;

public class Stock {
    
    String stockid = null;
    String product = null;
    String flavor = null;
    String contentrate = null;
    String batchno = null;
    String expdate = null;
    String unit = null;
    String pack = null;
    String mrp = null;
    String rate = null;    
    String vatpercentage = null;    
    String available = null;

    public String getStockid() {
        return stockid;
    }

    public void setStockid(String stockid) {
        this.stockid = stockid;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getFlavor() {
        return flavor;
    }

    public void setFlavor(String flavor) {
        this.flavor = flavor;
    }

    public String getContentrate() {
        return contentrate;
    }

    public void setContentrate(String contentrate) {
        this.contentrate = contentrate;
    }

    public String getBatchno() {
        return batchno;
    }

    public void setBatchno(String batchno) {
        this.batchno = batchno;
    }

    public String getExpdate() {
        return expdate;
    }

    public void setExpdate(String expdate) {
        this.expdate = expdate;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getPack() {
        return pack;
    }

    public void setPack(String pack) {
        this.pack = pack;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getVatpercentage() {
        return vatpercentage;
    }

    public void setVatpercentage(String vatpercentage) {
        this.vatpercentage = vatpercentage;
    }

    public String getAvailable() {
        return available;
    }

    public void setAvailable(String available) {
        this.available = available;
    }

}
