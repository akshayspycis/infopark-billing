/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datamanager.Support;

/**
 *
 * @author Akshay
 */
public class SupportWholeSaleBill {
    String billid="";
    String locality="";
    String org="";
    String cus="";
    String con="";
    String date="";
    String billno="";
    String payableamt="";
    String paidamt="";
    String balanceamt="";
    String print="";
    String salesman="";

    public String getBillid() {
        return billid;
    }

    public void setBillid(String billid) {
        this.billid = billid;
    }

    public String getLocality() {
        return locality;
    }

    public void setLocality(String locality) {
        this.locality = locality;
    }

    public String getOrg() {
        return org;
    }

    public void setOrg(String org) {
        this.org = org;
    }

    public String getCus() {
        return cus;
    }

    public void setCus(String cus) {
        this.cus = cus;
    }

    public String getCon() {
        return con;
    }

    public void setCon(String con) {
        this.con = con;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getBillno() {
        return billno;
    }

    public void setBillno(String billno) {
        this.billno = billno;
    }

    public String getPayableamt() {
        return payableamt;
    }

    public void setPayableamt(String payableamt) {
        this.payableamt = payableamt;
    }

    public String getPaidamt() {
        return paidamt;
    }

    public void setPaidamt(String paidamt) {
        this.paidamt = paidamt;
    }

    public String getBalanceamt() {
        return balanceamt;
    }

    public void setBalanceamt(String balanceamt) {
        this.balanceamt = balanceamt;
    }

    public String getPrint() {
        return print;
    }

    public void setPrint(String print) {
        this.print = print;
    }

    public String getSalesman() {
        return salesman;
    }

    public void setSalesman(String salesman) {
        this.salesman = salesman;
    }

        
}
