package datamanager.config;

public class ConfigFlavor {
    String flavorname = null;
    String productname = null;

    public String getFlavorname() {
        return flavorname;
    }

    public void setFlavorname(String flavorname) {
        this.flavorname = flavorname;
    }

    public String getProductname() {
        return productname;
    }

    public void setProductname(String productname) {
        this.productname = productname;
    }    
}
