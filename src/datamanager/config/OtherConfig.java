/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datamanager.config;

/**
 *
 * @author akshay
 */
public class OtherConfig {
    String wholesale_stock_vat ="";
    String retail_stock_vat ="";
    String wholesale_discount ="";
    String retail_discount ="";
    String retail_billing_stock ="";
    String wholesale_billing_stock ="";
    String pos_58 ="";
    String pos_80 ="";
    String pos_a4 ="";
    String wholesale_bill_type ="";

    public String getWholesale_stock_vat() {
        return wholesale_stock_vat;
    }

    public void setWholesale_stock_vat(String wholesale_stock_vat) {
        this.wholesale_stock_vat = wholesale_stock_vat;
    }

    public String getRetail_stock_vat() {
        return retail_stock_vat;
    }

    public void setRetail_stock_vat(String retail_stock_vat) {
        this.retail_stock_vat = retail_stock_vat;
    }

    public String getWholesale_discount() {
        return wholesale_discount;
    }

    public void setWholesale_discount(String wholesale_discount) {
        this.wholesale_discount = wholesale_discount;
    }

    public String getRetail_discount() {
        return retail_discount;
    }

    public void setRetail_discount(String retail_discount) {
        this.retail_discount = retail_discount;
    }

    public String getRetail_billing_stock() {
        return retail_billing_stock;
    }

    public void setRetail_billing_stock(String retail_billing_stock) {
        this.retail_billing_stock = retail_billing_stock;
    }

    public String getWholesale_billing_stock() {
        return wholesale_billing_stock;
    }

    public void setWholesale_billing_stock(String wholesale_billing_stock) {
        this.wholesale_billing_stock = wholesale_billing_stock;
    }

    public String getPos_58() {
        return pos_58;
    }

    public void setPos_58(String pos_58) {
        this.pos_58 = pos_58;
    }

    public String getPos_80() {
        return pos_80;
    }

    public void setPos_80(String pos_80) {
        this.pos_80 = pos_80;
    }

    public String getPos_a4() {
        return pos_a4;
    }

    public void setPos_a4(String pos_a4) {
        this.pos_a4 = pos_a4;
    }

    public String getWholesale_bill_type() {
        return wholesale_bill_type;
    }

    public void setWholesale_bill_type(String wholesale_bill_type) {
        this.wholesale_bill_type = wholesale_bill_type;
    }          
}
