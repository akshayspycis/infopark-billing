package datamanager.config;

public class ConfigContentRate {
    
    String contentrate = null;

    public String getContentrate() {
        return contentrate;
    }

    public void setContentrate(String contentrate) {
        this.contentrate = contentrate;
    }
}
