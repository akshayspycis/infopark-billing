package datamanager.config;

import classmanager.barcode_reader.barcode.ReadMgr;
import classmanager.billing.BillingDetailsMgr;
import classmanager.config.ConfigContentRateMgr;
import classmanager.config.ConfigFlavorMgr;
import classmanager.config.ConfigMgr;
import classmanager.config.ConfigProductMgr;
import classmanager.config.ConfigProfitMgr;
import classmanager.config.ConfigRProductMgr;
import classmanager.config.ConfigRVarietyMgr;
import classmanager.config.ConfigSchemeMgr;
import classmanager.config.ConfigUserProfileMgr;
import classmanager.config.OtherConfigMgr;
import classmanager.config.RetailGstMgr;
import classmanager.config.WholesaleGstMgr;
import classmanager.customer.CustomerProfileMgr;
import classmanager.payroll.EmployeeProfileMgr;
import classmanager.payroll.PayrollMgr;
import classmanager.reatail_stock.RetailStockBillMgr;
import classmanager.reatail_stock.RetailStockMgr;
import classmanager.reatail_stock.RetailStockPurchaseMgr;
import classmanager.retail.RetailBillMgr;
import classmanager.retail.RetailBillPurchaseMgr;
import classmanager.retail.RetailBillReturnMgr;
import classmanager.stock.StockBillMgr;
import classmanager.stock.StockBillPurchaseMgr;
import classmanager.stock.StockBillReturnMgr;
import classmanager.stock.StockMgr;
import classmanager.stock.TempPriceDetailsMgr;
import classmanager.supplier.SupplierProfileMgr;
import classmanager.wholesale.WholesaleBillMgr;
import classmanager.wholesale.WholesaleBillPurchaseMgr;
import classmanager.wholesale.WholesaleBillReturnMgr;
import datamanager.billing.BillingDetails;
import datamanager.customer.CustomerProfile;
import datamanager.payroll.EmployeeProfile;
import datamanager.payroll.PayRoll;
import datamanager.retail_stock.RetailStock;
import datamanager.stock.Stock;
import datamanager.stock.TempPriceDetails;
import datamanager.supplier.SupplierProfile;
import datamanager.wholesale.WholesaleBill;
import datamanager.wholesale.WholesaleBillPurchase;
import datamanager.wholesale.WholesaleBillReturn;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import javax.swing.JTextField;
import modules.account.Login;
import modules.account.ManageAccount;
import modules.config.loading.Loading;
import modules.configuration.ProductConfiguration;
import modules.customer.CustomerManagement;
import modules.customer.NewCustomer;
import modules.customer.ViewCustomer;
import modules.home.Home;
import modules.payroll.NewEmployee;
import modules.payroll.PayrollManagement;
import modules.payroll.ViewEmployee;
import modules.productlicense.ProductLicense;
import modules.retail_stock.RetailStockEntry;
import modules.retail_stock.RetailStockManagement;
import modules.retail_stock.RetailStockNewBill;
import modules.retail_stock.RetailStockViewBill;
import modules.retail_stock.SearchTableRetailStock;
import modules.retailsbill.BatchShow;
import modules.retailsbill.ItemSearchTable;
import modules.retailsbill.RetailBillEntry;
import modules.retailsbill.RetailBillEntryMedical;
import modules.retailsbill.RetailBillSummary;
import modules.retailsbill.RetailManagement;
import modules.retailsbill.RetailNewBill;
import modules.retailsbill.RetailPendingBill;
import modules.retailsbill.RetailSearchTable;
import modules.retailsbill.RetailViewBill;
import modules.search.HomeSearch;
import modules.search.Search;
import modules.stock.SearchTableStock;
import modules.stock.StockEntry;
import modules.stock.StockManagement;
import modules.stock.StockNewBill;
import modules.stock.StockPendingBill;
import modules.stock.StockViewBill;
import modules.supplier.NewSupplier;
import modules.supplier.SupplierManagement;
import modules.supplier.ViewSupplier;
import modules.wholesalebill.BillSummary;
import modules.wholesalebill.FreeSummary;
import modules.wholesalebill.SchemeSummary;
import modules.wholesalebill.SearchEmployee;
import modules.wholesalebill.SearchTable;
import modules.wholesalebill.StockShow;
import modules.wholesalebill.WholeSaleBillSummary;
import modules.wholesalebill.WholeSalePaid;
import modules.wholesalebill.WholesaleBillEntry;
import modules.wholesalebill.WholesaleNewBill;
import modules.wholesalebill.WholesalePendingBill;
import modules.wholesalebill.WholesaleViewBill;

public class Config {

    //database variables
    public static Connection conn = null;
    public static PreparedStatement pstmt = null;
    public static Statement stmt = null;
    public static ResultSet rs = null;
    public static String sql = null;    
    
    //product variables
    public static ArrayList<ConfigProduct> configproduct = null;
    public static ArrayList<WholesaleGst> wholesale_gst = null;
    public static ArrayList<RetailGst> retail_gst = null;
    public static ArrayList<ConfigRProduct> configrproduct = null;
    public static ArrayList<ConfigRVariety> configrvariety = null;
    
    public static ArrayList<ConfigFlavor> configflavor = null;
    public static ArrayList<ConfigContentRate> configcontentrate = null;
    public static ArrayList<ConfigScheme> configscheme = null;
    public static ArrayList<ConfigProfit> configprofit = null;    
    public static ArrayList<ConfigUserProfile> configuserprofile = null;
    
    public static ArrayList<CustomerProfile> customerprofile = null;
    public static ArrayList<EmployeeProfile> employeeprofile = null;
    public static ArrayList<SupplierProfile> supplierprofile = null;    
    
    public static ArrayList<BillingDetails> billing_details = null;    
    public static ArrayList<OtherConfig> config_other_config = null;    
    public static ArrayList<Stock> config_stock = null;    
    
    public static ArrayList<TempPriceDetails> temp_price_details = null;    
    public static ArrayList<RetailStock> config_retail_stock = null;    
    
    public static ArrayList<WholesaleBill> wholesalebill = null;    
    public static ArrayList<WholesaleBillPurchase> wholesalebillpurchase = null;    
    public static ArrayList<WholesaleBillReturn> wholesalebillreturn = null;    
    
    
    
    public static boolean check_wbpurchase = false;
    
    //class managers
    public static ConfigMgr configmgr = null;
    public static ConfigProductMgr configproductmgr = null;
    public static WholesaleGstMgr wholesale_gst_mgr = null;
    public static ConfigRProductMgr configrproductmgr = null;
    public static RetailGstMgr retail_gst_mgr = null;
    public static ConfigRVarietyMgr configrvarietymgr = null;
    public static ConfigFlavorMgr configflavormgr  = null;    
    public static ConfigContentRateMgr configcontentratemgr  = null;    
    public static ConfigSchemeMgr configschememgr  = null;
    public static ConfigProfitMgr configprofitmgr  = null;
    public static ConfigUserProfileMgr configuserprofilemgr  = null;
    
    public static CustomerProfileMgr customerprofilemgr = null;
    public static EmployeeProfileMgr employeeprofilemgr = null;
    public static SupplierProfileMgr supplierprofilemgr  = null;
    public static PayrollMgr payrollmgr = null;
    
    public static StockMgr stockmgr = null;
    public static TempPriceDetailsMgr temp_price_details_mgr = null;    
    public static StockBillMgr stockbillmgr  = null;
    public static StockBillPurchaseMgr stockbillpurchasemgr = null;
    
    public static StockBillReturnMgr stockbillreturnmgr = null;
    
    public static WholesaleBillMgr wholesalebillmgr = null;
    public static WholesaleBillPurchaseMgr wholesalebillpurchasemgr = null;
    public static WholesaleBillReturnMgr wholesalebillreturnmgr = null;
    
    public static RetailBillMgr retailbillmgr = null;
    public static RetailBillPurchaseMgr retailbillpurchasemgr = null;
    public static RetailBillReturnMgr retailbillreturnmgr = null;
    
    public static RetailStockMgr retail_stock_mgr = null;
    public static RetailStockBillMgr retail_stock_bill_mgr = null;
    public static RetailStockPurchaseMgr retail_stock_purchase_mgr = null;
    
    public static BillingDetailsMgr billing_details_mgr = null;
    public static OtherConfigMgr other_config_mgr = null;
    public static ReadMgr read_mgr = null;

    
    //module forms
    public static Login login = null;
    public static ManageAccount manageaccount = null;
    
    public static ProductConfiguration productconfiguration = null;    
    public static ProductLicense productlicense = null;
    
    public static Home home = null;    
    
    public static SupplierManagement suppliermanagement = null;
    public static NewSupplier newsupplier = null;
    public static ViewSupplier viewsupplier = null;
    
    public static CustomerManagement customermanagement=null;
    public static NewCustomer newcustomer=null;
    public static ViewCustomer viewcustomer =null;
    
    public static PayrollManagement payrollmanagement=null;
    public static NewEmployee newemployee = null;
    public static ViewEmployee viewemployee = null;
        
    public static StockManagement stockmanagement = null;
    public static StockNewBill stocknewbill = null;
    public static StockViewBill stockviewbill = null;    
    public static StockEntry stockentry = null;
    public static StockPendingBill stockpendingbill = null;    
    
    public static WholesaleNewBill wholesalenewbill = null;
    public static WholesaleViewBill wholesaleviewbill = null;
    public static WholesaleBillEntry wholesalebillentry = null;
    public static WholesalePendingBill wholesalependingbill = null;
    public static WholeSalePaid whole_sale_paid = null;
    
    public static RetailNewBill retailnewbill = null;
    public static RetailViewBill retailviewbill = null;
    public static RetailBillEntry retailbillentry = null;
    public static RetailBillEntryMedical retailbillentry_medical = null;
    public static RetailPendingBill retailpendingbill = null;
    public static RetailManagement retailmanagement = null;
    
    public static StockShow stockshow = null;
    public static BatchShow batch_show = null;
    public static BillSummary billsummary = null;
    public static SchemeSummary schemesummary = null;
    public static WholeSaleBillSummary wholesalebillsummary = null;
    public static RetailBillSummary retailbillsummary = null;
    public static FreeSummary freesummary = null;
    
    
    public static RetailStockEntry retail_stock_entry = null;
    public static RetailStockManagement retail_stock_management = null;
    public static RetailStockNewBill retail_stock_new_bill = null;
    public static RetailStockViewBill retail_stock_view_bill = null;

//...................search form...........................
    public static SearchTable search_table = null;
    public static RetailSearchTable retail_search_table = null;
    public static ItemSearchTable item_search_table = null;
    public static SearchTableStock search_table_stock = null;
    public static Search search = null;
    public static HomeSearch homesearch = null;
    public static SearchEmployee search_employee = null;
    public static SearchTableRetailStock search_table_retail_stock = null;
    //.......................................................
    public static ArrayList<String> customer = null;
    
    public static boolean check_stock = false;
    public static boolean check = false;
    
    public static Loading Loading = null;
    public static    Calendar cal = Calendar.getInstance();
    public static  SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
    public static  JTextField module_text = null;
    public static  int module_barcode = 0;
        
}
