/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datamanager.billing;

/**
 *
 * @author akshay
 */
public class BillingDetails {
    
    String tin_no = "";
    String company_name = "";
    String tagline = "";
    String address = "";
    String contact_no = "";
    String alt_contact_no = "";
    String start_date = "";
    String renew_date = "";
    String end_date = "";
    String last_date = "";

    public String getTin_no() {
        return tin_no;
    }

    public void setTin_no(String tin_no) {
        this.tin_no = tin_no;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getContact_no() {
        return contact_no;
    }

    public void setContact_no(String contact_no) {
        this.contact_no = contact_no;
    }

    public String getAlt_contact_no() {
        return alt_contact_no;
    }

    public void setAlt_contact_no(String alt_contact_no) {
        this.alt_contact_no = alt_contact_no;
    }

    public String getStart_date() {
        return start_date;
    }

    public void setStart_date(String start_date) {
        this.start_date = start_date;
    }

    public String getRenew_date() {
        return renew_date;
    }

    public void setRenew_date(String renew_date) {
        this.renew_date = renew_date;
    }

    public String getEnd_date() {
        return end_date;
    }

    public void setEnd_date(String end_date) {
        this.end_date = end_date;
    }

    public String getLast_date() {
        return last_date;
    }

    public void setLast_date(String last_date) {
        this.last_date = last_date;
    }

        
}

