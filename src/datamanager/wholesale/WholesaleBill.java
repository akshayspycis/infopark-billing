package datamanager.wholesale;

import datamanager.customer.CustomerProfile;
import java.util.ArrayList;

public class WholesaleBill {
    
    String wholesalebillid = null;    
    CustomerProfile customerprofile = null;    
    String billno = null;
    String date = null;    
    String salesman = null;    
    String purchaseamt = null;    
    String schemeamt = null;
    String vatamt = null;
    String discount = null;    
    String payableamt = null;
    String paidamt = null;
    String balanceamt = null;    
    String customerid = null;    
    String print = "false";   
     ArrayList<WholesaleBillPurchase> wholesalebillpurchase = null;
    ArrayList<WholesaleBillReturn> wholesalebillreturn = null;

    public String getWholesalebillid() {
        return wholesalebillid;
    }

    public void setWholesalebillid(String wholesalebillid) {
        this.wholesalebillid = wholesalebillid;
    }

    public CustomerProfile getCustomerprofile() {
        return customerprofile;
    }

    public void setCustomerprofile(CustomerProfile customerprofile) {
        this.customerprofile = customerprofile;
    }

    public String getBillno() {
        return billno;
    }

    public void setBillno(String billno) {
        this.billno = billno;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSalesman() {
        return salesman;
    }

    public void setSalesman(String salesman) {
        this.salesman = salesman;
    }

    public String getPurchaseamt() {
        return purchaseamt;
    }

    public void setPurchaseamt(String purchaseamt) {
        this.purchaseamt = purchaseamt;
    }

    public String getSchemeamt() {
        return schemeamt;
    }

    public void setSchemeamt(String schemeamt) {
        this.schemeamt = schemeamt;
    }

    public String getVatamt() {
        return vatamt;
    }

    public void setVatamt(String vatamt) {
        this.vatamt = vatamt;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getPayableamt() {
        return payableamt;
    }

    public void setPayableamt(String payableamt) {
        this.payableamt = payableamt;
    }

    public String getPaidamt() {
        return paidamt;
    }

    public void setPaidamt(String paidamt) {
        this.paidamt = paidamt;
    }

    public String getBalanceamt() {
        return balanceamt;
    }

    public void setBalanceamt(String balanceamt) {
        this.balanceamt = balanceamt;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getPrint() {
        return print;
    }

    public void setPrint(String print) {
        this.print = print;
    }

    public ArrayList<WholesaleBillPurchase> getWholesalebillpurchase() {
        return wholesalebillpurchase;
    }

    public void setWholesalebillpurchase(ArrayList<WholesaleBillPurchase> wholesalebillpurchase) {
        this.wholesalebillpurchase = wholesalebillpurchase;
    }

    public ArrayList<WholesaleBillReturn> getWholesalebillreturn() {
        return wholesalebillreturn;
    }

    public void setWholesalebillreturn(ArrayList<WholesaleBillReturn> wholesalebillreturn) {
        this.wholesalebillreturn = wholesalebillreturn;
    }

    
}

