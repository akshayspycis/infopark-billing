/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datamanager.wholesale;

/**
 *
 * @author akki
 */
public class WholeSaleSummary {
String organization = null;    
String date = null;    
String customername = null;    
String billno = null;    
String payableamt = null;    
String paidamt = null;    
String balanceamt = null;    

    public String getOrganization() {
        return organization;
    }

    public void setOrganization(String organization) {
        this.organization = organization;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getCustomername() {
        return customername;
    }

    public void setCustomername(String customername) {
        this.customername = customername;
    }

    public String getBillno() {
        return billno;
    }

    public void setBillno(String billno) {
        this.billno = billno;
    }

    public String getPayableamt() {
        return payableamt;
    }

    public void setPayableamt(String payableamt) {
        this.payableamt = payableamt;
    }

    public String getPaidamt() {
        return paidamt;
    }

    public void setPaidamt(String paidamt) {
        this.paidamt = paidamt;
    }

    public String getBalanceamt() {
        return balanceamt;
    }

    public void setBalanceamt(String balanceamt) {
        this.balanceamt = balanceamt;
    }

        

}
