package datamanager.wholesale;

public class WholesaleBillPurchase {

    String wholesalebillpurchaseid = null;
    String wholesalebillid = null;
    String product = null;
    String flavor = null;
    String contentrate = null;
    String batchno = null;
    String expdate = null;
    String unit = null;
    String mrp = null;
    String quantity = null;
    String pack = null;
    String rate = null;
    String amount = null;
    String vat = null;
    String vatamt = null;
    String scheme = null;
    String schemeamt = null;

    public String getWholesalebillpurchaseid() {
        return wholesalebillpurchaseid;
    }

    public void setWholesalebillpurchaseid(String wholesalebillpurchaseid) {
        this.wholesalebillpurchaseid = wholesalebillpurchaseid;
    }

    public String getWholesalebillid() {
        return wholesalebillid;
    }

    public void setWholesalebillid(String wholesalebillid) {
        this.wholesalebillid = wholesalebillid;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getFlavor() {
        return flavor;
    }

    public void setFlavor(String flavor) {
        this.flavor = flavor;
    }

    public String getContentrate() {
        return contentrate;
    }

    public void setContentrate(String contentrate) {
        this.contentrate = contentrate;
    }

    public String getBatchno() {
        return batchno;
    }

    public void setBatchno(String batchno) {
        this.batchno = batchno;
    }

    public String getExpdate() {
        return expdate;
    }

    public void setExpdate(String expdate) {
        this.expdate = expdate;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getPack() {
        return pack;
    }

    public void setPack(String pack) {
        this.pack = pack;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getVatamt() {
        return vatamt;
    }

    public void setVatamt(String vatamt) {
        this.vatamt = vatamt;
    }

    public String getScheme() {
        return scheme;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public String getSchemeamt() {
        return schemeamt;
    }

    public void setSchemeamt(String schemeamt) {
        this.schemeamt = schemeamt;
    }

    
}
