/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datamanager.retail;


import datamanager.customer.CustomerProfile;
import java.util.ArrayList;

/**
 *
 * @author akshay
 */
public class RetailBill {
    String retailbillid =null;
    CustomerProfile customerprofile = null;    
    String customerid =null;
    String billno =null;
    String date =null;
    String purchaseamt =null;
    String discount =null;
    String payableamt =null;
    String paidamt =null;
    String balanceamt =null;
    String vatamt =null;
    String salesman =null;
    ArrayList<RetailBillPurchase> retailbillpurchase = null;
    ArrayList<RetailBillReturn> retailbillreturn = null;

    public String getRetailbillid() {
        return retailbillid;
    }

    public void setRetailbillid(String retailbillid) {
        this.retailbillid = retailbillid;
    }

    public CustomerProfile getCustomerprofile() {
        return customerprofile;
    }

    public void setCustomerprofile(CustomerProfile customerprofile) {
        this.customerprofile = customerprofile;
    }

    public String getCustomerid() {
        return customerid;
    }

    public void setCustomerid(String customerid) {
        this.customerid = customerid;
    }

    public String getBillno() {
        return billno;
    }

    public void setBillno(String billno) {
        this.billno = billno;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getPurchaseamt() {
        return purchaseamt;
    }

    public void setPurchaseamt(String purchaseamt) {
        this.purchaseamt = purchaseamt;
    }

    public String getDiscount() {
        return discount;
    }

    public void setDiscount(String discount) {
        this.discount = discount;
    }

    public String getPayableamt() {
        return payableamt;
    }

    public void setPayableamt(String payableamt) {
        this.payableamt = payableamt;
    }

    public String getPaidamt() {
        return paidamt;
    }

    public void setPaidamt(String paidamt) {
        this.paidamt = paidamt;
    }

    public String getBalanceamt() {
        return balanceamt;
    }

    public void setBalanceamt(String balanceamt) {
        this.balanceamt = balanceamt;
    }

    public String getVatamt() {
        return vatamt;
    }

    public void setVatamt(String vatamt) {
        this.vatamt = vatamt;
    }

    public String getSalesman() {
        return salesman;
    }

    public void setSalesman(String salesman) {
        this.salesman = salesman;
    }

    public ArrayList<RetailBillPurchase> getRetailbillpurchase() {
        return retailbillpurchase;
    }

    public void setRetailbillpurchase(ArrayList<RetailBillPurchase> retailbillpurchase) {
        this.retailbillpurchase = retailbillpurchase;
    }

    public ArrayList<RetailBillReturn> getRetailbillreturn() {
        return retailbillreturn;
    }

    public void setRetailbillreturn(ArrayList<RetailBillReturn> retailbillreturn) {
        this.retailbillreturn = retailbillreturn;
    }

                
}
