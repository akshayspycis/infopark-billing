/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datamanager.retail;

/**
 *
 * @author akshay
 */
public class RetailBillReturn {
    String retailbillreturnid = null;
    String retailbillid = null;
    String rproduct = null;
    String rvariety = null;
    String contentrate = null;
    String quantity = null;
    String amount = null;
    String vat = null;
    String vatamt = null;

    public String getRetailbillreturnid() {
        return retailbillreturnid;
    }

    public void setRetailbillreturnid(String retailbillreturnid) {
        this.retailbillreturnid = retailbillreturnid;
    }

    public String getRetailbillid() {
        return retailbillid;
    }

    public void setRetailbillid(String retailbillid) {
        this.retailbillid = retailbillid;
    }

    public String getRproduct() {
        return rproduct;
    }

    public void setRproduct(String rproduct) {
        this.rproduct = rproduct;
    }

    public String getRvariety() {
        return rvariety;
    }

    public void setRvariety(String rvariety) {
        this.rvariety = rvariety;
    }

    public String getContentrate() {
        return contentrate;
    }

    public void setContentrate(String contentrate) {
        this.contentrate = contentrate;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getVatamt() {
        return vatamt;
    }

    public void setVatamt(String vatamt) {
        this.vatamt = vatamt;
    }

            
}
