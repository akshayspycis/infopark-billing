/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datamanager.retail;

/**
 *
 * @author akshay
 */
public class RetailBillPurchase {
String retailbill_purchase_id = null;    
String retailbillid = null;    
String product = null;    
String variety = null;    
String unit  = null;    
String mrp = null;    
String quantity = null;    
String amount = null;    

    public String getRetailbill_purchase_id() {
        return retailbill_purchase_id;
    }

    public void setRetailbill_purchase_id(String retailbill_purchase_id) {
        this.retailbill_purchase_id = retailbill_purchase_id;
    }

    public String getRetailbillid() {
        return retailbillid;
    }

    public void setRetailbillid(String retailbillid) {
        this.retailbillid = retailbillid;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getVariety() {
        return variety;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

                
}
