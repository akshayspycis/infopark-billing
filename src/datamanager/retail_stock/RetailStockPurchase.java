/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datamanager.retail_stock;

/**
 *
 * @author akshay
 */
public class RetailStockPurchase {
    String retail_stock_purchase_id ="";
    String retail_stock_bill_id  ="";
    String product   ="";
    String variety ="";
    String unit ="";
    String mrp ="";
    String rate ="";
    String quantity ="";
    String amount ="";
    String vat ="";
    String vatamt ="";
    String netamt ="";

    public String getRetail_stock_purchase_id() {
        return retail_stock_purchase_id;
    }

    public void setRetail_stock_purchase_id(String retail_stock_purchase_id) {
        this.retail_stock_purchase_id = retail_stock_purchase_id;
    }

    public String getRetail_stock_bill_id() {
        return retail_stock_bill_id;
    }

    public void setRetail_stock_bill_id(String retail_stock_bill_id) {
        this.retail_stock_bill_id = retail_stock_bill_id;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getVariety() {
        return variety;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getVat() {
        return vat;
    }

    public void setVat(String vat) {
        this.vat = vat;
    }

    public String getVatamt() {
        return vatamt;
    }

    public void setVatamt(String vatamt) {
        this.vatamt = vatamt;
    }

    public String getNetamt() {
        return netamt;
    }

    public void setNetamt(String netamt) {
        this.netamt = netamt;
    }

             
}
