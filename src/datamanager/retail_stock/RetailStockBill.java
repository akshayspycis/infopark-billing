/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datamanager.retail_stock;

import datamanager.supplier.SupplierProfile;
import java.util.ArrayList;

/**
 *
 * @author akshay
 */
public class RetailStockBill {
    String retail_stock_bill_id ="";
    String supplier_id ="";
    String bill_no ="";
    String date ="";
    String salesman ="";
    String purchaseamt ="";
    String schemeamt     ="";
    String vatmat ="";
    String discountamt ="";
    String payableamt ="";
    String paidamt   ="";
    String balanceamt ="";
    
    ArrayList<RetailStockPurchase> retail_stock_purchase=null;
    SupplierProfile supplier_profile=null;

    public String getRetail_stock_bill_id() {
        return retail_stock_bill_id;
    }

    public void setRetail_stock_bill_id(String retail_stock_bill_id) {
        this.retail_stock_bill_id = retail_stock_bill_id;
    }

    public String getSupplier_id() {
        return supplier_id;
    }

    public void setSupplier_id(String supplier_id) {
        this.supplier_id = supplier_id;
    }

    public String getBill_no() {
        return bill_no;
    }

    public void setBill_no(String bill_no) {
        this.bill_no = bill_no;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getSalesman() {
        return salesman;
    }

    public void setSalesman(String salesman) {
        this.salesman = salesman;
    }

    public String getPurchaseamt() {
        return purchaseamt;
    }

    public void setPurchaseamt(String purchaseamt) {
        this.purchaseamt = purchaseamt;
    }

    public String getSchemeamt() {
        return schemeamt;
    }

    public void setSchemeamt(String schemeamt) {
        this.schemeamt = schemeamt;
    }

    public String getVatmat() {
        return vatmat;
    }

    public void setVatmat(String vatmat) {
        this.vatmat = vatmat;
    }

    public String getDiscountamt() {
        return discountamt;
    }

    public void setDiscountamt(String discountamt) {
        this.discountamt = discountamt;
    }

    public String getPayableamt() {
        return payableamt;
    }

    public void setPayableamt(String payableamt) {
        this.payableamt = payableamt;
    }

    public String getPaidamt() {
        return paidamt;
    }

    public void setPaidamt(String paidamt) {
        this.paidamt = paidamt;
    }

    public String getBalanceamt() {
        return balanceamt;
    }

    public void setBalanceamt(String balanceamt) {
        this.balanceamt = balanceamt;
    }

    public ArrayList<RetailStockPurchase> getRetail_stock_purchase() {
        return retail_stock_purchase;
    }

    public void setRetail_stock_purchase(ArrayList<RetailStockPurchase> retail_stock_purchase) {
        this.retail_stock_purchase = retail_stock_purchase;
    }

    public SupplierProfile getSupplier_profile() {
        return supplier_profile;
    }

    public void setSupplier_profile(SupplierProfile supplier_profile) {
        this.supplier_profile = supplier_profile;
    }

                    
}
