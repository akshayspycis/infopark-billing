/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package datamanager.retail_stock;

import java.util.ArrayList;

/**
 *
 * @author akshay
 */
public class RetailStock {
    String retail_stock_id ="";
    String product ="";
    String variety ="";
    String unit ="";
    String mrp ="";
    String quantity ="";

    public String getRetail_stock_id() {
        return retail_stock_id;
    }

    public void setRetail_stock_id(String retail_stock_id) {
        this.retail_stock_id = retail_stock_id;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getVariety() {
        return variety;
    }

    public void setVariety(String variety) {
        this.variety = variety;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public String getMrp() {
        return mrp;
    }

    public void setMrp(String mrp) {
        this.mrp = mrp;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

            
}
