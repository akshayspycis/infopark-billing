package datamanager.payroll;

public class PayRoll {
    
    String payrollid =null;
    String employeeid =null;
    String month =null;
    String year =null;
    String payment =null;
    String date =null;
    String time =null;

    public String getPayrollid() {
        return payrollid;
    }

    public void setPayrollid(String payrollid) {
        this.payrollid = payrollid;
    }

    public String getEmployeeid() {
        return employeeid;
    }

    public void setEmployeeid(String employeeid) {
        this.employeeid = employeeid;
    }

    public String getMonth() {
        return month;
    }

    public void setMonth(String month) {
        this.month = month;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

}
