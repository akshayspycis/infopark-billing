package modules.payroll;

import datamanager.config.Config;
import datamanager.payroll.EmployeeProfile;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

/**
 *
 * @author AMS
 */
public class ViewEmployee extends javax.swing.JDialog {

    Calendar currentDate;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    
    public ViewEmployee(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/icon.png")));

        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_employeeName = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txt_fatherName = new javax.swing.JTextField();
        jLabel18 = new javax.swing.JLabel();
        cb_maritalStatus = new javax.swing.JComboBox();
        jLabel15 = new javax.swing.JLabel();
        cb_gender = new javax.swing.JComboBox();
        jLabel13 = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txt_age = new javax.swing.JTextField();
        jdc_dob = new com.toedter.calendar.JDateChooser();
        panal = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();
        txt_contactNo = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txt_AlternateContactNo = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        txt_address = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txt_locality = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txt_city = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txt_pincode = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        cb_state = new javax.swing.JComboBox();
        jLabel11 = new javax.swing.JLabel();
        txt_otherInformation = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txt_email = new javax.swing.JTextField();
        jPanel4 = new javax.swing.JPanel();
        txt_salary = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jdc_joiningDate = new com.toedter.calendar.JDateChooser();
        jLabel19 = new javax.swing.JLabel();
        jdc_leavingDate = new com.toedter.calendar.JDateChooser();
        jLabel17 = new javax.swing.JLabel();
        cb_identityType = new javax.swing.JComboBox();
        jLabel16 = new javax.swing.JLabel();
        txt_identityNo = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        btn_delete = new javax.swing.JButton();
        btn_update = new javax.swing.JButton();
        btn_cancel = new javax.swing.JButton();
        lbl_employeeid = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Payroll Management - View Profile");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setForeground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Personal Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(51, 51, 51))); // NOI18N

        jLabel1.setForeground(new java.awt.Color(51, 51, 51));
        jLabel1.setText("Employee Name *");

        jLabel5.setForeground(new java.awt.Color(51, 51, 51));
        jLabel5.setText("Father Name");

        jLabel18.setForeground(new java.awt.Color(51, 51, 51));
        jLabel18.setText("Marital Status *");

        cb_maritalStatus.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "- Select -", "Marired", "Unmarried" }));

        jLabel15.setForeground(new java.awt.Color(51, 51, 51));
        jLabel15.setText("Gender *");

        cb_gender.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "-Select-", "Male", "Female" }));

        jLabel13.setForeground(new java.awt.Color(51, 51, 51));
        jLabel13.setText("DOB *");

        jLabel14.setForeground(new java.awt.Color(51, 51, 51));
        jLabel14.setText("Age");

        jdc_dob.setDateFormatString("dd-MM-yyyy");

        org.jdesktop.layout.GroupLayout jPanel2Layout = new org.jdesktop.layout.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jLabel1)
                    .add(txt_employeeName, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 238, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel15)
                            .add(cb_gender, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 73, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .add(10, 10, 10)
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel13)
                            .add(jdc_dob, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 99, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel14)
                            .add(txt_age, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 46, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                        .add(cb_maritalStatus, 0, 238, Short.MAX_VALUE)
                        .add(jLabel18)
                        .add(txt_fatherName))
                    .add(jLabel5))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel1)
                    .add(jLabel5))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txt_employeeName, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txt_fatherName, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(jPanel2Layout.createSequentialGroup()
                        .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                            .add(jLabel18)
                            .add(jLabel14))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(cb_maritalStatus, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                    .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                        .add(cb_gender, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .add(org.jdesktop.layout.GroupLayout.LEADING, jPanel2Layout.createSequentialGroup()
                            .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                                .add(jLabel13)
                                .add(jLabel15))
                            .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                            .add(jPanel2Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                .add(jdc_dob, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .add(txt_age, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panal.setBackground(new java.awt.Color(255, 255, 255));
        panal.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Contact Details", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12), new java.awt.Color(51, 51, 51))); // NOI18N

        jLabel6.setForeground(new java.awt.Color(51, 51, 51));
        jLabel6.setText("Contact No. *");

        jTextField3.setEditable(false);
        jTextField3.setText("+91");
        jTextField3.setEnabled(false);

        txt_contactNo.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_contactNoCaretUpdate(evt);
            }
        });

        jLabel7.setForeground(new java.awt.Color(51, 51, 51));
        jLabel7.setText("Alternate Contact No.");

        jLabel8.setForeground(new java.awt.Color(51, 51, 51));
        jLabel8.setText("Address *");

        jLabel9.setForeground(new java.awt.Color(51, 51, 51));
        jLabel9.setText("Locality *");

        jLabel10.setForeground(new java.awt.Color(51, 51, 51));
        jLabel10.setText("City *");

        jLabel2.setForeground(new java.awt.Color(51, 51, 51));
        jLabel2.setText("Pincode");

        jLabel12.setForeground(new java.awt.Color(51, 51, 51));
        jLabel12.setText("State *");

        cb_state.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "- Select -", "Andaman and Nicobar Islands", "Andhra Pradesh", "Arunachal Pradesh", "Assam", "Chandigarh", "Chhattisgarh", "Dadra and Nagar Haveli", "Daman and Diu", "Delhi", "Goa", "Gujarat", "Haryana", "Himachal Pradesh", "Jammu and Kashmir", "Jharkhand", "Karnataka", "Kerala", "Lakshadweep", "Madhya Pradesh", "Maharashtra", "Manipur", "Meghalaya", "Mizoram", "Nagaland", "Orissa", "Puduchery", "Punjab", "Rajasthan", "Sikkim", "Tamil Nadu", "Tripura", "Uttar Pradesh", "Uttaranchal", "West Bengal" }));

        jLabel11.setForeground(new java.awt.Color(51, 51, 51));
        jLabel11.setText("Other Information");

        jLabel4.setForeground(new java.awt.Color(51, 51, 51));
        jLabel4.setText("Email");

        org.jdesktop.layout.GroupLayout panalLayout = new org.jdesktop.layout.GroupLayout(panal);
        panal.setLayout(panalLayout);
        panalLayout.setHorizontalGroup(
            panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panalLayout.createSequentialGroup()
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, panalLayout.createSequentialGroup()
                        .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(panalLayout.createSequentialGroup()
                                .addContainerGap()
                                .add(jLabel8))
                            .add(panalLayout.createSequentialGroup()
                                .addContainerGap()
                                .add(jTextField3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                                .add(txt_contactNo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 206, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                            .add(panalLayout.createSequentialGroup()
                                .add(10, 10, 10)
                                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                    .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING)
                                        .add(org.jdesktop.layout.GroupLayout.LEADING, txt_address, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 238, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                                        .add(org.jdesktop.layout.GroupLayout.LEADING, jLabel6))
                                    .add(panalLayout.createSequentialGroup()
                                        .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                            .add(jLabel10)
                                            .add(txt_city, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 143, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                                        .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                                            .add(jLabel2)
                                            .add(txt_pincode, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 85, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, 10, Short.MAX_VALUE)
                        .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                            .add(txt_AlternateContactNo)
                            .add(jLabel7)
                            .add(jLabel9)
                            .add(txt_locality)
                            .add(jLabel12)
                            .add(cb_state, 0, 238, Short.MAX_VALUE)))
                    .add(org.jdesktop.layout.GroupLayout.TRAILING, panalLayout.createSequentialGroup()
                        .addContainerGap()
                        .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(panalLayout.createSequentialGroup()
                                .add(jLabel4)
                                .add(0, 0, Short.MAX_VALUE))
                            .add(txt_email))
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                        .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                            .add(jLabel11)
                            .add(txt_otherInformation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 238, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        panalLayout.setVerticalGroup(
            panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(panalLayout.createSequentialGroup()
                .add(6, 6, 6)
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel6)
                    .add(jLabel7))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jTextField3, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txt_contactNo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txt_AlternateContactNo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel8)
                    .add(jLabel9))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txt_address, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txt_locality, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel10)
                    .add(jLabel2)
                    .add(jLabel12))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txt_city, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txt_pincode, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(cb_state, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(jLabel11)
                    .add(jLabel4))
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(panalLayout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(txt_otherInformation, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(txt_email, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel20.setForeground(new java.awt.Color(51, 51, 51));
        jLabel20.setText("Salary *");

        jLabel3.setForeground(new java.awt.Color(51, 51, 51));
        jLabel3.setText("Joining Date *");

        jdc_joiningDate.setDateFormatString("dd-MM-yyyy");

        jLabel19.setForeground(new java.awt.Color(51, 51, 51));
        jLabel19.setText("Leaving Date");

        jdc_leavingDate.setDateFormatString("dd-MM-yyyy");

        jLabel17.setForeground(new java.awt.Color(51, 51, 51));
        jLabel17.setText("Identity Type");

        cb_identityType.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "- Select -", "Password", "Adhar card", "Voter ID", "Driving Licances" }));

        jLabel16.setForeground(new java.awt.Color(51, 51, 51));
        jLabel16.setText("Identity No.");

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel21.setText("Other Details");

        org.jdesktop.layout.GroupLayout jPanel4Layout = new org.jdesktop.layout.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel21)
                .addContainerGap())
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jSeparator1)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
                    .add(txt_salary, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 146, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel20)
                    .add(jLabel3)
                    .add(jdc_joiningDate, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 135, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jPanel4Layout.createSequentialGroup()
                        .add(1, 1, 1)
                        .add(jLabel19))
                    .add(jdc_leavingDate, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel17)
                    .add(cb_identityType, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 90, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                    .add(jLabel16)
                    .add(txt_identityNo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 76, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        jPanel4Layout.linkSize(new java.awt.Component[] {cb_identityType, jLabel16, jLabel17, jLabel19, jLabel20, jLabel3, jdc_joiningDate, jdc_leavingDate, txt_identityNo, txt_salary}, org.jdesktop.layout.GroupLayout.HORIZONTAL);

        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .add(jLabel21)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jSeparator1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 10, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel17)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(cb_identityType, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jLabel16, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 14, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(txt_identityNo, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 20, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jLabel20)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(txt_salary, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jLabel3)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jdc_joiningDate, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.UNRELATED)
                .add(jLabel19)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(jdc_leavingDate, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        org.jdesktop.layout.GroupLayout jPanel1Layout = new org.jdesktop.layout.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING, false)
                    .add(panal, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanel2, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .add(10, 10, 10)
                .add(jPanel4, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .add(jPanel1Layout.createParallelGroup(org.jdesktop.layout.GroupLayout.TRAILING, false)
                    .add(jPanel4, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .add(jPanel1Layout.createSequentialGroup()
                        .add(jPanel2, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                        .add(panal, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btn_delete.setText("Delete");
        btn_delete.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_deleteActionPerformed(evt);
            }
        });

        btn_update.setText("Update");
        btn_update.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_updateActionPerformed(evt);
            }
        });

        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });

        lbl_employeeid.setForeground(new java.awt.Color(240, 240, 240));

        org.jdesktop.layout.GroupLayout layout = new org.jdesktop.layout.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(layout.createSequentialGroup()
                .addContainerGap()
                .add(btn_delete)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(lbl_employeeid, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(119, 119, 119)
                .add(btn_update, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, 67, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED)
                .add(btn_cancel)
                .addContainerGap())
            .add(jPanel1, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        layout.linkSize(new java.awt.Component[] {btn_cancel, btn_delete, btn_update}, org.jdesktop.layout.GroupLayout.HORIZONTAL);

        layout.setVerticalGroup(
            layout.createParallelGroup(org.jdesktop.layout.GroupLayout.LEADING)
            .add(org.jdesktop.layout.GroupLayout.TRAILING, layout.createSequentialGroup()
                .add(jPanel1, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, org.jdesktop.layout.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(org.jdesktop.layout.LayoutStyle.RELATED, org.jdesktop.layout.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .add(layout.createParallelGroup(org.jdesktop.layout.GroupLayout.BASELINE)
                    .add(btn_cancel)
                    .add(btn_delete)
                    .add(lbl_employeeid)
                    .add(btn_update))
                .add(6, 6, 6))
        );

        layout.linkSize(new java.awt.Component[] {btn_delete, lbl_employeeid}, org.jdesktop.layout.GroupLayout.VERTICAL);

        getRootPane().setDefaultButton(btn_update);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void btn_updateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_updateActionPerformed
        String str = checkValidity();
        
        if (str.equals("ok")) {        
            EmployeeProfile ep = new EmployeeProfile();
            
            ep.setEmployeeid(lbl_employeeid.getText());
            ep.setEmployeename(txt_employeeName.getText());
            ep.setGender(cb_gender.getSelectedItem().toString());
            ep.setDob(sdf.format(jdc_dob.getDate()));
            ep.setAge(txt_age.getText());
            ep.setFathername(txt_fatherName.getText());
            ep.setMaritalstatus(cb_maritalStatus.getSelectedItem().toString());
            ep.setIdentitytype(cb_identityType.getSelectedItem().toString());
            ep.setIdentityno(txt_identityNo.getText());
            ep.setContactno(txt_contactNo.getText()) ;
            ep.setAltcontactno(txt_AlternateContactNo.getText());
            ep.setAddress(txt_address.getText());
            ep.setLocality(txt_locality.getText());
            ep.setCity(txt_city.getText());
            ep.setPincode(txt_pincode.getText());
            ep.setState(cb_state.getSelectedItem().toString());        
            ep.setEmail(txt_email.getText());
            ep.setSalary(txt_salary.getText());
            ep.setJoiningdate(sdf.format(jdc_joiningDate.getDate()));
            try {
                ep.setLeavingdate(sdf.format(jdc_leavingDate.getDate()));
            } catch (Exception e) {
                ep.setLeavingdate(null);
            }            
            ep.setOther(txt_otherInformation.getText());
 
            if (Config.employeeprofilemgr.updEmployeeProfile(ep)) {
                Config.payrollmanagement.onloadReset();
                JOptionPane.showMessageDialog(this, "Employee porfile successfully upadted.", "Creation successful", JOptionPane.NO_OPTION);
               
            } else {
                JOptionPane.showMessageDialog(this, "Error in profile updated.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Check '"+str+"' field.", "Error", JOptionPane.ERROR_MESSAGE);
        }         
    }//GEN-LAST:event_btn_updateActionPerformed

    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancelActionPerformed

    private void btn_deleteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_deleteActionPerformed
 if (Config.employeeprofilemgr.delEmployeeProfile(lbl_employeeid.getText())) {
            Config.payrollmanagement.onloadReset();
            JOptionPane.showMessageDialog(this, "Employee Profile deleted successfully.", "Deletion successful", JOptionPane.NO_OPTION);
            dispose();            
        } else {
            JOptionPane.showMessageDialog(this, "Error in profile deletion.", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_deleteActionPerformed

    private void txt_contactNoCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_contactNoCaretUpdate
        if (!txt_contactNo.getText().equals("")) {
            try {
                Integer.parseInt(txt_contactNo.getText());
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, "Only nos allowed.", "Error", JOptionPane.ERROR_MESSAGE);
            }   
        }
    }//GEN-LAST:event_txt_contactNoCaretUpdate
            
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_delete;
    private javax.swing.JButton btn_update;
    private javax.swing.JComboBox cb_gender;
    private javax.swing.JComboBox cb_identityType;
    private javax.swing.JComboBox cb_maritalStatus;
    private javax.swing.JComboBox cb_state;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JTextField jTextField3;
    private com.toedter.calendar.JDateChooser jdc_dob;
    private com.toedter.calendar.JDateChooser jdc_joiningDate;
    private com.toedter.calendar.JDateChooser jdc_leavingDate;
    private javax.swing.JLabel lbl_employeeid;
    private javax.swing.JPanel panal;
    private javax.swing.JTextField txt_AlternateContactNo;
    private javax.swing.JTextField txt_address;
    private javax.swing.JTextField txt_age;
    private javax.swing.JTextField txt_city;
    private javax.swing.JTextField txt_contactNo;
    private javax.swing.JTextField txt_email;
    private javax.swing.JTextField txt_employeeName;
    private javax.swing.JTextField txt_fatherName;
    private javax.swing.JTextField txt_identityNo;
    private javax.swing.JTextField txt_locality;
    private javax.swing.JTextField txt_otherInformation;
    private javax.swing.JTextField txt_pincode;
    private javax.swing.JTextField txt_salary;
    // End of variables declaration//GEN-END:variables
        
    public void onloadReset(String employeeid) {
        try {
             int i;
        for (i = 0; i < Config.employeeprofile.size(); i++) {
            if (Config.employeeprofile.get(i).getEmployeeid().equals(employeeid)) {
                break;
            }            
        }  
            currentDate = Calendar.getInstance();
            EmployeeProfile ep = Config.employeeprofile.get(i);
            
            lbl_employeeid.setText(ep.getEmployeeid());
            txt_employeeName.setText(ep.getEmployeename());
            txt_fatherName.setText(ep.getFathername());
            cb_gender.setSelectedItem(ep.getGender());
            try {
                jdc_dob.setDate(sdf.parse(ep.getDob()));
            } catch (Exception e) {
                jdc_dob.setDate(null);
            }            
            txt_age.setText(ep.getAge());
            cb_maritalStatus.setSelectedItem(ep.getMaritalstatus());
            cb_identityType.setSelectedItem(ep.getIdentitytype());
            txt_identityNo.setText(ep.getIdentityno());
            txt_contactNo.setText( ep.getContactno());
            txt_AlternateContactNo.setText(ep.getAltcontactno());
            txt_address.setText(ep.getAddress());
            txt_locality.setText(ep.getLocality());
            txt_city.setText(ep.getCity());
            txt_pincode.setText(ep.getPincode());
            cb_state.setSelectedItem(ep.getState());
            txt_otherInformation.setText(ep.getOther());
            jdc_joiningDate.setDate(sdf.parse(ep.getJoiningdate()));
            try {
                jdc_leavingDate.setDate(sdf.parse(ep.getLeavingdate()));
            } catch (Exception e) {
                jdc_leavingDate.setDate(null);
            }            
            txt_salary.setText(ep.getSalary());
        } catch (ParseException ex) {
            Logger.getLogger(ViewEmployee.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
      
    private String checkValidity() {
        if (txt_employeeName.getText().equals("")) {
            return "Employee Name";
        }
        else if (cb_gender.getSelectedIndex()==0) {
            return "Gender";
        }
        else if (jdc_dob.getDate() == null) {
            return "DOB";
        }
        else if (cb_maritalStatus.getSelectedIndex() == 0) {
            return "Marital Status";
        }        
        else if (txt_contactNo.getText().equals("") || txt_contactNo.getText().length() != 10) {
            return "Contact No.";
        }
        else if (txt_address.getText().equals("")) {
            return "Address";
        }
        else if (txt_locality.getText().equals("")) {
            return "Locality";
        }
        else if (txt_city.getText().equals("")) {
            return "City";
        }
        else if (cb_state.getSelectedIndex()==0) {
            return "State";
        }
        else if (txt_salary.getText().equals("")) {
            return "Salary";
        }
        else if (jdc_joiningDate.getDate() == null) {
            return "Joining Date";
        }
        else {
            return "ok";
        }
    }
}
