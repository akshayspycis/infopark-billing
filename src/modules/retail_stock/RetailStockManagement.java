package modules.retail_stock;

import modules.stock.*;
import datamanager.config.Config;
import datamanager.config.ConfigFlavor;
import datamanager.supplier.SupplierProfile;
import java.awt.EventQueue;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Vector;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.DefaultComboBoxModel;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;

public class RetailStockManagement extends javax.swing.JDialog {
    
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    
    DefaultTableModel tbl_stockbillmodel;
    DefaultTableModel tbl_avialabilitymodel;
    DefaultTableModel tbl_soldmodel;
    
    ArrayList<String> stockbillids = null;    
    
    private JTextField tf1 = null;
    private boolean hide_flag1 = false;
    private Vector<String> v1 = new Vector<String>();    
   
    public RetailStockManagement(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        tbl_stockbillmodel = (DefaultTableModel) tbl_StockBill.getModel();
        tbl_avialabilitymodel = (DefaultTableModel) tbl_Available.getModel();        
        
        this.setLocationRelativeTo(null);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/icon.png")));

        autosugg1();

        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
 
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel11 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txt_totalpayableamt = new javax.swing.JTextField();
        btn_viewbill = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txt_totalbalanceamt = new javax.swing.JTextField();
        txt_totalpaidamt = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        btn_newbill = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_StockBill = new javax.swing.JTable();
        jPanel7 = new javax.swing.JPanel();
        lbl_searchby = new javax.swing.JLabel();
        btn_search_1 = new javax.swing.JButton();
        cb_organization = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jdc_date = new com.toedter.calendar.JDateChooser();
        btn_cancel_1 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        txt_totalItemCount = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_Available = new javax.swing.JTable();
        jPanel8 = new javax.swing.JPanel();
        lbl_searchby1 = new javax.swing.JLabel();
        btn_search_2 = new javax.swing.JButton();
        cb_productname = new javax.swing.JComboBox();
        jLabel6 = new javax.swing.JLabel();
        cb_flavorname = new javax.swing.JComboBox();
        btn_cancel_2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Stock Management");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));

        jPanel6.setBackground(new java.awt.Color(2, 79, 99));

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(255, 255, 255));
        jLabel5.setText("Retail Stock Management");

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel5)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel5)
                .addContainerGap())
        );

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel3.setText("Total Paid Amount :");

        txt_totalpayableamt.setEditable(false);
        txt_totalpayableamt.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        btn_viewbill.setMnemonic('V');
        btn_viewbill.setText("View Bill");
        btn_viewbill.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_viewbillActionPerformed(evt);
            }
        });

        jLabel2.setText("Total Payable Amount :");

        txt_totalbalanceamt.setEditable(false);
        txt_totalbalanceamt.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        txt_totalpaidamt.setEditable(false);
        txt_totalpaidamt.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        jLabel4.setText("Total Balance Amount :");

        btn_newbill.setMnemonic('N');
        btn_newbill.setText("New Bill");
        btn_newbill.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_newbillActionPerformed(evt);
            }
        });

        tbl_StockBill.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbl_StockBill.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ORGANIZATION", "DATE", "BILL NO.", "PAYABLE AMOUNT", "PAID AMOUNT", "BALANCE AMOUNT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_StockBill.setRowHeight(20);
        tbl_StockBill.setSelectionBackground(new java.awt.Color(96, 118, 159));
        tbl_StockBill.getTableHeader().setReorderingAllowed(false);
        tbl_StockBill.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_StockBillMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tbl_StockBill);
        if (tbl_StockBill.getColumnModel().getColumnCount() > 0) {
            tbl_StockBill.getColumnModel().getColumn(0).setPreferredWidth(200);
            tbl_StockBill.getColumnModel().getColumn(1).setPreferredWidth(30);
            tbl_StockBill.getColumnModel().getColumn(2).setPreferredWidth(30);
            tbl_StockBill.getColumnModel().getColumn(3).setPreferredWidth(30);
            tbl_StockBill.getColumnModel().getColumn(4).setPreferredWidth(30);
            tbl_StockBill.getColumnModel().getColumn(5).setPreferredWidth(30);
        }

        lbl_searchby.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lbl_searchby.setText("Organization :");

        btn_search_1.setMnemonic('S');
        btn_search_1.setText("Search");
        btn_search_1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_search_1ActionPerformed(evt);
            }
        });

        cb_organization.setEditable(true);
        cb_organization.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_organizationActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Date :");

        jdc_date.setDateFormatString("dd-MM-yyyy");
        jdc_date.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jdc_datePropertyChange(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_searchby)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cb_organization, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jdc_date, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_search_1)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(lbl_searchby)
                    .addComponent(btn_search_1)
                    .addComponent(jdc_date, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel1)
                    .addComponent(cb_organization, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );

        jPanel7Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_search_1, cb_organization, jLabel1, jdc_date, lbl_searchby});

        btn_cancel_1.setText("Cancel");
        btn_cancel_1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancel_1ActionPerformed(evt);
            }
        });

        jButton1.setText("Delete");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_newbill)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_viewbill)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_totalpayableamt, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_totalpaidamt, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_totalbalanceamt, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cancel_1)))
                .addContainerGap())
            .addComponent(jPanel7, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_newbill, btn_viewbill});

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txt_totalbalanceamt, txt_totalpaidamt, txt_totalpayableamt});

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_cancel_1, jButton1});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_cancel_1)
                    .addComponent(btn_newbill)
                    .addComponent(btn_viewbill)
                    .addComponent(jLabel2)
                    .addComponent(txt_totalpayableamt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(txt_totalpaidamt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4)
                    .addComponent(txt_totalbalanceamt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addGap(10, 10, 10))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_cancel_1, btn_newbill, btn_viewbill, jLabel2, jLabel3, jLabel4, txt_totalbalanceamt, txt_totalpaidamt, txt_totalpayableamt});

        jTabbedPane1.addTab("MANAGE", jPanel1);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        txt_totalItemCount.setEditable(false);
        txt_totalItemCount.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        jLabel7.setText("Total Item Count :");

        tbl_Available.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbl_Available.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "PRODUCT", "FLAVOR", " RATE", "UNIT", "AVAILABLE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_Available.setRowHeight(20);
        tbl_Available.setSelectionBackground(new java.awt.Color(96, 118, 159));
        tbl_Available.getTableHeader().setReorderingAllowed(false);
        jScrollPane3.setViewportView(tbl_Available);
        if (tbl_Available.getColumnModel().getColumnCount() > 0) {
            tbl_Available.getColumnModel().getColumn(0).setPreferredWidth(100);
            tbl_Available.getColumnModel().getColumn(1).setPreferredWidth(100);
        }

        lbl_searchby1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lbl_searchby1.setText("Product :");

        btn_search_2.setMnemonic('S');
        btn_search_2.setText("Search");
        btn_search_2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_search_2ActionPerformed(evt);
            }
        });

        cb_productname.setEditable(true);
        cb_productname.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_productnameActionPerformed(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Flavor :");

        cb_flavorname.setEditable(true);
        cb_flavorname.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_flavornameActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_searchby1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cb_productname, 0, 389, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cb_flavorname, javax.swing.GroupLayout.PREFERRED_SIZE, 387, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_search_2)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel6)
                    .addComponent(cb_flavorname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_productname, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_search_2)
                    .addComponent(lbl_searchby1))
                .addGap(10, 10, 10))
        );

        jPanel8Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_search_2, cb_flavorname, cb_productname, jLabel6, lbl_searchby1});

        btn_cancel_2.setText("Cancel");
        btn_cancel_2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancel_2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane3)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel7)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_totalItemCount, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_cancel_2)))
                .addContainerGap())
            .addComponent(jPanel8, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(jPanel8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 418, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_cancel_2)
                    .addComponent(jLabel7)
                    .addComponent(txt_totalItemCount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10))
        );

        jPanel4Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_cancel_2, jLabel7, txt_totalItemCount});

        jTabbedPane1.addTab("AVAILABLE", jPanel4);

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        setSize(new java.awt.Dimension(1031, 640));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents
    
    //checked
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog
    
    //checked
    private void btn_search_1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_search_1ActionPerformed
        try {
            stockbillids = new ArrayList<String>();
            tbl_stockbillmodel.setRowCount(0);

            float totalpayableamt = 0;
            float totalpaidamt = 0;
            float totalbalanceamt = 0;

            String supplierid = null;
            String date;        

            try {
                String org = cb_organization.getSelectedItem().toString();
                for (int i = 0; i < Config.supplierprofile.size(); i++) {
                    SupplierProfile sp = Config.supplierprofile.get(i);
                    if (sp.getOrganization().equals(org)) {
                        supplierid = sp.getSupplierid();
                    }
                }
            } catch (Exception e) {
                supplierid = null;            
            }

            try {
                date = sdf.format(jdc_date.getDate());
            } catch (Exception e) {
                date = "";
            }

            try {
                if (supplierid == null) {
                    if (date.equals("")) {
                        Config.sql = "select * from retail_stock_bill";
                    } else {
                        Config.sql = "select * from retail_stock_bill where date = '"+date+"'";
                    }
                } else {
                    if (date.equals("")) {
                        Config.sql = "select * from retail_stock_bill where supplierid = '"+supplierid+"'";
                    } else {
                        Config.sql = "select * from retail_stock_bill where supplierid = '"+supplierid+"' and date = '"+date+"'";
                    }
                }                        

                Config.rs = Config.stmt.executeQuery(Config.sql);
                while (Config.rs.next()) {
                    stockbillids.add(Config.rs.getString("retail_stock_bill_id"));
                    String org = null;
                    for (int i = 0; i < Config.supplierprofile.size(); i++) {
                        if (Config.supplierprofile.get(i).getSupplierid().equals(Config.rs.getString("supplierid"))) {
                            org = Config.supplierprofile.get(i).getOrganization();
                            break;
                        }
                    }                
                    tbl_stockbillmodel.addRow(new Object[] {                    
                        org,
                        Config.rs.getString("date"),
                        Config.rs.getString("bill_no"),                    
                        Config.rs.getString("payableamt"),
                        Config.rs.getString("paidamt"),
                        Config.rs.getString("balanceamt")                        
                    });

                    totalpayableamt = totalpayableamt + Float.parseFloat(Config.rs.getString("payableamt"));
                    totalpaidamt = totalpaidamt + Float.parseFloat(Config.rs.getString("paidamt"));
                    totalbalanceamt = totalbalanceamt + Float.parseFloat(Config.rs.getString("balanceamt"));
                }

                txt_totalpayableamt.setText(String.valueOf(totalpayableamt));
                txt_totalpaidamt.setText(String.valueOf(totalpaidamt));
                txt_totalbalanceamt.setText(String.valueOf(totalbalanceamt));

            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } catch (Exception e) {}
    }//GEN-LAST:event_btn_search_1ActionPerformed

    //checked
    private void tbl_StockBillMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_StockBillMouseClicked
        if (evt.getClickCount() == 2) {
            btn_viewbillActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_StockBillMouseClicked

    //checked
    private void btn_newbillActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_newbillActionPerformed
        Config.retail_stock_new_bill.onloadReset();
        Config.retail_stock_new_bill.setVisible(true);
    }//GEN-LAST:event_btn_newbillActionPerformed

    //checked
    private void btn_viewbillActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_viewbillActionPerformed
        try {
            Config.retail_stock_view_bill.onloadReset(stockbillids.get(tbl_StockBill.getSelectedRow()));
            Config.retail_stock_view_bill.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select any row from the table..", "No row selected", JOptionPane.ERROR_MESSAGE);
        }        
    }//GEN-LAST:event_btn_viewbillActionPerformed

    //checked
    private void btn_cancel_1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancel_1ActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancel_1ActionPerformed

    //checked
    private void btn_search_2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_search_2ActionPerformed
        try {
            tbl_avialabilitymodel.setRowCount(0);
            int totalcount = 0;

            String pn;
            try {
                pn = cb_productname.getSelectedItem().toString();                
            } catch (Exception e) {
                pn = "";            
            }

            String fn;
            try {
                fn = cb_flavorname.getSelectedItem().toString();                
            } catch (Exception e) {
                fn = "";            
            }
            
            try {
                if (pn.equals("") && fn.equals("") ) {
                    Config.sql = "select * from retail_stock";
                } 
                else if (!pn.equals("") && fn.equals("") ) {
                    Config.sql = "select * from retail_stock where product = '"+pn+"' ";
                }
                else if (!pn.equals("") && !fn.equals("") ) {
                    Config.sql = "select * from retail_stock where product = '"+pn+"' and variety = '"+fn+"'";
                }
                System.out.println(Config.sql);
                Config.rs = Config.stmt.executeQuery(Config.sql);
                while (Config.rs.next()) {
                    tbl_avialabilitymodel.addRow(new Object[] {                    
                        Config.rs.getString("product"),
                        Config.rs.getString("variety"),
                        Config.rs.getString("mrp"),                    
                        Config.rs.getString("unit"),
                        Config.rs.getString("quantity")
                    });
                    totalcount++;
                }

                txt_totalItemCount.setText(String.valueOf(totalcount));
            } catch (SQLException ex) {
                ex.printStackTrace();
            }
        } catch (Exception e) {}
    }//GEN-LAST:event_btn_search_2ActionPerformed

    //checked
    private void btn_cancel_2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancel_2ActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancel_2ActionPerformed
    
    //checked
    private void jdc_datePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jdc_datePropertyChange
        btn_search_1ActionPerformed(null);
    }//GEN-LAST:event_jdc_datePropertyChange

    //checked
    private void cb_organizationActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_organizationActionPerformed
        btn_search_1ActionPerformed(evt);
    }//GEN-LAST:event_cb_organizationActionPerformed

    //checked
    private void cb_productnameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_productnameActionPerformed
        try {
            String product = cb_productname.getSelectedItem().toString();
            cb_flavorname.removeAllItems();
            for (int i = 0; i < Config.configrvariety.size(); i++) {
                if(Config.configrvariety.get(i).getRproductname().equals(product)) {
                     cb_flavorname.addItem(Config.configrvariety.get(i).getVarietyname());
                }            
            }

            btn_search_2ActionPerformed(evt);
        } catch (Exception e) {}
    }//GEN-LAST:event_cb_productnameActionPerformed

    //checked
    private void cb_flavornameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_flavornameActionPerformed
        btn_search_2ActionPerformed(evt);
    }//GEN-LAST:event_cb_flavornameActionPerformed

    //checked
    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        int op = JOptionPane.showConfirmDialog(this,"Are you really want to delete Bill entry ?","Confirm Delete",JOptionPane.YES_NO_OPTION);
        if (op == 0) {
            try {
                if (Config.retail_stock_bill_mgr.delRetailStockBill(stockbillids.get(tbl_StockBill.getSelectedRow()))) {
                    onloadReset();
                    JOptionPane.showMessageDialog(this, "Bill entry deleted successfully.", "Deletion successful", JOptionPane.NO_OPTION);                
                } else {
                    JOptionPane.showMessageDialog(this, "Error in entry deletion.", "Error", JOptionPane.ERROR_MESSAGE);
                }            
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this,ex.getMessage(),"Error : No row Selected",JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_jButton1ActionPerformed
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancel_1;
    private javax.swing.JButton btn_cancel_2;
    private javax.swing.JButton btn_newbill;
    private javax.swing.JButton btn_search_1;
    private javax.swing.JButton btn_search_2;
    private javax.swing.JButton btn_viewbill;
    private javax.swing.JComboBox cb_flavorname;
    private javax.swing.JComboBox cb_organization;
    private javax.swing.JComboBox cb_productname;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTabbedPane jTabbedPane1;
    private com.toedter.calendar.JDateChooser jdc_date;
    private javax.swing.JLabel lbl_searchby;
    private javax.swing.JLabel lbl_searchby1;
    private javax.swing.JTable tbl_Available;
    private javax.swing.JTable tbl_StockBill;
    private javax.swing.JTextField txt_totalItemCount;
    private javax.swing.JTextField txt_totalbalanceamt;
    private javax.swing.JTextField txt_totalpaidamt;
    private javax.swing.JTextField txt_totalpayableamt;
    // End of variables declaration//GEN-END:variables
        
  //checked
    public void onloadReset() {
        //tab 1 reset
            tbl_stockbillmodel.setRowCount(0);
            cb_organization.removeAllItems();
            for(int i=0; i<Config.supplierprofile.size(); i++) {
                cb_organization.addItem(Config.supplierprofile.get(i).getOrganization());
            }
            cb_organization.setSelectedIndex(-1);
            jdc_date.setDate(null);            
        //---------------------------------------------------------------------------------------------------

        //tab 2 reset
            tbl_avialabilitymodel.setRowCount(0);
            cb_productname.removeAllItems();        
            for (int i = 0; i < Config.configrproduct.size(); i++) {
                cb_productname.addItem(Config.configrproduct.get(i).getProductname());
            }
            cb_productname.setSelectedIndex(-1);
            cb_flavorname.setSelectedIndex(-1);
        //---------------------------------------------------------------------------------------------------
    }    
    
    //checked
    private void autosugg1() {
        tf1 = (JTextField) cb_organization.getEditor().getEditorComponent();
        tf1.addKeyListener(new KeyAdapter() {                
            @Override
            public void keyTyped(KeyEvent e) {            
            EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    String text = tf1.getText();
                    if(text.length()==0) {
                        cb_organization.hidePopup();
                        setModel1(new DefaultComboBoxModel(v1), "");
                    }else{
                        DefaultComboBoxModel m = getSuggestedModel1(v1, text);
                        if(m.getSize()==0 || hide_flag1) {
                            cb_organization.hidePopup();
                            hide_flag1 = false;
                        }else{
                            setModel1(m, text);
                            cb_organization.showPopup();
                        }
                    }
                }
            });
            }
            @Override
            public void keyPressed(KeyEvent e) {
                String text = tf1.getText();
                int code = e.getKeyCode();
                if(code==KeyEvent.VK_ENTER) {
                    if(!v1.contains(text)) {
                        //v.addElement(text);
                        Collections.sort(v1);
                        setModel1(getSuggestedModel1(v1, text), text);
                    }
                    hide_flag1 = true; 
                }else if(code==KeyEvent.VK_ESCAPE) {
                hide_flag1 = true; 
                }else if(code==KeyEvent.VK_RIGHT) {
                for(int i=0;i<v1.size();i++) {
                    String str = v1.elementAt(i);
                    if(str.startsWith(text)) {
                    cb_organization.setSelectedIndex(-1);
                    tf1.setText(str);
                    return;
                    }
                }
                }
            }
        });
        
        Collections.sort(v1);
        setModel1(new DefaultComboBoxModel(v1), "");
    }
    
    private void setModel1(DefaultComboBoxModel mdl, String str) {
        cb_organization.setModel(mdl);
        cb_organization.setSelectedIndex(-1);
        tf1.setText(str);
    }
    
    private DefaultComboBoxModel getSuggestedModel1(java.util.List<String> list, String text) {
        DefaultComboBoxModel m = new DefaultComboBoxModel();
        for(String s: list) {
            if(s.toLowerCase().startsWith(text.toLowerCase())) {
                m.addElement(s);
            }
        }
        return m;
    }   
}
