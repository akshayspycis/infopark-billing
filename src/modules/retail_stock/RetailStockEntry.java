package modules.retail_stock;

import modules.stock.*;
import datamanager.config.Config;
import datamanager.config.ConfigRVariety;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

public class RetailStockEntry extends javax.swing.JDialog {
     
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");    
    
    String parent = null;

        
    public RetailStockEntry(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        this.setLocationRelativeTo(null);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/icon.png")));        
        
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        panal = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        cb_product = new javax.swing.JComboBox();
        cb_flavor = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        cb_unit = new javax.swing.JComboBox();
        txt_quantity = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        txt_rate = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txt_totalamount = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txt_vatper = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txt_vatamt = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txt_purchase = new javax.swing.JTextField();
        jLabel15 = new javax.swing.JLabel();
        txt_nat_amt = new javax.swing.JTextField();
        btn_cancel = new javax.swing.JButton();
        btn_save = new javax.swing.JButton();
        btn_reset = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Stock Management - Stock Entry");
        setAlwaysOnTop(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        panal.setBackground(new java.awt.Color(255, 255, 255));
        panal.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Stock Entry", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setText("Product");

        jLabel2.setText("Variety");

        cb_product.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_productActionPerformed(evt);
            }
        });

        cb_flavor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_flavorActionPerformed(evt);
            }
        });

        jLabel7.setText("Unit ");

        cb_unit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Kg", "Lit", "Jar", "Box", "Poly Beg", "Pcs" }));
        cb_unit.setEnabled(false);

        txt_quantity.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_quantityCaretUpdate(evt);
            }
        });
        txt_quantity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_quantityActionPerformed(evt);
            }
        });

        jLabel8.setText("Quantity");

        jLabel12.setText("MRP");

        txt_rate.setEditable(false);
        txt_rate.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_rateCaretUpdate(evt);
            }
        });
        txt_rate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_rateActionPerformed(evt);
            }
        });

        jLabel4.setText("Total Amount");

        txt_totalamount.setEditable(false);
        txt_totalamount.setBackground(new java.awt.Color(245, 245, 245));
        txt_totalamount.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txt_totalamount.setEnabled(false);

        jLabel5.setText("GST %");

        txt_vatper.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_vatperCaretUpdate(evt);
            }
        });
        txt_vatper.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_vatperActionPerformed(evt);
            }
        });

        jLabel13.setText("GST Amount");

        txt_vatamt.setEditable(false);
        txt_vatamt.setBackground(new java.awt.Color(245, 245, 245));
        txt_vatamt.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txt_vatamt.setEnabled(false);

        jLabel14.setText("Purchase Price");

        txt_purchase.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_purchaseCaretUpdate(evt);
            }
        });
        txt_purchase.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_purchaseActionPerformed(evt);
            }
        });

        jLabel15.setText("Net Amount");

        txt_nat_amt.setEditable(false);
        txt_nat_amt.setBackground(new java.awt.Color(245, 245, 245));
        txt_nat_amt.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txt_nat_amt.setEnabled(false);

        javax.swing.GroupLayout panalLayout = new javax.swing.GroupLayout(panal);
        panal.setLayout(panalLayout);
        panalLayout.setHorizontalGroup(
            panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panalLayout.createSequentialGroup()
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cb_product, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cb_flavor, javax.swing.GroupLayout.PREFERRED_SIZE, 111, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(panalLayout.createSequentialGroup()
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel14)
                            .addComponent(txt_purchase, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 76, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_quantity, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(panalLayout.createSequentialGroup()
                                .addGap(11, 11, 11)
                                .addComponent(jLabel4))
                            .addGroup(panalLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txt_totalamount)))
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panalLayout.createSequentialGroup()
                                .addGap(16, 16, 16)
                                .addComponent(txt_vatper, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panalLayout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panalLayout.createSequentialGroup()
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cb_unit, 0, 104, Short.MAX_VALUE)
                            .addComponent(jLabel7, javax.swing.GroupLayout.DEFAULT_SIZE, 51, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txt_rate, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 32, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panalLayout.createSequentialGroup()
                        .addGap(5, 5, 5)
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panalLayout.createSequentialGroup()
                                .addComponent(jLabel13, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(14, 14, 14))
                            .addComponent(txt_vatamt, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panalLayout.createSequentialGroup()
                                .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(14, 14, 14))
                            .addComponent(txt_nat_amt, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 92, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel9))))
        );

        panalLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cb_unit, jLabel7});

        panalLayout.setVerticalGroup(
            panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(jLabel2))
                    .addGroup(panalLayout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addGap(6, 6, 6)
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cb_product, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cb_flavor, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cb_unit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panalLayout.createSequentialGroup()
                        .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_rate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panalLayout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_totalamount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_vatper, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panalLayout.createSequentialGroup()
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_purchase, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_quantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(panalLayout.createSequentialGroup()
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(jLabel5))
                        .addGap(7, 7, 7)
                        .addComponent(txt_vatamt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panalLayout.createSequentialGroup()
                        .addComponent(jLabel15)
                        .addGap(7, 7, 7)
                        .addComponent(txt_nat_amt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
            .addGroup(panalLayout.createSequentialGroup()
                .addGap(80, 80, 80)
                .addComponent(jLabel9))
        );

        panalLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cb_flavor, cb_product});

        panalLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cb_unit, txt_quantity});

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });

        btn_save.setText("Add");
        btn_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_saveActionPerformed(evt);
            }
        });

        btn_reset.setText("Reset");
        btn_reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_resetActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btn_reset)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_cancel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_save, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_cancel, btn_reset, btn_save});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_save)
                        .addComponent(btn_cancel))
                    .addComponent(btn_reset))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getRootPane().setDefaultButton(btn_save);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //checked
    private void btn_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_saveActionPerformed
        String str = checkValidity();   
        if (str.equals("ok")) {
            String[] data = new String[10];            
                data[0] = cb_product.getSelectedItem().toString();
            try {
                data[1] = cb_flavor.getSelectedItem().toString();
            } catch (Exception e) {
                data[1] = "";
            }
                data[2] = cb_unit.getSelectedItem().toString();
                data[3] = txt_rate.getText();
                data[4] = txt_purchase.getText();
                for (int i = 0; i < Config.configrvariety.size(); i++) {
                    try {
                        ConfigRVariety co=Config.configrvariety.get(i);
                        if( cb_product.getSelectedItem().toString().equals(co.getRproductname()) && cb_flavor.getSelectedItem().toString().equals(co.getVarietyname())){
                            Config.configrvariety.get(i).setPurchase_price(txt_purchase.getText());
                        }
                    }catch(Exception e){}
                }
                data[5] = txt_quantity.getText();            
                data[6] = txt_totalamount.getText();
                data[7] = txt_vatper.getText();
                data[8] = txt_vatamt.getText();
                data[9] = txt_nat_amt.getText();
            if (parent.equals("new")) {
                Config.retail_stock_new_bill.insertRow(data);
            } else {
                Config.retail_stock_view_bill.insertRow(data);
            }            
            onloadReset(parent);
        } else {
           JOptionPane.showMessageDialog(this, "Check '"+str+"' field.", "Error", JOptionPane.ERROR_MESSAGE);
        }   
    }//GEN-LAST:event_btn_saveActionPerformed
    
    //checked
    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancelActionPerformed
    
    //checked
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    //checked
    private void btn_resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_resetActionPerformed
        onloadReset(parent);
    }//GEN-LAST:event_btn_resetActionPerformed

    private void txt_vatperActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_vatperActionPerformed
        btn_saveActionPerformed(evt);
    }//GEN-LAST:event_txt_vatperActionPerformed

    private void txt_vatperCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_vatperCaretUpdate
        cal();
    }//GEN-LAST:event_txt_vatperCaretUpdate

    //checked
    private void txt_rateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_rateActionPerformed
        btn_saveActionPerformed(evt);
    }//GEN-LAST:event_txt_rateActionPerformed

    //checked
    private void txt_rateCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_rateCaretUpdate
        cal();
    }//GEN-LAST:event_txt_rateCaretUpdate

    //checked
    private void txt_quantityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_quantityActionPerformed
        btn_saveActionPerformed(evt);
    }//GEN-LAST:event_txt_quantityActionPerformed

    //checked
    private void txt_quantityCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_quantityCaretUpdate
        cal();
    }//GEN-LAST:event_txt_quantityCaretUpdate

    //checked
    private void cb_productActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_productActionPerformed
        try {
            String product = cb_product.getSelectedItem().toString();
            cb_flavor.removeAllItems();
            for (int i = 0; i < Config.configrvariety.size(); i++) {
                if( Config.configrvariety.get(i).getRproductname().equals(product)) {
                    cb_flavor.addItem(Config.configrvariety.get(i).getVarietyname());
                }
            }
            if(cb_flavor.getItemCount()==0){
                cb_unit.setSelectedIndex(0);
            }else{
                cb_flavor.setSelectedIndex(0);
            }
            
        }catch (Exception e) {}
    }//GEN-LAST:event_cb_productActionPerformed

    private void cb_flavorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_flavorActionPerformed
          try {
            String product = cb_product.getSelectedItem().toString();
            String varietyname = cb_flavor.getSelectedItem().toString();
            for (int i = 0; i < Config.configrvariety.size(); i++) {
                if( Config.configrvariety.get(i).getRproductname().equals(product) && Config.configrvariety.get(i).getVarietyname().equals(varietyname)) {
                    cb_unit.setSelectedItem(Config.configrvariety.get(i).getUnit());
                    txt_rate.setText(Config.configrvariety.get(i).getRate());
                    if(Config.configrvariety.get(i).getPurchase_price()!=null)
                    txt_purchase.setText(Config.configrvariety.get(i).getPurchase_price());
                    else
                    txt_purchase.setText("0.0");
                    break;
                }
            }
             
        }
        catch (Exception e) {}
    }//GEN-LAST:event_cb_flavorActionPerformed

    private void txt_purchaseCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_purchaseCaretUpdate
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_purchaseCaretUpdate

    private void txt_purchaseActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_purchaseActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_purchaseActionPerformed

      
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_reset;
    private javax.swing.JButton btn_save;
    private javax.swing.JComboBox cb_flavor;
    private javax.swing.JComboBox cb_product;
    private javax.swing.JComboBox cb_unit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel panal;
    private javax.swing.JTextField txt_nat_amt;
    private javax.swing.JTextField txt_purchase;
    private javax.swing.JTextField txt_quantity;
    private javax.swing.JTextField txt_rate;
    private javax.swing.JTextField txt_totalamount;
    private javax.swing.JTextField txt_vatamt;
    private javax.swing.JTextField txt_vatper;
    // End of variables declaration//GEN-END:variables
    
    //checked
    public void onloadReset(String what) {
        parent = what;     
        cb_product.removeAllItems();
        for(int i=0; i<Config.configrproduct.size(); i++) {
            cb_product.addItem(Config.configrproduct.get(i).getProductname());
        }
        cb_product.setSelectedIndex(0);
//        txt_purchase.setText("0.0");
        txt_quantity.setText("0");
        txt_vatper.setText(Config.config_other_config.get(0).getRetail_stock_vat());
        txt_vatamt.setText("0.0");
        
    }    

    //checked
    private String checkValidity() {
        if (cb_unit.getSelectedIndex() == 0) {
            return "Unit";
        }else if (txt_rate.getText().equals("")) {
            return "Rate";
        }else if (txt_quantity.getText().equals("")) {
            return "Quantity";
        }else if (txt_totalamount.getText().equals("") || txt_totalamount.getText().equals("0.0")) {
            return "Total Amount";
        }else {
            return "ok";
        }
    }
    
    //checked
    public void cal() {
        try {
            int quantity = Integer.parseInt(txt_quantity.getText());
            float rate = Float.parseFloat(txt_purchase.getText());
            float totalamount = rate * quantity;            
            txt_totalamount.setText(String.valueOf(totalamount));
            float vatamt = totalamount*Float.parseFloat(txt_vatper.getText())/100;
            float netamt = totalamount+vatamt;
            txt_vatamt.setText(String.valueOf(vatamt));
            txt_nat_amt.setText(String.valueOf(netamt));
        } catch(Exception e){
            txt_totalamount.setText("0.0");
        }
    }
    
//    //checked
//    private void calculateTotalItem() {
//        try {
//            txt_totalitem.setText(String.valueOf(Integer.parseInt(txt_quantity.getText())*Integer.parseInt(txt_pack.getText())));
//        } catch(Exception e){
//            txt_totalitem.setText(String.valueOf(0));
//        }        
//    }
//    
//    //checked
//    private void calculateVatAmount() {
//        try {
//            int quantity = Integer.parseInt(txt_quantity.getText());
//            float tp = Float.parseFloat(txt_rate.getText()) * quantity;
//            float vatamount = (tp * Float.parseFloat(txt_vatpercentage.getText()))/100;
//
//            txt_vatamount.setText(String.valueOf(vatamount));
//        } catch(Exception e){
//            txt_vatamount.setText("0.0");
//        }
//    }
//    
//    //checked
//    private void calculateGrossAmount() {
//        try {
//            int quantity = Integer.parseInt(txt_quantity.getText());
//            float tp = Float.parseFloat(txt_rate.getText()) * quantity;
//            float vatamount = (tp * Float.parseFloat(txt_vatpercentage.getText()))/100;
//
//            txt_grossamount.setText(String.valueOf(tp + vatamount));
//        } catch (Exception e) {
//            txt_grossamount.setText("0.0");
//        }        
//    }
//    
//    //checked
//    private void calculateProfitePercentage() {
//        try {
//            int quantity = Integer.parseInt(txt_quantity.getText());
//            float sp = Float.parseFloat(txt_mrp.getText()) * quantity;
//            float tp = Float.parseFloat(txt_rate.getText()) * quantity;
//            float cp = tp+((tp * Float.parseFloat(txt_vatpercentage.getText()))/100);
//            float profitpercentage = ((sp - cp)/cp)*100;
//
//            txt_profitpercentage.setText(String.valueOf(profitpercentage));
//        } catch (Exception e) {
//            txt_profitpercentage.setText("0.0");
//        }        
//    }
//    
    
}
