package modules.configuration;

import datamanager.config.ConfigRProduct;
import datamanager.config.Config;
import datamanager.config.ConfigContentRate;
import datamanager.config.ConfigFlavor;
import datamanager.config.ConfigProduct;
import datamanager.config.ConfigProfit;
import datamanager.config.ConfigRVariety;
import datamanager.config.ConfigScheme;
import datamanager.config.OtherConfig;
import datamanager.config.WholesaleGst;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;


public class ProductConfiguration extends javax.swing.JDialog {
    
    DefaultTableModel tbl_productmodel;
    DefaultTableModel tbl_gst_model;
    DefaultTableModel tbl_flavormodel;
    DefaultTableModel tbl_contentmodel;
    DefaultTableModel tbl_schememodel;
    DefaultTableModel tbl_profitmodel;
    DefaultTableModel tbl_RProductName_model;
    DefaultTableModel tbl_r_variety_model;
    boolean pos58=false;
    boolean pos80=false;
    boolean posA4=false;
    boolean rmedical=false;
    boolean retailbillingstock=false;
    public ProductConfiguration(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        
        
        this.setLocationRelativeTo(null);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/icon.png")));
        
        tbl_productmodel = (DefaultTableModel) tbl_ProductName.getModel();
        tbl_gst_model = (DefaultTableModel) tbl_gst.getModel();
        tbl_flavormodel = (DefaultTableModel) tbl_FlavorName.getModel();
        tbl_contentmodel = (DefaultTableModel) tbl_ContentRate.getModel();
        tbl_schememodel = (DefaultTableModel) tbl_Scheme.getModel();
        tbl_profitmodel = (DefaultTableModel) tbl_Profit.getModel();
        tbl_RProductName_model=(DefaultTableModel) tbl_RProductName.getModel();
        tbl_r_variety_model=(DefaultTableModel) tbl_r_variety.getModel();
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });        
    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup3 = new javax.swing.ButtonGroup();
        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jTabbedPane2 = new javax.swing.JTabbedPane();
        jPanel8 = new javax.swing.JPanel();
        jSplitPane1 = new javax.swing.JSplitPane();
        jPanel2 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_Product = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tbl_ProductName = new javax.swing.JTable();
        btn_ProductRemove = new javax.swing.JButton();
        btn_ProductSave = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jPanel23 = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        tbl_gst = new javax.swing.JTable();
        btn_gst_remove = new javax.swing.JButton();
        btn_gst_save = new javax.swing.JButton();
        jLabel19 = new javax.swing.JLabel();
        cb_product_gst = new javax.swing.JComboBox();
        jLabel20 = new javax.swing.JLabel();
        txt_above_rate = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        txt_gst = new javax.swing.JTextField();
        jButton7 = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jPanel4 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tbl_FlavorName = new javax.swing.JTable();
        btn_FlavorRemove = new javax.swing.JButton();
        cb_Product = new javax.swing.JComboBox();
        txt_Flavor = new javax.swing.JTextField();
        btn_FlavorSave = new javax.swing.JButton();
        jPanel10 = new javax.swing.JPanel();
        jLabel10 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbl_ContentRate = new javax.swing.JTable();
        btn_ContentRateRemove = new javax.swing.JButton();
        txt_ContentRate = new javax.swing.JTextField();
        btn_ContentRateSave = new javax.swing.JButton();
        jPanel12 = new javax.swing.JPanel();
        jSplitPane2 = new javax.swing.JSplitPane();
        jPanel13 = new javax.swing.JPanel();
        jPanel14 = new javax.swing.JPanel();
        jLabel5 = new javax.swing.JLabel();
        txt_rproductname = new javax.swing.JTextField();
        jScrollPane6 = new javax.swing.JScrollPane();
        tbl_RProductName = new javax.swing.JTable();
        btn_ProductRemove1 = new javax.swing.JButton();
        btn_ProductSave1 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();
        jPanel15 = new javax.swing.JPanel();
        jPanel16 = new javax.swing.JPanel();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane7 = new javax.swing.JScrollPane();
        tbl_r_variety = new javax.swing.JTable();
        btn_FlavorRemove1 = new javax.swing.JButton();
        cb_r_Product = new javax.swing.JComboBox();
        txt_variety = new javax.swing.JTextField();
        btn_FlavorSave1 = new javax.swing.JButton();
        btn_ProductSave2 = new javax.swing.JButton();
        jLabel11 = new javax.swing.JLabel();
        txt_r_ContentRate = new javax.swing.JTextField();
        txt_barocde = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        cb_unit = new javax.swing.JComboBox();
        cb_unit_variety = new javax.swing.JComboBox();
        jLabel18 = new javax.swing.JLabel();
        txt_discount = new javax.swing.JTextField();
        jPanel7 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        btn_SchemeAdd = new javax.swing.JButton();
        txt_Percentage = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txt_Amount = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_Scheme = new javax.swing.JTable();
        btn_SchemeRemove = new javax.swing.JButton();
        btn_SchemeSave = new javax.swing.JButton();
        jPanel11 = new javax.swing.JPanel();
        jScrollPane5 = new javax.swing.JScrollPane();
        tbl_Profit = new javax.swing.JTable();
        btn_ProfitRemove = new javax.swing.JButton();
        btn_ProfitSave = new javax.swing.JButton();
        jPanel17 = new javax.swing.JPanel();
        jPanel20 = new javax.swing.JPanel();
        jPanel18 = new javax.swing.JPanel();
        jLabel12 = new javax.swing.JLabel();
        txt_whole_sale_stock_vat = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        txt_retail_stock_vat = new javax.swing.JTextField();
        jPanel19 = new javax.swing.JPanel();
        jLabel16 = new javax.swing.JLabel();
        txt_whole_sale_stock_discount = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        txt_retail_stock_discount = new javax.swing.JTextField();
        jButton3 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jPanel21 = new javax.swing.JPanel();
        wholesale_billing_stock = new javax.swing.JCheckBox();
        retail_billing_stock = new javax.swing.JCheckBox();
        pos_58 = new javax.swing.JRadioButton();
        pos_80 = new javax.swing.JRadioButton();
        pos_a4 = new javax.swing.JRadioButton();
        jPanel22 = new javax.swing.JPanel();
        radio_medical = new javax.swing.JRadioButton();
        radio_other = new javax.swing.JRadioButton();

        buttonGroup3.add(pos_58);
        buttonGroup3.add(pos_80);
        buttonGroup3.add(pos_a4);

        buttonGroup1.add(radio_medical);
        buttonGroup1.add(radio_other);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Product Configuration");
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel3.setBackground(new java.awt.Color(2, 79, 99));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Product Configuration");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jTabbedPane2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jTabbedPane2.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                jTabbedPane2StateChanged(evt);
            }
        });

        jPanel8.setBackground(new java.awt.Color(255, 255, 255));

        jSplitPane1.setBorder(null);
        jSplitPane1.setDividerLocation(496);
        jSplitPane1.setDividerSize(2);

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Product", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setText("Product :");

        txt_Product.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_ProductActionPerformed(evt);
            }
        });

        tbl_ProductName.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tbl_ProductName.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "PRODUCT NAME", "HSN CODE", "BAR CODE", "DISCOUNT ( %)"
            }
        ));
        tbl_ProductName.setRowHeight(20);
        tbl_ProductName.setSelectionBackground(new java.awt.Color(96, 118, 159));
        tbl_ProductName.getTableHeader().setReorderingAllowed(false);
        tbl_ProductName.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                tbl_ProductNamePropertyChange(evt);
            }
        });
        jScrollPane1.setViewportView(tbl_ProductName);
        if (tbl_ProductName.getColumnModel().getColumnCount() > 0) {
            tbl_ProductName.getColumnModel().getColumn(0).setResizable(false);
            tbl_ProductName.getColumnModel().getColumn(1).setResizable(false);
            tbl_ProductName.getColumnModel().getColumn(2).setResizable(false);
            tbl_ProductName.getColumnModel().getColumn(3).setResizable(false);
        }
        tbl_ProductName.getColumnModel().getColumn(0).setPreferredWidth(160);

        btn_ProductRemove.setText("Remove");
        btn_ProductRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ProductRemoveActionPerformed(evt);
            }
        });

        btn_ProductSave.setText("Save");
        btn_ProductSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ProductSaveActionPerformed(evt);
            }
        });

        jButton4.setText("Add");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_Product)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton4)
                .addContainerGap())
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 464, Short.MAX_VALUE)
                .addGap(10, 10, 10))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btn_ProductRemove)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_ProductSave)
                .addContainerGap())
        );

        jPanel5Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_ProductRemove, btn_ProductSave});

        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_Product, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 288, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_ProductRemove)
                    .addComponent(btn_ProductSave)))
        );

        jPanel5Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel1, txt_Product});

        jPanel23.setBackground(new java.awt.Color(255, 255, 255));
        jPanel23.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Gst Configuration", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        tbl_gst.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tbl_gst.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "ABOVE RATE", "Gst"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_gst.setRowHeight(20);
        tbl_gst.setSelectionBackground(new java.awt.Color(96, 118, 159));
        tbl_gst.getTableHeader().setReorderingAllowed(false);
        tbl_gst.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                tbl_gstPropertyChange(evt);
            }
        });
        jScrollPane8.setViewportView(tbl_gst);
        if (tbl_gst.getColumnModel().getColumnCount() > 0) {
            tbl_gst.getColumnModel().getColumn(0).setResizable(false);
            tbl_gst.getColumnModel().getColumn(1).setResizable(false);
        }

        btn_gst_remove.setText("Remove");
        btn_gst_remove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_gst_removeActionPerformed(evt);
            }
        });

        btn_gst_save.setText("Save");
        btn_gst_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_gst_saveActionPerformed(evt);
            }
        });

        jLabel19.setText("Product ");

        cb_product_gst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_product_gstActionPerformed(evt);
            }
        });

        jLabel20.setText("Above Rate ");

        txt_above_rate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_above_rateActionPerformed(evt);
            }
        });

        jLabel21.setText("Gst :");

        txt_gst.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_gstActionPerformed(evt);
            }
        });

        jButton7.setText("Add");
        jButton7.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton7ActionPerformed(evt);
            }
        });

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        javax.swing.GroupLayout jPanel23Layout = new javax.swing.GroupLayout(jPanel23);
        jPanel23.setLayout(jPanel23Layout);
        jPanel23Layout.setHorizontalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel23Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel19)
                    .addGroup(jPanel23Layout.createSequentialGroup()
                        .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(txt_above_rate)
                            .addComponent(jLabel20, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel23Layout.createSequentialGroup()
                                .addComponent(txt_gst, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton7))
                            .addComponent(jLabel21)))
                    .addComponent(cb_product_gst, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 6, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btn_gst_remove, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_gst_save, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        jPanel23Layout.setVerticalGroup(
            jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel23Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jSeparator1)
                    .addGroup(jPanel23Layout.createSequentialGroup()
                        .addComponent(btn_gst_remove)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_gst_save))
                    .addGroup(jPanel23Layout.createSequentialGroup()
                        .addComponent(jLabel19)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cb_product_gst, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel21)
                            .addComponent(jLabel20))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel23Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton7)
                            .addComponent(txt_gst, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_above_rate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addGap(2, 2, 2))
        );

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel23, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel23, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(51, 51, 51))
        );

        jSplitPane1.setLeftComponent(jPanel2);

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Flavor", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel3.setText("Product :");

        jLabel4.setText("Flavor :");

        tbl_FlavorName.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tbl_FlavorName.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "FLAVOR NAME"
            }
        ));
        tbl_FlavorName.setRowHeight(20);
        tbl_FlavorName.setSelectionBackground(new java.awt.Color(96, 118, 159));
        tbl_FlavorName.getTableHeader().setReorderingAllowed(false);
        jScrollPane2.setViewportView(tbl_FlavorName);
        if (tbl_FlavorName.getColumnModel().getColumnCount() > 0) {
            tbl_FlavorName.getColumnModel().getColumn(0).setResizable(false);
        }

        btn_FlavorRemove.setText("Remove");
        btn_FlavorRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_FlavorRemoveActionPerformed(evt);
            }
        });

        cb_Product.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_ProductActionPerformed(evt);
            }
        });

        txt_Flavor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_FlavorActionPerformed(evt);
            }
        });

        btn_FlavorSave.setText("Save");
        btn_FlavorSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_FlavorSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(cb_Product, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_Flavor, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 544, Short.MAX_VALUE)
                        .addGap(10, 10, 10))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(btn_FlavorRemove)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_FlavorSave)
                        .addContainerGap())))
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_FlavorRemove, btn_FlavorSave});

        jPanel6Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cb_Product, txt_Flavor});

        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_Flavor, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(cb_Product, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_FlavorRemove)
                    .addComponent(btn_FlavorSave)))
        );

        jPanel6Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cb_Product, jLabel3, jLabel4});

        jPanel10.setBackground(new java.awt.Color(255, 255, 255));
        jPanel10.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Content Rate", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel10.setText("Content Rate :");

        tbl_ContentRate.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tbl_ContentRate.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "PRODUCT CONTENT RATE"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_ContentRate.setRowHeight(20);
        tbl_ContentRate.setSelectionBackground(new java.awt.Color(96, 118, 159));
        tbl_ContentRate.getTableHeader().setReorderingAllowed(false);
        jScrollPane4.setViewportView(tbl_ContentRate);
        if (tbl_ContentRate.getColumnModel().getColumnCount() > 0) {
            tbl_ContentRate.getColumnModel().getColumn(0).setResizable(false);
        }

        btn_ContentRateRemove.setText("Remove");
        btn_ContentRateRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ContentRateRemoveActionPerformed(evt);
            }
        });

        txt_ContentRate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_ContentRateActionPerformed(evt);
            }
        });

        btn_ContentRateSave.setText("Save");
        btn_ContentRateSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ContentRateSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel10Layout = new javax.swing.GroupLayout(jPanel10);
        jPanel10.setLayout(jPanel10Layout);
        jPanel10Layout.setHorizontalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_ContentRate)
                .addContainerGap())
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addGap(10, 10, 10))
                    .addGroup(jPanel10Layout.createSequentialGroup()
                        .addComponent(btn_ContentRateRemove)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_ContentRateSave)
                        .addContainerGap())))
        );

        jPanel10Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_ContentRateRemove, btn_ContentRateSave});

        jPanel10Layout.setVerticalGroup(
            jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel10Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_ContentRate, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel10Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_ContentRateRemove)
                    .addComponent(btn_ContentRateSave))
                .addGap(6, 6, 6))
        );

        jPanel10Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {jLabel10, txt_ContentRate});

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel10, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel10, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41))
        );

        jSplitPane1.setRightComponent(jPanel4);

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1)
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 543, Short.MAX_VALUE)
        );

        jTabbedPane2.addTab("Whole Sale Product Configuration", jPanel8);

        jSplitPane2.setBorder(null);
        jSplitPane2.setDividerLocation(496);
        jSplitPane2.setDividerSize(2);

        jPanel13.setBackground(new java.awt.Color(255, 255, 255));

        jPanel14.setBackground(new java.awt.Color(255, 255, 255));
        jPanel14.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Product", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel5.setText("Product :");

        txt_rproductname.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_rproductnameActionPerformed(evt);
            }
        });

        tbl_RProductName.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tbl_RProductName.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "PRODUCT NAME"
            }
        ));
        tbl_RProductName.setRowHeight(20);
        tbl_RProductName.setSelectionBackground(new java.awt.Color(96, 118, 159));
        tbl_RProductName.getTableHeader().setReorderingAllowed(false);
        jScrollPane6.setViewportView(tbl_RProductName);

        btn_ProductRemove1.setText("Remove");
        btn_ProductRemove1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ProductRemove1ActionPerformed(evt);
            }
        });

        btn_ProductSave1.setText("Add");
        btn_ProductSave1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ProductSave1ActionPerformed(evt);
            }
        });

        jButton1.setText("Save");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel14Layout = new javax.swing.GroupLayout(jPanel14);
        jPanel14.setLayout(jPanel14Layout);
        jPanel14Layout.setHorizontalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(10, 10, 10))
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_rproductname, javax.swing.GroupLayout.DEFAULT_SIZE, 335, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_ProductSave1))
                    .addGroup(jPanel14Layout.createSequentialGroup()
                        .addComponent(btn_ProductRemove1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        jPanel14Layout.setVerticalGroup(
            jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel14Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_rproductname, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_ProductSave1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel14Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_ProductRemove1)
                    .addComponent(jButton1))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel13Layout = new javax.swing.GroupLayout(jPanel13);
        jPanel13.setLayout(jPanel13Layout);
        jPanel13Layout.setHorizontalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel13Layout.setVerticalGroup(
            jPanel13Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel13Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(33, 33, 33))
        );

        jSplitPane2.setLeftComponent(jPanel13);

        jPanel15.setBackground(new java.awt.Color(255, 255, 255));

        jPanel16.setBackground(new java.awt.Color(255, 255, 255));
        jPanel16.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Variety", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel6.setText("Product :");

        jLabel7.setText("Variety :");

        tbl_r_variety.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tbl_r_variety.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "VARIETY NAME", "Barcode", "MRP", "UNIT", "DISCOUNT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                true, false, true, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_r_variety.setRowHeight(20);
        tbl_r_variety.setSelectionBackground(new java.awt.Color(96, 118, 159));
        tbl_r_variety.getTableHeader().setReorderingAllowed(false);
        jScrollPane7.setViewportView(tbl_r_variety);
        if (tbl_r_variety.getColumnModel().getColumnCount() > 0) {
            tbl_r_variety.getColumnModel().getColumn(0).setResizable(false);
            tbl_r_variety.getColumnModel().getColumn(1).setResizable(false);
            tbl_r_variety.getColumnModel().getColumn(2).setResizable(false);
            tbl_r_variety.getColumnModel().getColumn(3).setResizable(false);
            tbl_r_variety.getColumnModel().getColumn(4).setResizable(false);
        }

        btn_FlavorRemove1.setText("Remove");
        btn_FlavorRemove1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_FlavorRemove1ActionPerformed(evt);
            }
        });

        cb_r_Product.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_r_ProductActionPerformed(evt);
            }
        });

        txt_variety.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_varietyActionPerformed(evt);
            }
        });

        btn_FlavorSave1.setText("Save");
        btn_FlavorSave1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_FlavorSave1ActionPerformed(evt);
            }
        });

        btn_ProductSave2.setText("Add");
        btn_ProductSave2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ProductSave2ActionPerformed(evt);
            }
        });

        jLabel11.setText("MRP ");

        txt_r_ContentRate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_r_ContentRateActionPerformed(evt);
            }
        });

        txt_barocde.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_barocdeKeyReleased(evt);
            }
        });

        jLabel13.setText("Barcode");

        jLabel15.setText("Unit");

        cb_unit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "- Select -", "Kg", "Lit", "Jar", "Box", "Poly Beg", "Pcs" }));

        cb_unit_variety.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "- Select -", "Kg", "Lit", "Jar", "Box", "Poly Beg", "Pcs" }));

        jLabel18.setText("Discount %");

        javax.swing.GroupLayout jPanel16Layout = new javax.swing.GroupLayout(jPanel16);
        jPanel16.setLayout(jPanel16Layout);
        jPanel16Layout.setHorizontalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel16Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(jScrollPane7, javax.swing.GroupLayout.DEFAULT_SIZE, 564, Short.MAX_VALUE)
                        .addGap(10, 10, 10))
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(btn_FlavorRemove1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_FlavorSave1)
                        .addContainerGap())))
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cb_r_Product, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel16Layout.createSequentialGroup()
                                .addComponent(jLabel13)
                                .addGap(102, 102, 102)
                                .addComponent(jLabel11, javax.swing.GroupLayout.PREFERRED_SIZE, 72, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(40, 40, 40)
                                .addComponent(jLabel15)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(txt_barocde, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_r_ContentRate, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cb_unit_variety, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jLabel7)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel16Layout.createSequentialGroup()
                            .addComponent(txt_discount)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                            .addComponent(btn_ProductSave2))
                        .addComponent(txt_variety, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jLabel18))
                .addContainerGap())
            .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel16Layout.createSequentialGroup()
                    .addGap(178, 178, 178)
                    .addComponent(cb_unit, javax.swing.GroupLayout.PREFERRED_SIZE, 104, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(178, Short.MAX_VALUE)))
        );
        jPanel16Layout.setVerticalGroup(
            jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel16Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(cb_r_Product, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel13)
                            .addComponent(jLabel11)
                            .addComponent(jLabel15))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txt_barocde, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_r_ContentRate, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn_ProductSave2)
                            .addComponent(cb_unit_variety, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txt_discount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(11, 11, 11))
                    .addGroup(jPanel16Layout.createSequentialGroup()
                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_variety, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jLabel18)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_FlavorRemove1)
                    .addComponent(btn_FlavorSave1))
                .addContainerGap())
            .addGroup(jPanel16Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel16Layout.createSequentialGroup()
                    .addGap(235, 235, 235)
                    .addComponent(cb_unit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(236, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout jPanel15Layout = new javax.swing.GroupLayout(jPanel15);
        jPanel15.setLayout(jPanel15Layout);
        jPanel15Layout.setHorizontalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel15Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel16, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel15Layout.setVerticalGroup(
            jPanel15Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel15Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jPanel16, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(43, 43, 43))
        );

        jSplitPane2.setRightComponent(jPanel15);

        javax.swing.GroupLayout jPanel12Layout = new javax.swing.GroupLayout(jPanel12);
        jPanel12.setLayout(jPanel12Layout);
        jPanel12Layout.setHorizontalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSplitPane2)
        );
        jPanel12Layout.setVerticalGroup(
            jPanel12Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel12Layout.createSequentialGroup()
                .addComponent(jSplitPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 534, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Retail Product Configuration", jPanel12);

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));

        btn_SchemeAdd.setText("Add");
        btn_SchemeAdd.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SchemeAddActionPerformed(evt);
            }
        });

        jLabel9.setText("Percentage :");

        jLabel8.setText("Amount :");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createSequentialGroup()
                .addGap(225, 225, 225)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_Amount, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel9)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_Percentage, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_SchemeAdd)
                .addContainerGap(348, Short.MAX_VALUE))
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_Percentage, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_SchemeAdd, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_Amount, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel9Layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {btn_SchemeAdd, jLabel8, jLabel9, txt_Percentage});

        tbl_Scheme.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tbl_Scheme.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "Amount", "Percentage"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_Scheme.setRowHeight(20);
        tbl_Scheme.setSelectionBackground(new java.awt.Color(96, 118, 159));
        tbl_Scheme.getTableHeader().setReorderingAllowed(false);
        jScrollPane3.setViewportView(tbl_Scheme);

        btn_SchemeRemove.setText("Remove");
        btn_SchemeRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SchemeRemoveActionPerformed(evt);
            }
        });

        btn_SchemeSave.setText("Save");
        btn_SchemeSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SchemeSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(btn_SchemeRemove)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_SchemeSave)))
                .addContainerGap())
        );

        jPanel7Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_SchemeRemove, btn_SchemeSave});

        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 419, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_SchemeRemove)
                    .addComponent(btn_SchemeSave))
                .addContainerGap())
        );

        jTabbedPane2.addTab("Scheme Configuration", jPanel7);

        jPanel11.setBackground(new java.awt.Color(255, 255, 255));

        tbl_Profit.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tbl_Profit.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "PRODUCT", "FLAVOR", "PROFIT PERCENTAGE"
            }
        ) {
            Class[] types = new Class [] {
                java.lang.Object.class, java.lang.Object.class, java.lang.Float.class
            };
            boolean[] canEdit = new boolean [] {
                false, false, true
            };

            public Class getColumnClass(int columnIndex) {
                return types [columnIndex];
            }

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_Profit.setRowHeight(20);
        tbl_Profit.setSelectionBackground(new java.awt.Color(96, 118, 159));
        tbl_Profit.getTableHeader().setReorderingAllowed(false);
        jScrollPane5.setViewportView(tbl_Profit);
        if (tbl_Profit.getColumnModel().getColumnCount() > 0) {
            tbl_Profit.getColumnModel().getColumn(0).setResizable(false);
            tbl_Profit.getColumnModel().getColumn(0).setPreferredWidth(400);
            tbl_Profit.getColumnModel().getColumn(1).setResizable(false);
            tbl_Profit.getColumnModel().getColumn(1).setPreferredWidth(400);
            tbl_Profit.getColumnModel().getColumn(2).setResizable(false);
            tbl_Profit.getColumnModel().getColumn(2).setPreferredWidth(200);
        }

        btn_ProfitRemove.setText("Remove");
        btn_ProfitRemove.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ProfitRemoveActionPerformed(evt);
            }
        });

        btn_ProfitSave.setText("Save");
        btn_ProfitSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ProfitSaveActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 1094, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel11Layout.createSequentialGroup()
                        .addComponent(btn_ProfitRemove)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_ProfitSave)))
                .addContainerGap())
        );

        jPanel11Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_ProfitRemove, btn_ProfitSave});

        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane5, javax.swing.GroupLayout.DEFAULT_SIZE, 464, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_ProfitRemove)
                    .addComponent(btn_ProfitSave))
                .addContainerGap())
        );

        jTabbedPane2.addTab("Profit Configuration", jPanel11);

        jPanel17.setBackground(new java.awt.Color(255, 255, 255));

        jPanel20.setBackground(new java.awt.Color(255, 255, 255));
        jPanel20.setBorder(javax.swing.BorderFactory.createTitledBorder("Basic Configuration"));

        jPanel18.setBackground(new java.awt.Color(255, 255, 255));
        jPanel18.setBorder(javax.swing.BorderFactory.createTitledBorder("Vat Configuration"));

        jLabel12.setText("WholeSale GST %");

        txt_whole_sale_stock_vat.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_whole_sale_stock_vatCaretUpdate(evt);
            }
        });
        txt_whole_sale_stock_vat.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_whole_sale_stock_vatFocusLost(evt);
            }
        });

        jLabel14.setText("Retail  GST %");

        txt_retail_stock_vat.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_retail_stock_vatFocusLost(evt);
            }
        });

        javax.swing.GroupLayout jPanel18Layout = new javax.swing.GroupLayout(jPanel18);
        jPanel18.setLayout(jPanel18Layout);
        jPanel18Layout.setHorizontalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12)
                    .addComponent(jLabel14, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(txt_whole_sale_stock_vat, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE)
                        .addComponent(txt_retail_stock_vat, javax.swing.GroupLayout.Alignment.LEADING)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel18Layout.setVerticalGroup(
            jPanel18Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel18Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_whole_sale_stock_vat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel14)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_retail_stock_vat, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41))
        );

        jPanel19.setBackground(new java.awt.Color(255, 255, 255));
        jPanel19.setBorder(javax.swing.BorderFactory.createTitledBorder("Discount Information"));

        jLabel16.setText("WholeSale %");

        txt_whole_sale_stock_discount.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_whole_sale_stock_discountCaretUpdate(evt);
            }
        });
        txt_whole_sale_stock_discount.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_whole_sale_stock_discountFocusLost(evt);
            }
        });

        jLabel17.setText("Retail  %");

        txt_retail_stock_discount.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_retail_stock_discountFocusLost(evt);
            }
        });

        javax.swing.GroupLayout jPanel19Layout = new javax.swing.GroupLayout(jPanel19);
        jPanel19.setLayout(jPanel19Layout);
        jPanel19Layout.setHorizontalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel16)
                    .addComponent(jLabel17, javax.swing.GroupLayout.PREFERRED_SIZE, 115, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(txt_whole_sale_stock_discount, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 124, Short.MAX_VALUE)
                        .addComponent(txt_retail_stock_discount, javax.swing.GroupLayout.Alignment.LEADING)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel19Layout.setVerticalGroup(
            jPanel19Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel19Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel16)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_whole_sale_stock_discount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel17)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_retail_stock_discount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41))
        );

        jButton3.setText("Reset");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton2.setText("Save");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jPanel21.setBackground(new java.awt.Color(255, 255, 255));
        jPanel21.setBorder(javax.swing.BorderFactory.createTitledBorder("Visibility Information"));

        wholesale_billing_stock.setText("Do you want use wholesale billing with stock ");
        wholesale_billing_stock.setEnabled(false);
        wholesale_billing_stock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                wholesale_billing_stockActionPerformed(evt);
            }
        });

        retail_billing_stock.setText("Do you want use Retail billing with stock ");
        retail_billing_stock.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                retail_billing_stockActionPerformed(evt);
            }
        });

        pos_58.setText("Do you want to use 58 mm termal printer for Ratail Biiling");
        pos_58.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pos_58ActionPerformed(evt);
            }
        });

        pos_80.setText("Do you want to use 80 mm termal printer for Ratail Biiling");
        pos_80.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pos_80ActionPerformed(evt);
            }
        });

        pos_a4.setText("Do you want to use A4 size  printer for Ratail Biiling");
        pos_a4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pos_a4ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel21Layout = new javax.swing.GroupLayout(jPanel21);
        jPanel21.setLayout(jPanel21Layout);
        jPanel21Layout.setHorizontalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(wholesale_billing_stock, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(retail_billing_stock, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel21Layout.createSequentialGroup()
                        .addComponent(pos_58)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(pos_80, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(pos_a4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel21Layout.setVerticalGroup(
            jPanel21Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel21Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(wholesale_billing_stock)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(retail_billing_stock)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pos_58)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pos_80)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(pos_a4)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel22.setBackground(new java.awt.Color(255, 255, 255));
        jPanel22.setBorder(javax.swing.BorderFactory.createTitledBorder("Select Businnes Type"));

        radio_medical.setBackground(new java.awt.Color(255, 255, 255));
        radio_medical.setText(" Pharmacy Medical Store");
        radio_medical.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radio_medicalActionPerformed(evt);
            }
        });

        radio_other.setBackground(new java.awt.Color(255, 255, 255));
        radio_other.setText("Other Store");
        radio_other.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                radio_otherActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel22Layout = new javax.swing.GroupLayout(jPanel22);
        jPanel22.setLayout(jPanel22Layout);
        jPanel22Layout.setHorizontalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(radio_medical, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(18, 18, 18)
                .addComponent(radio_other)
                .addGap(45, 45, 45))
        );
        jPanel22Layout.setVerticalGroup(
            jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel22Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel22Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(radio_medical)
                    .addComponent(radio_other))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel20Layout = new javax.swing.GroupLayout(jPanel20);
        jPanel20.setLayout(jPanel20Layout);
        jPanel20Layout.setHorizontalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel20Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel20Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel21, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel20Layout.createSequentialGroup()
                        .addComponent(jButton3, javax.swing.GroupLayout.PREFERRED_SIZE, 61, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButton2)))
                .addGap(118, 118, 118))
        );
        jPanel20Layout.setVerticalGroup(
            jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel20Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel20Layout.createSequentialGroup()
                        .addComponent(jPanel21, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton2)
                            .addComponent(jButton3)))
                    .addGroup(jPanel20Layout.createSequentialGroup()
                        .addGroup(jPanel20Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel18, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(jPanel19, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel22, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE))))
        );

        javax.swing.GroupLayout jPanel17Layout = new javax.swing.GroupLayout(jPanel17);
        jPanel17.setLayout(jPanel17Layout);
        jPanel17Layout.setHorizontalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, 680, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(424, Short.MAX_VALUE))
        );
        jPanel17Layout.setVerticalGroup(
            jPanel17Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel17Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel20, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(267, Short.MAX_VALUE))
        );

        jTabbedPane2.addTab("Other Configuration", jPanel17);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jTabbedPane2)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 546, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //checked
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    //checked
    private void btn_ProductRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ProductRemoveActionPerformed
        try {
            int srs[] = tbl_ProductName.getSelectedRows();
            for (int i = 0; i < srs.length; i++) {
                tbl_productmodel.removeRow(srs[i]-i);
            }
        } catch (Exception e) {            
            JOptionPane.showMessageDialog(this, "Select any row from the table.", "No row selected.", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_ProductRemoveActionPerformed

    //checked
    private void btn_FlavorRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_FlavorRemoveActionPerformed
        try {
            int srs[] = tbl_FlavorName.getSelectedRows();
            for (int i = 0; i < srs.length; i++) {
                tbl_flavormodel.removeRow(srs[i]-i);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select any row from the table.", "No row selected.", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_FlavorRemoveActionPerformed

    //checked
    private void cb_ProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_ProductActionPerformed
        try {
            tbl_flavormodel.setRowCount(0);
            String product = cb_Product.getSelectedItem().toString();

            for (int i = 0; i < Config.configflavor.size(); i++) {
                ConfigFlavor flavor = Config.configflavor.get(i);            
                if( flavor.getProductname().equals(product)) {
                    tbl_flavormodel.addRow(new Object[] {flavor.getFlavorname()});
                }
            }
            txt_Flavor.setText("");
        } catch (Exception e) {}
    }//GEN-LAST:event_cb_ProductActionPerformed

    //checked
    private void btn_SchemeAddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SchemeAddActionPerformed
        String amount = txt_Amount.getText();
        String percentage = txt_Percentage.getText();        
        
        if(!amount.equals("") && !percentage.equals("")) {        
            int i;
            for (i = 0; i < tbl_schememodel.getRowCount(); i++) {
                if (tbl_Scheme.getValueAt(i, 0).equals(amount) && tbl_Scheme.getValueAt(i, 1).equals(percentage)) {
                    break;
                }
            }
            
            if (i == tbl_schememodel.getRowCount()) {
                tbl_schememodel.addRow(new Object[] {amount,percentage});                
                txt_Amount.setText("");
                txt_Percentage.setText("");
                txt_Amount.requestFocus();
            } else {
                JOptionPane.showMessageDialog(this, "Entry already exits.", "Multiple entry", JOptionPane.ERROR_MESSAGE);
                txt_Amount.requestFocus();
            }        
        } else {
            JOptionPane.showMessageDialog(this, "All fields are mandatory.", "Error", JOptionPane.ERROR_MESSAGE);
            txt_Amount.requestFocus();
        }       
    }//GEN-LAST:event_btn_SchemeAddActionPerformed

    //checked
    private void btn_SchemeRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SchemeRemoveActionPerformed
        try {
            int srs[] = tbl_Scheme.getSelectedRows();
            for (int i = 0; i < srs.length; i++) {
                tbl_schememodel.removeRow(srs[i]-i);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select any row from the table.", "No row selected.", JOptionPane.ERROR_MESSAGE);
        }        
    }//GEN-LAST:event_btn_SchemeRemoveActionPerformed

    //checked
    private void btn_ProductSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ProductSaveActionPerformed
        ConfigProduct[] product = new ConfigProduct[tbl_productmodel.getRowCount()];
        int i;
        try {
            for (i = 0; i < product.length; i++) {
                product[i] = new ConfigProduct();
                product[i].setProductname(tbl_ProductName.getValueAt(i, 0).toString());
                product[i].setHsn_code(tbl_ProductName.getValueAt(i, 1).toString());
                product[i].setBar_code(tbl_ProductName.getValueAt(i, 2).toString());
                product[i].setDiscount(tbl_ProductName.getValueAt(i, 3).toString());
            }
            
            if (Config.configproductmgr.insProduct(product)) {
                onloadReset();
                JOptionPane.showMessageDialog(this,"Products configure successfully.","Configure Successful.",JOptionPane.NO_OPTION);
            } else {
                JOptionPane.showMessageDialog(this,"Problem in configure process.","Error",JOptionPane.ERROR_MESSAGE);
            }            
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_ProductSaveActionPerformed

    //checked
    private void btn_FlavorSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_FlavorSaveActionPerformed
        ConfigFlavor[] flavor = new ConfigFlavor[tbl_flavormodel.getRowCount()];
        int i;
        try {
            for (i = 0; i < flavor.length; i++) {
                flavor[i] = new ConfigFlavor();
                flavor[i].setFlavorname(tbl_FlavorName.getValueAt(i, 0).toString());                
            }
            
            if (Config.configflavormgr.insFlavor(flavor, cb_Product.getSelectedItem().toString())) {
                onloadReset();
                JOptionPane.showMessageDialog(this,"Flavors configure successfully.","Configure Successful.",JOptionPane.NO_OPTION);
            } else {
                JOptionPane.showMessageDialog(this,"Problem in configure process.","Error",JOptionPane.ERROR_MESSAGE);
            }            
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_FlavorSaveActionPerformed

    //checked
    private void btn_SchemeSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SchemeSaveActionPerformed
        ConfigScheme[] scheme = new ConfigScheme[tbl_schememodel.getRowCount()];
        
        int i;
        try {
            for (i = 0; i < scheme.length; i++) {
                scheme[i] = new ConfigScheme();                
                scheme[i].setAmount(tbl_Scheme.getValueAt(i, 0).toString());
                scheme[i].setPercentage(tbl_Scheme.getValueAt(i, 1).toString());
            }
            
            if (Config.configschememgr.insScheme(scheme)) {
                onloadReset();
                JOptionPane.showMessageDialog(this,"Scheme configure successfully.","Configure Successful.",JOptionPane.NO_OPTION);
            } else {
                JOptionPane.showMessageDialog(this,"Problem in configure process.","Error",JOptionPane.ERROR_MESSAGE);
            }            
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_SchemeSaveActionPerformed

    private void btn_ContentRateRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ContentRateRemoveActionPerformed
        try {
            int srs[] = tbl_ContentRate.getSelectedRows();
            for (int i = 0; i < srs.length; i++) {
                tbl_contentmodel.removeRow(srs[i]-i);
            }
        } catch (Exception e) {            
            JOptionPane.showMessageDialog(this, "Select any row from the table.", "No row selected.", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_ContentRateRemoveActionPerformed

    private void btn_ContentRateSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ContentRateSaveActionPerformed
        ConfigContentRate[] contentrate = new ConfigContentRate[tbl_contentmodel.getRowCount()];
        int i;
        try {
            for (i = 0; i < contentrate.length; i++) {
                contentrate[i] = new ConfigContentRate();
                contentrate[i].setContentrate(tbl_ContentRate.getValueAt(i, 0).toString());
            }
            
            if (Config.configcontentratemgr.insContentRate(contentrate)) {
                onloadReset();
                JOptionPane.showMessageDialog(this,"Content Rate configure successfully.","Configure Successful.",JOptionPane.NO_OPTION);
            } else {
                JOptionPane.showMessageDialog(this,"Problem in configure process.","Error",JOptionPane.ERROR_MESSAGE);
            }            
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_ContentRateSaveActionPerformed

    private void txt_ProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_ProductActionPerformed
        setProduct();
    }//GEN-LAST:event_txt_ProductActionPerformed

    private void txt_FlavorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_FlavorActionPerformed
        String flavor;
        try {
            flavor = txt_Flavor.getText().trim().toUpperCase();
        } catch (Exception e) {
            flavor = "";
        }
        
        if (!flavor.equals("")) {        
            int i;
            for (i = 0; i < tbl_flavormodel.getRowCount(); i++) {
                if (flavor.equals(tbl_FlavorName.getValueAt(i, 0).toString())) {
                    break;
                }
            }

            if (i == tbl_flavormodel.getRowCount()) {
                tbl_flavormodel.addRow(new Object[] {flavor});
                txt_Flavor.setText("");                
            } else {
                JOptionPane.showMessageDialog(this, "Entry already exits.", "Multiple entry", JOptionPane.ERROR_MESSAGE);
                txt_Flavor.setText("");
            }
        } else {
            JOptionPane.showMessageDialog(this, "Enter something to add.", "No Data Found", JOptionPane.ERROR_MESSAGE);
            txt_Flavor.setText("");
        }
    }//GEN-LAST:event_txt_FlavorActionPerformed

    private void txt_ContentRateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_ContentRateActionPerformed
        String contentrate;
        try {
            contentrate = txt_ContentRate.getText().trim().toUpperCase();
        } catch (Exception e) {
            contentrate = "";
        }
        
        if (!contentrate.equals("")) {        
            int i;
            for (i = 0; i < tbl_ContentRate.getRowCount(); i++) {
                if (contentrate.equals(tbl_ContentRate.getValueAt(i, 0).toString())) {
                    break;
                }
            }

            if (i == tbl_contentmodel.getRowCount()) {
                tbl_contentmodel.addRow(new Object[] {contentrate});
                txt_ContentRate.setText("");
            } else {
                JOptionPane.showMessageDialog(this, "Entry already exits.", "Multiple entry", JOptionPane.ERROR_MESSAGE);
                txt_ContentRate.setText("");
            }
        } else {
            JOptionPane.showMessageDialog(this, "Enter something to add.", "No Data Found", JOptionPane.ERROR_MESSAGE);
            txt_ContentRate.setText("");
        }
    }//GEN-LAST:event_txt_ContentRateActionPerformed

    private void btn_ProfitRemoveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ProfitRemoveActionPerformed
        try {
            int srs[] = tbl_Profit.getSelectedRows();
            for (int i = 0; i < srs.length; i++) {
                tbl_profitmodel.removeRow(srs[i]-i);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select any row from the table.", "No row selected.", JOptionPane.ERROR_MESSAGE);
        }        
    }//GEN-LAST:event_btn_ProfitRemoveActionPerformed

    private void btn_ProfitSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ProfitSaveActionPerformed
        ConfigProfit[] profit = new ConfigProfit[tbl_profitmodel.getRowCount()];
        
        int i;
        try {
            for (i = 0; i < profit.length; i++) {
                profit[i] = new ConfigProfit();
                try {
                    profit[i].setProduct(tbl_Profit.getValueAt(i, 0).toString());
                } catch (Exception e) {
                    profit[i].setProduct("");
                }
                try {
                    profit[i].setFlavor(tbl_Profit.getValueAt(i, 1).toString());
                } catch (Exception e) {
                    profit[i].setFlavor("");
                }
                try {
                    profit[i].setProfit(tbl_Profit.getValueAt(i, 2).toString());
                } catch (Exception e) {
                    profit[i].setProfit("0");
                }                
            }
            
            if (Config.configprofitmgr.insProfit(profit)) {
                onloadReset();
                JOptionPane.showMessageDialog(this,"Profit configure successfully.","Configure Successful.",JOptionPane.NO_OPTION);
            } else {
                JOptionPane.showMessageDialog(this,"Problem in configure process.","Error",JOptionPane.ERROR_MESSAGE);
            }            
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_ProfitSaveActionPerformed

    private void jTabbedPane2StateChanged(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_jTabbedPane2StateChanged
        int st = jTabbedPane2.getSelectedIndex();
        if (st == 0) {
            try {
                tbl_productmodel.setRowCount(0);
                cb_Product.removeAllItems();            
                cb_product_gst.removeAllItems();            
                for (int i = 0; i < Config.configproduct.size(); i++) {
                    tbl_productmodel.addRow(new Object[] { Config.configproduct.get(i).getProductname(),
                        Config.configproduct.get(i).getHsn_code(),
                        Config.configproduct.get(i).getBar_code(),
                        Config.configproduct.get(i).getDiscount()!=null || Config.configproduct.get(i).getDiscount()!="" ? Config.configproduct.get(i).getDiscount():"0.0",
                    });
                    cb_Product.addItem(Config.configproduct.get(i).getProductname());
                    cb_product_gst.addItem(Config.configproduct.get(i).getProductname());
                }
                cb_product_gstActionPerformed(null);
                tbl_flavormodel.setRowCount(0);            
                for (int i = 0; i < Config.configflavor.size(); i++) {
                    try {
                        if ( cb_Product.getSelectedItem().toString().equals(Config.configflavor.get(i).getProductname() )) {
                            tbl_flavormodel.addRow(new Object[] { Config.configflavor.get(i).getFlavorname() });
                        }
                    } catch (Exception e) {}           
                }

                tbl_contentmodel.setRowCount(0);
                for (int i = 0; i < Config.configcontentrate.size(); i++) {
                    tbl_contentmodel.addRow(new Object[] { Config.configcontentrate.get(i).getContentrate() });                
                }

                txt_Product.setText(null);
                txt_Flavor.setText(null);
                txt_ContentRate.setText(null);
                txt_above_rate.setText("0");
                txt_gst.setText("0");
            } catch (Exception e) {}            
            
        } else if (st == 2) { 
        
            tbl_schememodel.setRowCount(0);
            try {
                for (int i = 0; i < Config.configscheme.size(); i++) {
                    tbl_schememodel.addRow(new Object[] { Config.configscheme.get(i).getAmount(), Config.configscheme.get(i).getPercentage() });
                }
                
                txt_Amount.setText(null);
                txt_Percentage.setText(null);
            } catch (Exception e) {}
            
        } else if (st == 3) {
            try {
                tbl_profitmodel.setRowCount(0);
            
                for (int i = 0; i < Config.configproduct.size(); i++) {
                    int count = 0;
                    for (int j = 0; j < Config.configflavor.size(); j++) {                    
                        if (Config.configflavor.get(j).getProductname().equals(Config.configproduct.get(i).getProductname())) {
                            tbl_profitmodel.addRow(new Object[] {Config.configproduct.get(i).getProductname(), Config.configflavor.get(j).getFlavorname()});
                            count++;
                        }
                    }

                    if (count == 0) {
                        tbl_profitmodel.addRow(new Object[] {Config.configproduct.get(i).getProductname(), ""});
                    }                
                }
                
                for (int i = 0; i < tbl_profitmodel.getRowCount(); i++) {                    
                    for (int j = 0; j < Config.configprofit.size(); j++) {
                        if (Config.configprofit.get(j).getProduct().equals(tbl_Profit.getValueAt(i, 0).toString()) && Config.configprofit.get(j).getFlavor().equals(tbl_Profit.getValueAt(i, 1).toString())) {
                            tbl_Profit.setValueAt(Float.parseFloat(Config.configprofit.get(j).getProfit()), i, 2);
                        }
                    }
                }
            } catch (Exception e) {}
    } else if (st == 1) {
    try{    
                Config.module_text=txt_barocde;
                tbl_RProductName_model.setRowCount(0);
                cb_r_Product.removeAllItems();            
                for (int i = 0; i < Config.configrproduct.size(); i++) {
                    tbl_RProductName_model.addRow(new Object[] { Config.configrproduct.get(i).getProductname() });
                    cb_r_Product.addItem(Config.configrproduct.get(i).getProductname());
                }
                tbl_r_variety_model.setRowCount(0);            
                for (int i = 0; i < Config.configrvariety.size(); i++) {
                    try {
                        if (cb_r_Product.getSelectedItem().toString().equals(Config.configrvariety.get(i).getRproductname())) {
                            tbl_r_variety_model.addRow(new Object[] { 
                                    Config.configrvariety.get(i).getVarietyname(),
                                    Config.configrvariety.get(i).getBarcode(),
                                    Config.configrvariety.get(i).getRate(),
                                    Config.configrvariety.get(i).getUnit(),
                                    Config.configrvariety.get(i).getDiscount()!=null?Config.configrvariety.get(i).getDiscount():"0.0"
                            });
                        }
                    } catch (Exception e) {}           
                }

                txt_rproductname.setText(null);
                txt_variety.setText(null);
                txt_r_ContentRate.setText(null);
                txt_discount.setText("0.0");
            } catch (Exception e) {}            
        }else if(st==4){
            setVat();
        }
    }//GEN-LAST:event_jTabbedPane2StateChanged

    private void txt_rproductnameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_rproductnameActionPerformed
        if(!txt_rproductname.getText().equals("")){
            btn_ProductSave1ActionPerformed(null);
        }
    }//GEN-LAST:event_txt_rproductnameActionPerformed

    private void btn_ProductRemove1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ProductRemove1ActionPerformed
        try {
            int srs[] = tbl_RProductName.getSelectedRows();
            for (int i = 0; i < srs.length; i++) {
                tbl_RProductName_model.removeRow(srs[i]-i);
            }
        } catch (Exception e) {            
            JOptionPane.showMessageDialog(this, "Select any row from the table.", "No row selected.", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_ProductRemove1ActionPerformed

    private void btn_ProductSave1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ProductSave1ActionPerformed
        String rproductname;
        try {
            rproductname = txt_rproductname.getText().trim().toUpperCase();
        } catch (Exception e) {
            rproductname = "";
        }
        
        if (!rproductname.equals("")) {        
            int i;
            for (i = 0; i < tbl_RProductName_model.getRowCount(); i++) {
                if (rproductname.equals(tbl_RProductName_model.getValueAt(i, 0).toString())) {
                    break;
                }
            }

            if (i == tbl_RProductName_model.getRowCount()) {
                tbl_RProductName_model.addRow(new Object[] {rproductname});
                txt_rproductname.setText("");
            } else {
                JOptionPane.showMessageDialog(this, "Entry already exits.", "Multiple entry", JOptionPane.ERROR_MESSAGE);
                txt_rproductname.setText("");
            }
        } else {
            JOptionPane.showMessageDialog(this, "Enter something to add.", "No Data Found", JOptionPane.ERROR_MESSAGE);
            txt_rproductname.setText("");
        }
        
    }//GEN-LAST:event_btn_ProductSave1ActionPerformed

    private void btn_FlavorRemove1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_FlavorRemove1ActionPerformed
        try {
            int srs[] = tbl_r_variety.getSelectedRows();
            for (int i = 0; i < srs.length; i++) {
                tbl_r_variety_model.removeRow(srs[i]-i);
            }
        } catch (Exception e) {            
            JOptionPane.showMessageDialog(this, "Select any row from the table.", "No row selected.", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_FlavorRemove1ActionPerformed

    private void cb_r_ProductActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_r_ProductActionPerformed
        try {
            tbl_r_variety_model.setRowCount(0);
            String rproduct = cb_r_Product.getSelectedItem().toString();

            for (int i = 0; i < Config.configrvariety.size(); i++) {
                ConfigRVariety variety = Config.configrvariety.get(i);            
                if( variety.getRproductname().equals(rproduct)) {
                    tbl_r_variety_model.addRow(new Object[] {variety.getVarietyname(),variety.getBarcode(),variety.getRate(),variety.getUnit(),variety.getDiscount()!=null?Config.configrvariety.get(i).getDiscount():"0.0"});
                }
            }
            
        } catch (Exception e) {}

    }//GEN-LAST:event_cb_r_ProductActionPerformed

    private void txt_varietyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_varietyActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_varietyActionPerformed

    private void btn_FlavorSave1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_FlavorSave1ActionPerformed
        ArrayList<ConfigRVariety> configrvariety = new ArrayList<ConfigRVariety>();
        int i;
        try {
            for (i = 0; i < tbl_r_variety_model.getRowCount(); i++) {
                ConfigRVariety c = new ConfigRVariety();
                c.setVarietyname(tbl_r_variety_model.getValueAt(i, 0).toString());                
                c.setBarcode(tbl_r_variety_model.getValueAt(i, 1).toString());
                c.setRate(tbl_r_variety_model.getValueAt(i, 2).toString());
                c.setUnit(tbl_r_variety_model.getValueAt(i, 3).toString());
                c.setDiscount(tbl_r_variety_model.getValueAt(i, 4).toString());
                configrvariety.add(c);
            }
            
            if (Config.configrvarietymgr.insRVariety(configrvariety, cb_r_Product.getSelectedItem().toString())) {
                onloadReset();
                JOptionPane.showMessageDialog(this,"Flavors configure successfully.","Configure Successful.",JOptionPane.NO_OPTION);
            } else {
                JOptionPane.showMessageDialog(this,"Problem in configure process.","Error",JOptionPane.ERROR_MESSAGE);
            }            
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_FlavorSave1ActionPerformed

    private void txt_r_ContentRateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_r_ContentRateActionPerformed
        if(!txt_r_ContentRate.getText().equals("")){
            btn_ProductSave2ActionPerformed(null);
        }
    }//GEN-LAST:event_txt_r_ContentRateActionPerformed

    private void btn_ProductSave2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ProductSave2ActionPerformed
        String varietyname;
        String rate;
        String discount;
        String s="";
        if(!txt_barocde.getText().equals("")){
             for (int i = 0; i < Config.configrvariety.size(); i++) {
                 try {if(Config.configrvariety.get(i).getBarcode().equals(txt_barocde.getText())){
                        s="no";
                    }} catch (Exception e) {}
             }
             for (int i = 0; i < tbl_r_variety_model.getRowCount(); i++) {
                try {if(tbl_r_variety_model.getValueAt(i, 1).toString().equals(txt_barocde.getText())){
                        s="no";
                    }} catch (Exception e) {}
            }
        }
        
        try {
            varietyname = txt_variety.getText().trim().toUpperCase();
        } catch (Exception e) {
            varietyname = "";
        }
        try {
            rate = txt_r_ContentRate.getText().trim().toUpperCase();
        } catch (Exception e) {
            rate = "";
        }
        try {
            Float.parseFloat(txt_discount.getText().trim());
            discount = txt_discount.getText().trim();
        } catch (Exception e) {
            txt_discount.setText("0.0");
            discount = "0.0";
        }
        
        
        if(!s.equals("no")){if (!varietyname.equals("")&&!rate.equals("")) {        
            int i;
            for (i = 0; i < tbl_r_variety_model.getRowCount(); i++) {
                if (varietyname.equals(tbl_r_variety_model.getValueAt(i, 0).toString())) {
                    break;
                }
            }
            if (i == tbl_r_variety_model.getRowCount()) {
                tbl_r_variety_model.addRow(new Object[] {varietyname,txt_barocde.getText().trim(),rate,cb_unit_variety.getSelectedItem(),discount});
                txt_variety.setText("");                
                txt_r_ContentRate.setText("");          
                txt_barocde.setText("");
                txt_discount.setText("0.0");
            } else {
                JOptionPane.showMessageDialog(this, "Entry already exits.", "Multiple entry", JOptionPane.ERROR_MESSAGE);
                txt_variety.setText("");                
                txt_r_ContentRate.setText("");                
                txt_barocde.setText("");                
                txt_discount.setText("0.0");
            }
        } else {
            JOptionPane.showMessageDialog(this, "Enter something to add.", "No Data Found", JOptionPane.ERROR_MESSAGE);
            txt_variety.setText("");                
                txt_r_ContentRate.setText("");                
                txt_barocde.setText("");                
                txt_discount.setText("0.0");
        }
        }else{
            JOptionPane.showMessageDialog(this, "Barcode already exist.", "Erro", JOptionPane.ERROR_MESSAGE);
            txt_barocde.setText("");                
        }
        txt_variety.requestFocus();
    }//GEN-LAST:event_btn_ProductSave2ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        ConfigRProduct[] rproduct = new ConfigRProduct[tbl_RProductName_model.getRowCount()];
        int i;
        try {
            for (i = 0; i < rproduct.length; i++) {
                rproduct[i] = new ConfigRProduct();
                rproduct[i].setProductname(tbl_RProductName_model.getValueAt(i, 0).toString());
            }
            
            if (Config.configrproductmgr.insRProduct(rproduct)) {
                onloadReset();
                JOptionPane.showMessageDialog(this,"Products configure successfully.","Configure Successful.",JOptionPane.NO_OPTION);
            } else {
                JOptionPane.showMessageDialog(this,"Problem in configure process.","Error",JOptionPane.ERROR_MESSAGE);
            }            
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
        setVat();
    }//GEN-LAST:event_jButton3ActionPerformed

    private void txt_whole_sale_stock_vatCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_whole_sale_stock_vatCaretUpdate
        
    }//GEN-LAST:event_txt_whole_sale_stock_vatCaretUpdate

    private void txt_whole_sale_stock_vatFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_whole_sale_stock_vatFocusLost
        try {
            if(!txt_whole_sale_stock_vat.getText().equals("")){
                Float.parseFloat(txt_whole_sale_stock_vat.getText());
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,"Enter the correct value in Whole Sale Stock Vat %", "Error", JOptionPane.ERROR_MESSAGE);
            txt_whole_sale_stock_vat.setText("");
            txt_whole_sale_stock_vat.requestFocus();
        }
    }//GEN-LAST:event_txt_whole_sale_stock_vatFocusLost

    private void txt_retail_stock_vatFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_retail_stock_vatFocusLost
        try {
            if(!txt_retail_stock_vat.getText().equals("")){
                Float.parseFloat(txt_retail_stock_vat.getText());
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this,"Enter the correct value in Retail stock Vat %", "Error", JOptionPane.ERROR_MESSAGE);
            txt_retail_stock_vat.setText("");
            txt_retail_stock_vat.requestFocus();
        }
    }//GEN-LAST:event_txt_retail_stock_vatFocusLost

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        String str=checkValidation();
        try {
            if (str.equals("ok")) {
                OtherConfig o = new OtherConfig();
                o.setWholesale_stock_vat(txt_whole_sale_stock_vat.getText());
                o.setRetail_stock_vat(txt_retail_stock_vat.getText());
                o.setWholesale_discount(txt_whole_sale_stock_discount.getText());
                o.setRetail_discount(txt_retail_stock_discount.getText());
                o.setRetail_billing_stock(String.valueOf(retailbillingstock));
                o.setPos_58(String.valueOf(pos58));
                o.setPos_80(String.valueOf(pos80));
                o.setPos_a4(String.valueOf(posA4));
                o.setWholesale_bill_type(rmedical?"true":"false");
                if (Config.other_config_mgr.insOtherConfig(o)) {
                    JOptionPane.showMessageDialog(this,"Saving configure successfully", "Successfull", JOptionPane.NO_OPTION);
                } else {
                    JOptionPane.showMessageDialog(this,"Problem in saving", "Error", JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(this,"Check the all Vat field", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
            JOptionPane.showMessageDialog(this,"Problem in saving", "Error", JOptionPane.ERROR_MESSAGE);
        }

    }//GEN-LAST:event_jButton2ActionPerformed

    private void txt_whole_sale_stock_discountCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_whole_sale_stock_discountCaretUpdate
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_whole_sale_stock_discountCaretUpdate

    private void txt_whole_sale_stock_discountFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_whole_sale_stock_discountFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_whole_sale_stock_discountFocusLost

    private void txt_retail_stock_discountFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_retail_stock_discountFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_retail_stock_discountFocusLost

    private void wholesale_billing_stockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_wholesale_billing_stockActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_wholesale_billing_stockActionPerformed

    private void retail_billing_stockActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_retail_billing_stockActionPerformed
        if(retail_billing_stock.isSelected()){    
            retailbillingstock=true;
        }else{
            retailbillingstock=false;
        }    
    }//GEN-LAST:event_retail_billing_stockActionPerformed

    private void pos_58ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pos_58ActionPerformed
        if(pos_58.isSelected()){    
            pos58=true;
            pos80=false;
            posA4=false;
        }    
    }//GEN-LAST:event_pos_58ActionPerformed

    private void pos_80ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pos_80ActionPerformed
        if(pos_80.isSelected()){    
            pos58=false;
            pos80=true;
            posA4=false;
        }    
    }//GEN-LAST:event_pos_80ActionPerformed

    private void pos_a4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pos_a4ActionPerformed
        if(pos_a4.isSelected()){    
            pos58=false;
            pos80=false;
            posA4=true;
        }    
    }//GEN-LAST:event_pos_a4ActionPerformed

    private void txt_barocdeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_barocdeKeyReleased
         
    }//GEN-LAST:event_txt_barocdeKeyReleased

    private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
        setProduct();
    }//GEN-LAST:event_jButton4ActionPerformed

    private void jButton7ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton7ActionPerformed
        setGst();
    }//GEN-LAST:event_jButton7ActionPerformed

    private void txt_gstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_gstActionPerformed
        setGst();
    }//GEN-LAST:event_txt_gstActionPerformed

    private void txt_above_rateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_above_rateActionPerformed
        setGst();
    }//GEN-LAST:event_txt_above_rateActionPerformed

    private void tbl_gstPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_tbl_gstPropertyChange
        
    }//GEN-LAST:event_tbl_gstPropertyChange

    private void btn_gst_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_gst_saveActionPerformed
        WholesaleGst[] wholesale_gst = new WholesaleGst[tbl_gst_model.getRowCount()];
        int i;
        try {
            for (i = 0; i < wholesale_gst.length; i++) {
                wholesale_gst[i] = new WholesaleGst();
                wholesale_gst[i].setMin(tbl_gst_model.getValueAt(i, 0).toString());                
                wholesale_gst[i].setGst(tbl_gst_model.getValueAt(i, 1).toString());                
            }
            if (Config.wholesale_gst_mgr.insWholesaleGst(wholesale_gst, cb_product_gst.getSelectedItem().toString())) {
                onloadReset();
                JOptionPane.showMessageDialog(this,"Product Gst configure successfully.","Configure Successful.",JOptionPane.NO_OPTION);
            } else {
                JOptionPane.showMessageDialog(this,"Problem in configure process.","Error",JOptionPane.ERROR_MESSAGE);
            }            
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this,ex.getMessage(),"Error",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_gst_saveActionPerformed

    private void btn_gst_removeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_gst_removeActionPerformed
        try {
            int srs[] = tbl_gst.getSelectedRows();
            for (int i = 0; i < srs.length; i++) {
                tbl_gst_model.removeRow(srs[i]-i);
            }
        } catch (Exception e) {            
            JOptionPane.showMessageDialog(this, "Select any row from the table.", "No row selected.", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_gst_removeActionPerformed

    private void cb_product_gstActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_product_gstActionPerformed
                tbl_gst_model.setRowCount(0);            
                for (int i = 0; i < Config.wholesale_gst.size(); i++) {
                    try {
                        if ( cb_product_gst.getSelectedItem().toString().equals(Config.wholesale_gst.get(i).getProduct())) {
                            tbl_gst_model.addRow(new Object[] { Config.wholesale_gst.get(i).getMin(),Config.wholesale_gst.get(i).getGst()});
                        }
                    } catch (Exception e) {}           
                }
    }//GEN-LAST:event_cb_product_gstActionPerformed

    private void tbl_ProductNamePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_tbl_ProductNamePropertyChange
        try {
            for (int i = 0; i < tbl_productmodel.getRowCount(); i++) {
                tbl_productmodel.setValueAt(tbl_productmodel.getValueAt(i, 1)!=null?tbl_productmodel.getValueAt(i, 1).toString().toUpperCase():"", i, 1);
                tbl_productmodel.setValueAt(tbl_productmodel.getValueAt(i, 2)!=null?tbl_productmodel.getValueAt(i, 2).toString().toUpperCase():"", i, 2);
                try {
                    if(tbl_productmodel.getValueAt(i, 3)!=null && !tbl_productmodel.getValueAt(i, 3).toString().equals("")){
                        Float.parseFloat(tbl_productmodel.getValueAt(i, 3).toString());
                        tbl_productmodel.setValueAt(tbl_productmodel.getValueAt(i, 3).toString().toUpperCase(), i, 3);
                    }
                } catch (Exception e) {
                    tbl_productmodel.setValueAt("0", i, 3);
                }
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }
        
    }//GEN-LAST:event_tbl_ProductNamePropertyChange

    private void radio_medicalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radio_medicalActionPerformed
         if(radio_medical.isSelected()){    
            rmedical=true;
         } 
    }//GEN-LAST:event_radio_medicalActionPerformed

    private void radio_otherActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_radio_otherActionPerformed
        if(radio_other.isSelected()){    
            rmedical=false;
         } 
    }//GEN-LAST:event_radio_otherActionPerformed
        
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_ContentRateRemove;
    private javax.swing.JButton btn_ContentRateSave;
    private javax.swing.JButton btn_FlavorRemove;
    private javax.swing.JButton btn_FlavorRemove1;
    private javax.swing.JButton btn_FlavorSave;
    private javax.swing.JButton btn_FlavorSave1;
    private javax.swing.JButton btn_ProductRemove;
    private javax.swing.JButton btn_ProductRemove1;
    private javax.swing.JButton btn_ProductSave;
    private javax.swing.JButton btn_ProductSave1;
    private javax.swing.JButton btn_ProductSave2;
    private javax.swing.JButton btn_ProfitRemove;
    private javax.swing.JButton btn_ProfitSave;
    private javax.swing.JButton btn_SchemeAdd;
    private javax.swing.JButton btn_SchemeRemove;
    private javax.swing.JButton btn_SchemeSave;
    private javax.swing.JButton btn_gst_remove;
    private javax.swing.JButton btn_gst_save;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.JComboBox cb_Product;
    private javax.swing.JComboBox cb_product_gst;
    private javax.swing.JComboBox cb_r_Product;
    private javax.swing.JComboBox cb_unit;
    private javax.swing.JComboBox cb_unit_variety;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButton7;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel10;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel12;
    private javax.swing.JPanel jPanel13;
    private javax.swing.JPanel jPanel14;
    private javax.swing.JPanel jPanel15;
    private javax.swing.JPanel jPanel16;
    private javax.swing.JPanel jPanel17;
    private javax.swing.JPanel jPanel18;
    private javax.swing.JPanel jPanel19;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel20;
    private javax.swing.JPanel jPanel21;
    private javax.swing.JPanel jPanel22;
    private javax.swing.JPanel jPanel23;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JSplitPane jSplitPane2;
    private javax.swing.JTabbedPane jTabbedPane2;
    private javax.swing.JRadioButton pos_58;
    private javax.swing.JRadioButton pos_80;
    private javax.swing.JRadioButton pos_a4;
    private javax.swing.JRadioButton radio_medical;
    private javax.swing.JRadioButton radio_other;
    private javax.swing.JCheckBox retail_billing_stock;
    private javax.swing.JTable tbl_ContentRate;
    private javax.swing.JTable tbl_FlavorName;
    private javax.swing.JTable tbl_ProductName;
    private javax.swing.JTable tbl_Profit;
    private javax.swing.JTable tbl_RProductName;
    private javax.swing.JTable tbl_Scheme;
    private javax.swing.JTable tbl_gst;
    private javax.swing.JTable tbl_r_variety;
    private javax.swing.JTextField txt_Amount;
    private javax.swing.JTextField txt_ContentRate;
    private javax.swing.JTextField txt_Flavor;
    private javax.swing.JTextField txt_Percentage;
    private javax.swing.JTextField txt_Product;
    private javax.swing.JTextField txt_above_rate;
    private javax.swing.JTextField txt_barocde;
    private javax.swing.JTextField txt_discount;
    private javax.swing.JTextField txt_gst;
    private javax.swing.JTextField txt_r_ContentRate;
    private javax.swing.JTextField txt_retail_stock_discount;
    private javax.swing.JTextField txt_retail_stock_vat;
    private javax.swing.JTextField txt_rproductname;
    private javax.swing.JTextField txt_variety;
    private javax.swing.JTextField txt_whole_sale_stock_discount;
    private javax.swing.JTextField txt_whole_sale_stock_vat;
    private javax.swing.JCheckBox wholesale_billing_stock;
    // End of variables declaration//GEN-END:variables
   
    //checked
    public void onloadReset() {        
       jTabbedPane2StateChanged(null);
    }
    
    public void setVat(){
        try{    
                txt_whole_sale_stock_vat.setText(Config.config_other_config.get(0).getWholesale_stock_vat());
                txt_retail_stock_vat.setText(Config.config_other_config.get(0).getRetail_stock_vat());
                txt_whole_sale_stock_discount.setText(Config.config_other_config.get(0).getWholesale_discount());
                txt_retail_stock_discount.setText(Config.config_other_config.get(0).getRetail_discount());
                retail_billing_stock.setSelected(Boolean.valueOf(Config.config_other_config.get(0).getRetail_billing_stock()));
                pos_58.setSelected(Boolean.valueOf(Config.config_other_config.get(0).getPos_58()));
                pos_80.setSelected(Boolean.valueOf(Config.config_other_config.get(0).getPos_80()));
                pos_a4.setSelected(Boolean.valueOf(Config.config_other_config.get(0).getPos_a4()));
                radio_medical.setSelected(Config.config_other_config.get(0).getWholesale_bill_type()!=null && Config.config_other_config.get(0).getWholesale_bill_type().equals("true")?true:false);
                radio_other.setSelected(Config.config_other_config.get(0).getWholesale_bill_type()==null||Config.config_other_config.get(0).getWholesale_bill_type().equals("")|| Config.config_other_config.get(0).getWholesale_bill_type().equals("false")?true:false);
            } catch (Exception e) {e.printStackTrace();}            
    }

    private String checkValidation() {
        if(txt_whole_sale_stock_vat.getText().equals("") ){
            return "Whole Sale Stock Vat";
        }else if(txt_retail_stock_vat.getText().equals("")){
            return "Retail Stock Vat";
        }else if(txt_retail_stock_discount.getText().equals("")){
            return "Retail Stock Discount";
        }else if(txt_whole_sale_stock_discount.getText().equals("")){
            return "Whole Sale Stock Discount";
        }else{
            return "ok" ;
        }
    }
    private void setProduct(){
        String product;
        String gst;
        try {
            product = txt_Product.getText().trim().toUpperCase();
        } catch (Exception e) {
            product = "";
        }
        if (product.equals("") ) {
            JOptionPane.showMessageDialog(this, "Enter something to add.", "No Data Found", JOptionPane.ERROR_MESSAGE);
            txt_Product.setText("");
            txt_gst.setText("");
        }else{
            int i;
            for (i = 0; i < tbl_productmodel.getRowCount(); i++) {
                if (product.equals(tbl_ProductName.getValueAt(i, 0).toString())) {
                    break;
                }
            }
            if (i == tbl_productmodel.getRowCount()) {
                tbl_productmodel.addRow(new Object[] {product});
                tbl_ProductName.scrollRectToVisible(tbl_ProductName.getCellRect((tbl_ProductName.getRowCount()-1),0,true));
                txt_Product.setText("");
                txt_gst.setText("");
            } else {
                JOptionPane.showMessageDialog(this, "Entry already exits.", "Multiple entry", JOptionPane.ERROR_MESSAGE);
                txt_Product.setText("");
                txt_gst.setText("");
            }
        } 
        
    } 
    
    private void setGst(){
        String gst=txt_gst.getText().trim();
        String above_rate=txt_above_rate.getText().trim();
        if (gst.equals("") || above_rate.equals("")) {
            JOptionPane.showMessageDialog(this, "Enter something to add.", "No Data Found", JOptionPane.ERROR_MESSAGE);
            txt_above_rate.setText("0");
            txt_gst.setText("0");
        }else{
            try {
                Float.parseFloat(above_rate);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, "Please provide valid above rate.", "In Valid entry", JOptionPane.ERROR_MESSAGE);
                return;
            }
            try {
               Float.parseFloat(gst);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, "Please provide valid above gst.", "In Valid entry", JOptionPane.ERROR_MESSAGE);
                return;
            }
            if(tbl_gst.getRowCount()==0 ){
                 int result = JOptionPane.showConfirmDialog(this, "You do not provide gst rate above 0. So do want to provide "+gst+" rate above 0.","Add", JOptionPane.OK_CANCEL_OPTION);
                 if(result==0){
                    tbl_gst_model.addRow(new Object[] {0,gst});
                    txt_above_rate.setText("0");
                    txt_gst.setText("0");
                    txt_above_rate.requestFocus();
                 }
            }else{
                int i;
                for (i = 0; i < tbl_gst.getRowCount(); i++) {
                    if (above_rate.equals(tbl_gst.getValueAt(i, 0).toString()) || gst.equals(tbl_gst.getValueAt(i, 1).toString()) ) {
                        break;
                    }
                }
                if (i == tbl_gst.getRowCount()) {
                    tbl_gst_model.addRow(new Object[] {above_rate,gst});
                    txt_above_rate.setText("0");
                    txt_gst.setText("0");
                    txt_above_rate.requestFocus();
                } else {
                    JOptionPane.showMessageDialog(this, "Entry already exits.", "Multiple entry", JOptionPane.ERROR_MESSAGE);
                    txt_above_rate.setText("0");
                    txt_gst.setText("0");
                }
            }
        } 
    } 
}