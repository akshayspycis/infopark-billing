package modules.stock;

import datamanager.config.Config;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.SimpleDateFormat;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;

public class StockEntry extends javax.swing.JDialog {
     
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");    
    
    String p = null;
        
    public StockEntry(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images/icon.png")));        
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                if (p.equals("new")) {
                    Config.stocknewbill.setFocus();
                } else {
                    Config.stockviewbill.setFocus();
                }
                dispose();
            }
        });
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        panal = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        cb_unit = new javax.swing.JComboBox();
        txt_quantity = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txt_pack = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txt_batchNo = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        txt_rate = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        txt_totalamount = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txt_vatper = new javax.swing.JTextField();
        jLabel13 = new javax.swing.JLabel();
        txt_vatamt = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        cb_contentrate = new javax.swing.JComboBox();
        jdc_expdate = new com.toedter.calendar.JDateChooser();
        jLabel3 = new javax.swing.JLabel();
        txt_mrp = new javax.swing.JTextField();
        txt_product_name = new javax.swing.JTextField();
        txt_flavor = new javax.swing.JTextField();
        btn_cancel = new javax.swing.JButton();
        btn_save = new javax.swing.JButton();
        btn_reset = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Stock Management - Stock Entry");
        setAlwaysOnTop(true);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        panal.setBackground(new java.awt.Color(255, 255, 255));
        panal.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Stock Entry", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N

        jLabel1.setText("Product");

        jLabel2.setText("Flavor");

        jLabel7.setText("Unit ");

        cb_unit.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "- Select -", "Jar", "Box", "Poly Beg", "Pouch", "Combo & Box", "Pcs", "Kg", "Lit" }));

        txt_quantity.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_quantityCaretUpdate(evt);
            }
        });
        txt_quantity.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_quantityActionPerformed(evt);
            }
        });

        jLabel8.setText("Quantity");

        jLabel10.setText("Pack");

        txt_pack.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_packCaretUpdate(evt);
            }
        });
        txt_pack.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_packActionPerformed(evt);
            }
        });

        jLabel11.setText("Exp. Date");

        jLabel6.setText("Batch No.");

        txt_batchNo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_batchNoActionPerformed(evt);
            }
        });

        jLabel12.setText("Rate");

        txt_rate.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_rateCaretUpdate(evt);
            }
        });
        txt_rate.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_rateActionPerformed(evt);
            }
        });

        jLabel4.setText("Total Amount");

        txt_totalamount.setEditable(false);
        txt_totalamount.setBackground(new java.awt.Color(245, 245, 245));
        txt_totalamount.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txt_totalamount.setEnabled(false);

        jLabel5.setText("GST %");

        txt_vatper.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_vatperCaretUpdate(evt);
            }
        });
        txt_vatper.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_vatperActionPerformed(evt);
            }
        });

        jLabel13.setText("GST Amount");

        txt_vatamt.setEditable(false);
        txt_vatamt.setBackground(new java.awt.Color(245, 245, 245));
        txt_vatamt.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txt_vatamt.setEnabled(false);

        jLabel14.setText("Content Rate");

        jdc_expdate.setDateFormatString("dd-MM-yyyy");

        jLabel3.setText("MRP");

        txt_mrp.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_mrpCaretUpdate(evt);
            }
        });
        txt_mrp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_mrpActionPerformed(evt);
            }
        });

        txt_product_name.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_product_nameActionPerformed(evt);
            }
        });

        txt_flavor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_flavorActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout panalLayout = new javax.swing.GroupLayout(panal);
        panal.setLayout(panalLayout);
        panalLayout.setHorizontalGroup(
            panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panalLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panalLayout.createSequentialGroup()
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(panalLayout.createSequentialGroup()
                                    .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel1)
                                        .addComponent(jLabel4)
                                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGap(139, 139, 139))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panalLayout.createSequentialGroup()
                                    .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(txt_pack, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                        .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(txt_mrp, javax.swing.GroupLayout.DEFAULT_SIZE, 123, Short.MAX_VALUE))))
                            .addComponent(txt_totalamount, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panalLayout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(txt_batchNo, javax.swing.GroupLayout.PREFERRED_SIZE, 256, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txt_product_name))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(panalLayout.createSequentialGroup()
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addComponent(txt_rate, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txt_vatper, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jdc_expdate, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(txt_flavor))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cb_unit, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel13, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txt_vatamt)
                    .addComponent(jLabel14, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(cb_contentrate, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(txt_quantity, javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel9))
        );

        panalLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {cb_contentrate, cb_unit, jLabel13, jLabel14, jLabel7, txt_vatamt});

        panalLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {jLabel5, txt_vatper});

        panalLayout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txt_mrp, txt_rate});

        panalLayout.setVerticalGroup(
            panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panalLayout.createSequentialGroup()
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panalLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addGroup(panalLayout.createSequentialGroup()
                                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel14)
                                    .addComponent(jLabel2))
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(cb_contentrate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panalLayout.createSequentialGroup()
                                .addComponent(jLabel1)
                                .addGap(6, 6, 6)
                                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(txt_product_name, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txt_flavor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                            .addGroup(panalLayout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_batchNo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(panalLayout.createSequentialGroup()
                                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jLabel7)
                                    .addComponent(jLabel11))
                                .addGap(6, 6, 6)
                                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(cb_unit, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jdc_expdate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                    .addGroup(panalLayout.createSequentialGroup()
                        .addGap(80, 80, 80)
                        .addComponent(jLabel9)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10)
                    .addComponent(jLabel12)
                    .addComponent(jLabel3))
                .addGap(6, 6, 6)
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_quantity, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_pack, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_rate, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_mrp, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(jLabel5)
                    .addComponent(jLabel13))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(panalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_totalamount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_vatper, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txt_vatamt, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        panalLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {txt_batchNo, txt_mrp});

        panalLayout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[] {cb_unit, txt_pack, txt_quantity});

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(panal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        btn_cancel.setText("Cancel");
        btn_cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cancelActionPerformed(evt);
            }
        });

        btn_save.setText("Add");
        btn_save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_saveActionPerformed(evt);
            }
        });

        btn_reset.setText("Reset");
        btn_reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_resetActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btn_reset)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_cancel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_save, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {btn_cancel, btn_reset, btn_save});

        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_save)
                        .addComponent(btn_cancel))
                    .addComponent(btn_reset))
                .addGap(6, 6, 6))
        );

        getRootPane().setDefaultButton(btn_save);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    //checked
    private void btn_saveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_saveActionPerformed
        String str = checkValidity();   
        if (str.equals("ok")) {
            
            String[] data = new String[12];            
            data[0] = txt_product_name.getText();
            try {
                data[1] = txt_flavor.getText();
            } catch (Exception e) {
                data[1] = "";
            }
            data[2] = cb_contentrate.getSelectedItem().toString();            
            data[3] = txt_batchNo.getText();
            try {
                data[4] = sdf.format(jdc_expdate.getDate());
            } catch (Exception e) {
                data[4] = "";
            }
            data[5] = cb_unit.getSelectedItem().toString();
            data[6] = txt_pack.getText();
            data[7] = txt_mrp.getText();
            data[8] = txt_rate.getText();
            data[9] = txt_quantity.getText();            
            data[10] = txt_totalamount.getText();
            data[11] = txt_vatper.getText();
            if (p.equals("new")) {
                Config.stocknewbill.insertRow(data);
            } else {
                Config.stockviewbill.insertRow(data);
            }            
            onloadReset(p);
//            cb_product.requestFocus();
        } else {
           JOptionPane.showMessageDialog(this, "Check '"+str+"' field.", "Error", JOptionPane.ERROR_MESSAGE);
        }   
    }//GEN-LAST:event_btn_saveActionPerformed
    
    //checked
    private void btn_cancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_cancelActionPerformed
    
    //checked
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    //checked
    private void btn_resetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_resetActionPerformed
        onloadReset(p);
    }//GEN-LAST:event_btn_resetActionPerformed

    //checked
    private void txt_mrpCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_mrpCaretUpdate
        cal();
    }//GEN-LAST:event_txt_mrpCaretUpdate

    //checked
    private void txt_quantityCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_quantityCaretUpdate
        cal();
    }//GEN-LAST:event_txt_quantityCaretUpdate

    //checked
    private void txt_packCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_packCaretUpdate
        cal();
    }//GEN-LAST:event_txt_packCaretUpdate

    //checked
    private void txt_rateCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_rateCaretUpdate
        cal();
    }//GEN-LAST:event_txt_rateCaretUpdate

    //checked
    private void txt_vatperCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_vatperCaretUpdate
        cal();
    }//GEN-LAST:event_txt_vatperCaretUpdate

    //checked
    private void txt_batchNoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_batchNoActionPerformed
        btn_saveActionPerformed(evt);
    }//GEN-LAST:event_txt_batchNoActionPerformed

    //checked
    private void txt_mrpActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_mrpActionPerformed
        btn_saveActionPerformed(evt);
    }//GEN-LAST:event_txt_mrpActionPerformed

    //checked
    private void txt_rateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_rateActionPerformed
        btn_saveActionPerformed(evt);
    }//GEN-LAST:event_txt_rateActionPerformed

    //checked
    private void txt_quantityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_quantityActionPerformed
        btn_saveActionPerformed(evt);
    }//GEN-LAST:event_txt_quantityActionPerformed

    //checked
    private void txt_packActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_packActionPerformed
        btn_saveActionPerformed(evt);
    }//GEN-LAST:event_txt_packActionPerformed

    //checked
    private void txt_vatperActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_vatperActionPerformed
        btn_saveActionPerformed(evt);
    }//GEN-LAST:event_txt_vatperActionPerformed

    private void txt_flavorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_flavorActionPerformed
        if(!txt_product_name.getText().equals("")){
            Config.item_search_table.onloadReset(2,txt_product_name.getText().trim(),2);
            if(Config.item_search_table.search_model.getRowCount()>0){
                Config.item_search_table.setVisible(true);
            }else{
                cb_contentrate.requestFocus();
            }
        }else{
            txt_product_nameActionPerformed(null);
        }
    }//GEN-LAST:event_txt_flavorActionPerformed

    private void txt_product_nameActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_product_nameActionPerformed
        Config.item_search_table.onloadReset(2,"",1);
        Config.item_search_table.setVisible(true);
    }//GEN-LAST:event_txt_product_nameActionPerformed
       
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cancel;
    private javax.swing.JButton btn_reset;
    private javax.swing.JButton btn_save;
    private javax.swing.JComboBox cb_contentrate;
    private javax.swing.JComboBox cb_unit;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel3;
    private com.toedter.calendar.JDateChooser jdc_expdate;
    private javax.swing.JPanel panal;
    private javax.swing.JTextField txt_batchNo;
    private javax.swing.JTextField txt_flavor;
    private javax.swing.JTextField txt_mrp;
    private javax.swing.JTextField txt_pack;
    private javax.swing.JTextField txt_product_name;
    private javax.swing.JTextField txt_quantity;
    private javax.swing.JTextField txt_rate;
    private javax.swing.JTextField txt_totalamount;
    private javax.swing.JTextField txt_vatamt;
    private javax.swing.JTextField txt_vatper;
    // End of variables declaration//GEN-END:variables
    
    //checked
    public void onloadReset(String what) {
        p = what;     
        txt_product_name.setText("");
        txt_flavor.setText("");
        txt_batchNo.setText("");
        jdc_expdate.setDate(null);
        cb_unit.setSelectedIndex(0);
        txt_pack.setText("");
        txt_mrp.setText("");
        txt_rate.setText("");
        txt_quantity.setText("");
        txt_vatper.setText(Config.config_other_config.get(0).getWholesale_stock_vat());
         cb_contentrate.removeAllItems();
        for (int i = 0; i < Config.configcontentrate.size(); i++) {
            cb_contentrate.addItem(Config.configcontentrate.get(i).getContentrate());
        }
    }    

    //checked
    private String checkValidity() {
        if (txt_batchNo.getText().equals("")) {
            return "Batch No.";
        }
        else if (cb_unit.getSelectedIndex() == 0) {
            return "Unit";
        }
        else if (txt_pack.getText().equals("")) {
            return "Pack";
        }
        else if (txt_mrp.getText().equals("")) {
            return "MRP";
        }
        else if (txt_rate.getText().equals("")) {
            return "Rate";
        }                 
        else if (txt_quantity.getText().equals("")) {
            return "Quantity";
        }        
        else if (txt_totalamount.getText().equals("") || txt_totalamount.getText().equals("0.0")) {
            return "Total Amount";
        }
        else if (txt_vatper.getText().equals("")) {
            return "VAT %";
        }
        else {
            return "ok";
        }
    }
    
    //checked
    public void cal() {
        try {
            int quantity = Integer.parseInt(txt_quantity.getText());
            float rate = Float.parseFloat(txt_rate.getText());
            
            float totalamount = rate * quantity;            
            float vatamount = (totalamount * Float.parseFloat(txt_vatper.getText()))/100;

            txt_totalamount.setText(String.valueOf(totalamount));
            txt_vatamt.setText(String.valueOf(vatamount));
        } catch(Exception e){
            txt_vatamt.setText(null);
            txt_totalamount.setText(null);
        }
    }
    
//    //checked
//    private void calculateTotalItem() {
//        try {
//            txt_totalitem.setText(String.valueOf(Integer.parseInt(txt_quantity.getText())*Integer.parseInt(txt_pack.getText())));
//        } catch(Exception e){
//            txt_totalitem.setText(String.valueOf(0));
//        }        
//    }
//    
//    //checked
//    private void calculateVatAmount() {
//        try {
//            int quantity = Integer.parseInt(txt_quantity.getText());
//            float tp = Float.parseFloat(txt_rate.getText()) * quantity;
//            float vatamount = (tp * Float.parseFloat(txt_vatpercentage.getText()))/100;
//
//            txt_vatamount.setText(String.valueOf(vatamount));
//        } catch(Exception e){
//            txt_vatamount.setText("0.0");
//        }
//    }
//    
//    //checked
//    private void calculateGrossAmount() {
//        try {
//            int quantity = Integer.parseInt(txt_quantity.getText());
//            float tp = Float.parseFloat(txt_rate.getText()) * quantity;
//            float vatamount = (tp * Float.parseFloat(txt_vatpercentage.getText()))/100;
//
//            txt_grossamount.setText(String.valueOf(tp + vatamount));
//        } catch (Exception e) {
//            txt_grossamount.setText("0.0");
//        }        
//    }
//    
//    //checked
//    private void calculateProfitePercentage() {
//        try {
//            int quantity = Integer.parseInt(txt_quantity.getText());
//            float sp = Float.parseFloat(txt_mrp.getText()) * quantity;
//            float tp = Float.parseFloat(txt_rate.getText()) * quantity;
//            float cp = tp+((tp * Float.parseFloat(txt_vatpercentage.getText()))/100);
//            float profitpercentage = ((sp - cp)/cp)*100;
//
//            txt_profitpercentage.setText(String.valueOf(profitpercentage));
//        } catch (Exception e) {
//            txt_profitpercentage.setText("0.0");
//        }        
//    }
    
//  
    
public void setData(String data,int module) {
        if(module==1){
            txt_flavor.setText("");
            txt_product_name.setText(data);
            for (int i = 0; i < Config.wholesale_gst.size(); i++) {
                if(Config.wholesale_gst.get(i).getProduct().equals(data)){
                    txt_vatper.setText(Config.wholesale_gst.get(i).getGst());
                    break;
                }
            }
            txt_flavor.requestFocus();
            txt_flavorActionPerformed(null);
        }else{
            txt_flavor.setText(data);
        }
    }    
}
