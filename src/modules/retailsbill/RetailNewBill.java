/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modules.retailsbill;

import classmanager.pdfMgr.create.PosPdf;
import classmanager.pdfMgr.create.PosPdf80;
import classmanager.pdfMgr.create.RetailBillBidi;
import classmanager.pdfMgr.create.RetailBill_pdf1;
import datamanager.config.Config;
import datamanager.config.ConfigRVariety;
import datamanager.customer.CustomerProfile;
import datamanager.retail.RetailBill;
import datamanager.retail.RetailBillPurchase;
import datamanager.retail.RetailBillReturn;
import datamanager.retail_stock.RetailStock;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TreeSet;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JOptionPane;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author akshay
 */
public class RetailNewBill extends javax.swing.JDialog {
    
    Calendar currentDate;
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
    Boolean barcode = false;
    Boolean barcode_repeat = false;

    DefaultTableModel tbl_purchsemodel;
    public RetailNewBill(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(null);
        tbl_purchsemodel = (DefaultTableModel) tbl_Purchase.getModel();

        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel5 = new javax.swing.JPanel();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel7 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        tbl_Purchase = new javax.swing.JTable();
        btn_Add = new javax.swing.JButton();
        btn_Remove1 = new javax.swing.JButton();
        txt_PurchaseTotal = new javax.swing.JTextField();
        jLabel17 = new javax.swing.JLabel();
        btn_barcode = new javax.swing.JButton();
        txt_barcode = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        txt_discount = new javax.swing.JTextField();
        btn_Cancel = new javax.swing.JButton();
        btn_Save = new javax.swing.JButton();
        jScrollPane6 = new javax.swing.JScrollPane();
        tbl_BillAmt = new javax.swing.JTable();
        btn_Reset = new javax.swing.JButton();
        lbl_Customerid = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jLabel19 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        txt_Billno = new javax.swing.JTextField();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        txt_SalesPerson = new javax.swing.JTextField();
        jLabel23 = new javax.swing.JLabel();
        jdc_Date = new com.toedter.calendar.JDateChooser();
        txt_Customer = new javax.swing.JTextField();
        jLabel24 = new javax.swing.JLabel();
        txt_Tinno = new javax.swing.JTextField();
        jLabel1 = new javax.swing.JLabel();
        txt_locality = new javax.swing.JTextField();
        txt_org = new javax.swing.JTextField();
        check_inter_state = new javax.swing.JCheckBox();

        setTitle("Retail - New Bill");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));

        jTabbedPane1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));

        tbl_Purchase.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbl_Purchase.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "PRODUCT", "VARIETY", "UNIT", "MRP", "QUANTITY ", "AMOUNT", "", ""
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, true, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_Purchase.setRowHeight(20);
        tbl_Purchase.setSelectionBackground(new java.awt.Color(96, 118, 159));
        tbl_Purchase.getTableHeader().setReorderingAllowed(false);
        tbl_Purchase.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                tbl_PurchasePropertyChange(evt);
            }
        });
        jScrollPane4.setViewportView(tbl_Purchase);
        if (tbl_Purchase.getColumnModel().getColumnCount() > 0) {
            tbl_Purchase.getColumnModel().getColumn(0).setResizable(false);
            tbl_Purchase.getColumnModel().getColumn(1).setResizable(false);
            tbl_Purchase.getColumnModel().getColumn(2).setResizable(false);
            tbl_Purchase.getColumnModel().getColumn(3).setResizable(false);
            tbl_Purchase.getColumnModel().getColumn(4).setResizable(false);
            tbl_Purchase.getColumnModel().getColumn(5).setResizable(false);
            tbl_Purchase.getColumnModel().getColumn(6).setMinWidth(0);
            tbl_Purchase.getColumnModel().getColumn(6).setPreferredWidth(0);
            tbl_Purchase.getColumnModel().getColumn(6).setMaxWidth(0);
            tbl_Purchase.getColumnModel().getColumn(7).setMinWidth(0);
            tbl_Purchase.getColumnModel().getColumn(7).setPreferredWidth(0);
            tbl_Purchase.getColumnModel().getColumn(7).setMaxWidth(0);
        }

        btn_Add.setMnemonic('A');
        btn_Add.setText("Add");
        btn_Add.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_AddActionPerformed(evt);
            }
        });

        btn_Remove1.setMnemonic('R');
        btn_Remove1.setText("Remove");
        btn_Remove1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_Remove1ActionPerformed(evt);
            }
        });

        txt_PurchaseTotal.setEditable(false);
        txt_PurchaseTotal.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        jLabel17.setText("Total Amount :");

        btn_barcode.setText("Barcode enabled");
        btn_barcode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_barcodeActionPerformed(evt);
            }
        });

        txt_barcode.setEditable(false);
        txt_barcode.setCaretColor(new java.awt.Color(240, 240, 240));
        txt_barcode.setDisabledTextColor(new java.awt.Color(240, 240, 240));
        txt_barcode.setEnabled(false);
        txt_barcode.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_barcodeCaretUpdate(evt);
            }
        });
        txt_barcode.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txt_barcodeFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_barcodeFocusLost(evt);
            }
        });
        txt_barcode.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                txt_barcodeInputMethodTextChanged(evt);
            }
        });
        txt_barcode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_barcodeActionPerformed(evt);
            }
        });
        txt_barcode.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                txt_barcodePropertyChange(evt);
            }
        });
        txt_barcode.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_barcodeKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_barcodeKeyReleased(evt);
            }
        });

        jLabel2.setText("Discount");

        txt_discount.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_discountCaretUpdate(evt);
            }
        });
        txt_discount.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_discountActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addComponent(btn_Add)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_Remove1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_barcode, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_barcode, javax.swing.GroupLayout.PREFERRED_SIZE, 119, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txt_discount, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel17)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_PurchaseTotal, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 1106, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 334, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_Add)
                        .addComponent(txt_PurchaseTotal, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel17)
                        .addComponent(jLabel2)
                        .addComponent(txt_discount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btn_barcode, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txt_barcode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addComponent(btn_Remove1)))
                .addGap(6, 6, 6))
        );

        jTabbedPane1.addTab("PURCHASE", jPanel7);

        btn_Cancel.setText("Cancel");
        btn_Cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_CancelActionPerformed(evt);
            }
        });

        btn_Save.setText("Save & Print");
        btn_Save.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SaveActionPerformed(evt);
            }
        });

        jScrollPane6.setVerticalScrollBarPolicy(javax.swing.ScrollPaneConstants.VERTICAL_SCROLLBAR_NEVER);

        tbl_BillAmt.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        tbl_BillAmt.setForeground(new java.awt.Color(102, 0, 0));
        tbl_BillAmt.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {"", null, null, "", "", "", ""}
            },
            new String [] {
                "PURCHASE AMOUNT", "GST %", "GST AMOUNT", "DISCOUNT", "PAYABLE AMOUNT", "PAID AMOUNT", "BALANCE AMOUNT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, true, false, true, false, true, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_BillAmt.setColumnSelectionAllowed(true);
        tbl_BillAmt.setRowHeight(25);
        tbl_BillAmt.setSelectionBackground(new java.awt.Color(96, 118, 159));
        tbl_BillAmt.getTableHeader().setReorderingAllowed(false);
        tbl_BillAmt.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                tbl_BillAmtPropertyChange(evt);
            }
        });
        jScrollPane6.setViewportView(tbl_BillAmt);
        tbl_BillAmt.getColumnModel().getSelectionModel().setSelectionMode(javax.swing.ListSelectionModel.SINGLE_INTERVAL_SELECTION);
        if (tbl_BillAmt.getColumnModel().getColumnCount() > 0) {
            tbl_BillAmt.getColumnModel().getColumn(0).setResizable(false);
            tbl_BillAmt.getColumnModel().getColumn(1).setResizable(false);
            tbl_BillAmt.getColumnModel().getColumn(2).setResizable(false);
            tbl_BillAmt.getColumnModel().getColumn(3).setResizable(false);
            tbl_BillAmt.getColumnModel().getColumn(4).setResizable(false);
            tbl_BillAmt.getColumnModel().getColumn(5).setResizable(false);
            tbl_BillAmt.getColumnModel().getColumn(6).setResizable(false);
        }

        btn_Reset.setText("Reset");
        btn_Reset.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ResetActionPerformed(evt);
            }
        });

        jLabel19.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(102, 102, 102));
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel19.setText("Customer Name :");

        jLabel20.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(102, 102, 102));
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel20.setText("Organization :");

        txt_Billno.setEditable(false);

        jLabel21.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(102, 102, 102));
        jLabel21.setText("Bill No. :");

        jLabel22.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(102, 102, 102));
        jLabel22.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel22.setText("Sales Person :");

        txt_SalesPerson.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txt_SalesPerson.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_SalesPersonActionPerformed(evt);
            }
        });

        jLabel23.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(102, 102, 102));
        jLabel23.setText("Billing Date :");

        jdc_Date.setDateFormatString("dd-MM-yyyy");

        txt_Customer.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txt_Customer.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txt_Customer.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_CustomerActionPerformed(evt);
            }
        });

        jLabel24.setBackground(new java.awt.Color(51, 51, 51));
        jLabel24.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel24.setForeground(new java.awt.Color(102, 102, 102));
        jLabel24.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        jLabel24.setText("Tin No. :");

        txt_Tinno.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txt_Tinno.setDisabledTextColor(new java.awt.Color(0, 0, 0));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(102, 102, 102));
        jLabel1.setText("Locality :");

        txt_locality.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txt_locality.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_localityActionPerformed(evt);
            }
        });

        txt_org.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        txt_org.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_orgActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addGap(38, 38, 38)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel24)
                    .addComponent(jLabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(txt_Tinno, javax.swing.GroupLayout.DEFAULT_SIZE, 162, Short.MAX_VALUE)
                    .addComponent(txt_locality))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel20)
                    .addComponent(jLabel21))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(txt_Billno)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel23)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jdc_Date, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txt_org))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel19, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel22, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(txt_SalesPerson, javax.swing.GroupLayout.DEFAULT_SIZE, 310, Short.MAX_VALUE)
                    .addComponent(txt_Customer))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel19, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_Customer, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_org, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel1)
                        .addComponent(txt_locality, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_SalesPerson, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel23, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_Tinno, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel21, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(txt_Billno, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jdc_Date, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel22, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        check_inter_state.setText("InterState Trade");
        check_inter_state.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                check_inter_stateActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jTabbedPane1)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(btn_Reset)
                        .addGap(113, 113, 113)
                        .addComponent(lbl_Customerid, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(check_inter_state)
                        .addGap(4, 4, 4)
                        .addComponent(btn_Cancel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_Save, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane6))
                .addContainerGap())
            .addComponent(jPanel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 411, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(13, 13, 13)
                .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_Save)
                    .addComponent(btn_Cancel)
                    .addComponent(btn_Reset)
                    .addComponent(lbl_Customerid)
                    .addComponent(check_inter_state))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();
    }//GEN-LAST:event_closeDialog

    private void tbl_PurchasePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_tbl_PurchasePropertyChange
       try {
            int row=tbl_Purchase.getSelectedRow();
            int col=tbl_Purchase.getSelectedColumn();
            float rate=Float.parseFloat((String)tbl_purchsemodel.getValueAt(row, col-1));
            int quantity = Integer.parseInt((String)tbl_purchsemodel.getValueAt(row, col));
            float toal_amt = rate*quantity;
            tbl_purchsemodel.setValueAt(toal_amt, row, col+1);
        } catch (Exception e) {
        }
        cal();
    }//GEN-LAST:event_tbl_PurchasePropertyChange

    private void btn_AddActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_AddActionPerformed
        if(Config.config_other_config.get(0).getWholesale_bill_type()==null||Config.config_other_config.get(0).getWholesale_bill_type().equals("")|| Config.config_other_config.get(0).getWholesale_bill_type().equals("false")?true:false){
            Config.retailbillentry_medical.onloadReset("new");
            Config.retailbillentry_medical.setVisible(true);
        }else{
            Config.retailbillentry.onloadReset("new");
            Config.retailbillentry.setVisible(true);
        }
        
    }//GEN-LAST:event_btn_AddActionPerformed

    private void btn_Remove1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_Remove1ActionPerformed
        try {
            tbl_purchsemodel.removeRow(tbl_Purchase.getSelectedRow());
            cal();
        } catch (ArrayIndexOutOfBoundsException e) {
            JOptionPane.showMessageDialog(this,"Select any row form table.","No row selected",JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_Remove1ActionPerformed

    private void btn_CancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_CancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_CancelActionPerformed

    private void btn_SaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SaveActionPerformed
        try {
            String str = checkValidity();
        if (str.equals("ok")) {
            RetailBill retailbil = new RetailBill();
            CustomerProfile cp = new CustomerProfile();
            cp.setCustomerid(lbl_Customerid.getText());
            cp.setOrganization(txt_org.getText());
            cp.setCustomername(txt_Customer.getText());
            cp.setLocality(txt_locality.getText());
            cp.setTinno(txt_Tinno.getText());
            retailbil.setCustomerprofile(cp);
            retailbil.setBillno(txt_Billno.getText());
            retailbil.setDate(sdf.format(jdc_Date.getDate()));
            retailbil.setSalesman(txt_SalesPerson.getText());
            retailbil.setPurchaseamt(tbl_BillAmt.getValueAt(0, 0).toString());
            retailbil.setVatamt(tbl_BillAmt.getValueAt(0, 2).toString());
            retailbil.setDiscount(tbl_BillAmt.getValueAt(0, 3).toString());
            retailbil.setPayableamt(tbl_BillAmt.getValueAt(0, 4).toString());
            retailbil.setPaidamt(tbl_BillAmt.getValueAt(0, 5).toString());
            retailbil.setBalanceamt(tbl_BillAmt.getValueAt(0, 6).toString());

            ArrayList<RetailBillPurchase> retailbillpurchase = new ArrayList<RetailBillPurchase>();
            for (int i = 0; i < tbl_purchsemodel.getRowCount(); i++) {
                RetailBillPurchase wbp = new RetailBillPurchase();
                wbp.setProduct(tbl_purchsemodel.getValueAt(i, 0).toString());
                wbp.setVariety(tbl_purchsemodel.getValueAt(i, 1).toString());
                wbp.setUnit(tbl_purchsemodel.getValueAt(i, 2).toString());
                wbp.setMrp(tbl_purchsemodel.getValueAt(i, 3).toString());
                wbp.setQuantity(tbl_purchsemodel.getValueAt(i, 4).toString());
                wbp.setAmount(tbl_purchsemodel.getValueAt(i, 5).toString());
                retailbillpurchase.add(wbp);
            }
            retailbil.setRetailbillpurchase(retailbillpurchase);
            final boolean check_inter_state_var=check_inter_state.isSelected();

            if (Config.retailbillmgr.insRetailBill(retailbil)) {
                if(!lbl_Customerid.getText().equals("")){
                    for (int i = 0; i < Config.customerprofile.size(); i++) {
                        if(Config.customerprofile.get(i).getCustomerid().equals(lbl_Customerid.getText())){
                            retailbil.setCustomerprofile(Config.customerprofile.get(i));
                            break;
                        }
                    }
                }
                PosPdf pdfmgr=null;
                PosPdf80 pdfmgr80=null;
                RetailBill_pdf1 retailbilli=null;
                RetailBillBidi retail_bill_bidi=null;
                String filename="";
                if(Config.config_other_config.get(0).getPos_58().equals("true")){
                     pdfmgr = new PosPdf();
                      filename = pdfmgr.createPdf(retailbil);
                }
                if(Config.config_other_config.get(0).getPos_80().equals("true")){
                     pdfmgr80 = new PosPdf80();
                      filename = pdfmgr80.createPdf(retailbil);
                }
                if(Config.config_other_config.get(0).getPos_a4().equals("true")){
//                     retailbilli = new RetailBill_pdf1();
                     retail_bill_bidi = new RetailBillBidi();
//                     filename = retailbilli.createPdf(retailbil);
                     filename = retail_bill_bidi.createPdf(retailbil,check_inter_state_var);
                }
                Config.retailmanagement.onloadReset();
                onloadReset();
                if (filename != null) {
                    int op = JOptionPane.showConfirmDialog(this,"Go for print !","Bill saved successfully...",JOptionPane.YES_NO_OPTION);
                    if (op == 0) {
                        if(Config.config_other_config.get(0).getPos_58().equals("true")){
                     pdfmgr.printPdf(filename);
                }
                if(Config.config_other_config.get(0).getPos_80().equals("true")){
                     pdfmgr80.printPdf(filename);
                }
                if(Config.config_other_config.get(0).getPos_a4().equals("true")){
//                     retailbilli.printPdf(filename);
                     retail_bill_bidi.printPdf(filename);
                }
                    }
                } else {
                    JOptionPane.showMessageDialog(this,"Problem in bill printing.","Error.",JOptionPane.ERROR_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(this,"Problem in saving process.","Error.",JOptionPane.ERROR_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(this,"Check the '"+str+"' field.","Error",JOptionPane.ERROR_MESSAGE);
        }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }//GEN-LAST:event_btn_SaveActionPerformed

    private void tbl_BillAmtPropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_tbl_BillAmtPropertyChange
        try {
            float total=Float.parseFloat(tbl_BillAmt.getValueAt(0, 0).toString());
            float dis=(total)*Float.parseFloat(txt_discount.getText())/100;
            if(dis!=Float.parseFloat(tbl_BillAmt.getValueAt(0, 3).toString())){
                flag_discount=true;
            }
            cal();
        } catch (Exception e) {}
    }//GEN-LAST:event_tbl_BillAmtPropertyChange

    
    
    private void btn_ResetActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ResetActionPerformed
        onloadReset();
    }//GEN-LAST:event_btn_ResetActionPerformed

    private void txt_localityActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_localityActionPerformed
        if(txt_locality.getText().equals("")){
            Config.retail_search_table.onloadReset(true);
            Config.retail_search_table.setVisible(true);
        }
    }//GEN-LAST:event_txt_localityActionPerformed

    private void txt_orgActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_orgActionPerformed
        if(txt_org.getText().equals("")){
            Config.retail_search_table.onloadReset(true);
            Config.retail_search_table.setVisible(true);
        }
    }//GEN-LAST:event_txt_orgActionPerformed

    private void txt_CustomerActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_CustomerActionPerformed
            Config.retail_search_table.onloadReset(true);
            Config.retail_search_table.setVisible(true);
    }//GEN-LAST:event_txt_CustomerActionPerformed

    private void txt_SalesPersonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_SalesPersonActionPerformed
        if(txt_SalesPerson.getText().equals("")){
            Config.search_employee.onloadReset(3);
            Config.search_employee.setVisible(true);
        }
    }//GEN-LAST:event_txt_SalesPersonActionPerformed

    private void btn_barcodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_barcodeActionPerformed
        if(barcode){
            barcode=false;
            btn_barcode.setText("Barcode enabled");
            txt_barcode.setEditable(false);
            txt_barcode.setEnabled(false);
            btn_Add.setEnabled(true);
        }else{
            barcode=true;
            btn_barcode.setText("Barcode disabled");
            txt_barcode.setEditable(true);
            txt_barcode.setEnabled(true);
            txt_barcode.requestFocus();
            btn_Add.setEnabled(false);
        }
    }//GEN-LAST:event_btn_barcodeActionPerformed

    private void txt_barcodeCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_barcodeCaretUpdate

    }//GEN-LAST:event_txt_barcodeCaretUpdate

    private void txt_barcodeFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_barcodeFocusGained
        //        btn_barcodeActionPerformed(null);
    }//GEN-LAST:event_txt_barcodeFocusGained

    private void txt_barcodeFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_barcodeFocusLost
        //         btn_barcodeActionPerformed(null);
    }//GEN-LAST:event_txt_barcodeFocusLost

    private void txt_barcodeInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txt_barcodeInputMethodTextChanged

    }//GEN-LAST:event_txt_barcodeInputMethodTextChanged

    private void txt_barcodeActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_barcodeActionPerformed

    }//GEN-LAST:event_txt_barcodeActionPerformed

    private void txt_barcodePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_txt_barcodePropertyChange

    }//GEN-LAST:event_txt_barcodePropertyChange

    private void txt_barcodeKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_barcodeKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_barcodeKeyPressed

    private void txt_barcodeKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_barcodeKeyReleased
        if(evt.getKeyCode() == KeyEvent.VK_ENTER)
        try{Thread.sleep(100);updateBarcode();}catch(InterruptedException e){System.out.println(e);}
    }//GEN-LAST:event_txt_barcodeKeyReleased

    private void txt_discountCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_discountCaretUpdate
       
    }//GEN-LAST:event_txt_discountCaretUpdate

    private void txt_discountActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_discountActionPerformed
         try {
            Float.valueOf(txt_discount.getText());
            cal();
        } catch (Exception e) {}
    }//GEN-LAST:event_txt_discountActionPerformed

    private void check_inter_stateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_check_inter_stateActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_check_inter_stateActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_Add;
    private javax.swing.JButton btn_Cancel;
    private javax.swing.JButton btn_Remove1;
    private javax.swing.JButton btn_Reset;
    private javax.swing.JButton btn_Save;
    private javax.swing.JButton btn_barcode;
    private javax.swing.JCheckBox check_inter_state;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JTabbedPane jTabbedPane1;
    private com.toedter.calendar.JDateChooser jdc_Date;
    private javax.swing.JLabel lbl_Customerid;
    private javax.swing.JTable tbl_BillAmt;
    private javax.swing.JTable tbl_Purchase;
    private javax.swing.JTextField txt_Billno;
    private javax.swing.JTextField txt_Customer;
    private javax.swing.JTextField txt_PurchaseTotal;
    private javax.swing.JTextField txt_SalesPerson;
    private javax.swing.JTextField txt_Tinno;
    private javax.swing.JTextField txt_barcode;
    private javax.swing.JTextField txt_discount;
    private javax.swing.JTextField txt_locality;
    private javax.swing.JTextField txt_org;
    // End of variables declaration//GEN-END:variables

   public void onloadReset() {
        int no = 0;
        try {
            Config.sql = "select max(retailbillid) retailbillid from retailbill";        
            Config.rs = Config.stmt.executeQuery(Config.sql);
            while (Config.rs.next()) {
                no = Integer.parseInt(Config.rs.getString("retailbillid"));
            }            
        } catch (Exception ex) {
            no = 0;
        } 
        lbl_Customerid.setText("");
        txt_Customer.setText("");
        txt_org.setText("");
        txt_Tinno.setText("");
        txt_locality.setText("");
        txt_SalesPerson.setText("");
        txt_discount.setText(Config.config_other_config.get(0).getRetail_discount());
        tbl_purchsemodel.setRowCount(0);
        txt_Billno.setText("STC"+String.valueOf(1000+no));
        jdc_Date.setDate(Calendar.getInstance().getTime());
        tbl_BillAmt.setValueAt(0.0, 0, 0);
        tbl_BillAmt.setValueAt(Config.config_other_config.get(0).getRetail_stock_vat(), 0, 1);
        tbl_BillAmt.setValueAt(0.0, 0, 2);
        tbl_BillAmt.setValueAt(0.0, 0, 3);
        tbl_BillAmt.setValueAt(0.0, 0, 4);
        tbl_BillAmt.setValueAt(0.0, 0, 5);
        cal();        
    }
    
    public int checkRow(String[] data) {
            if(tbl_purchsemodel.getRowCount()>0){
                for (int i = 0; i < tbl_purchsemodel.getRowCount(); i++) {
                    if(tbl_purchsemodel.getValueAt(i, 0).equals(data[0]) &&  tbl_purchsemodel.getValueAt(i, 1).equals(data[1]) && tbl_purchsemodel.getValueAt(i, 6).equals(data[7])){
                        return i;
                    }
                }
            }
            return -1;
    }    
    public void insertRow(String[] data) {
        try { 
                        int j=checkRow(data);
                        if(j>-1){
                            int qty=Integer.parseInt(data[4])+Integer.parseInt((String) tbl_purchsemodel.getValueAt(j, 4));            
                            data[4] = String.valueOf(qty);            
                            data[5] = String.valueOf(qty*Float.parseFloat(data[4]));            
                            updateRow(data,j);
                        }else{
                            tbl_purchsemodel.addRow(new Object[] {
                                data[0],data[1],data[2],data[3],data[4],data[5],data[6]
                            });
                        }
            cal();
        } catch(Exception e) {
            e.printStackTrace();
        }
        cal();
    }    
    public void updateRow(String[] data ,int row) {
        try { 
            tbl_purchsemodel.setValueAt(data[4],row, 4);
            tbl_purchsemodel.setValueAt(data[5],row, 5);
            cal();
        } catch(Exception e) {
            e.printStackTrace();
        }
        cal();
    }        
    
    //checked
    boolean flag_discount=false;
    
    private void cal() {
        try {
            Double totalpurchaseamt = 0.0;
            Double discountamt;
            Double payableamt;
            Double paidamt;
            Double balanceamt;        
            Double vat = 0.0;
            Double vatamt = 0.0;
            for (int i = 0; i < tbl_purchsemodel.getRowCount(); i++) {
                try {
                    totalpurchaseamt = totalpurchaseamt + Double.parseDouble(tbl_Purchase.getValueAt(i, 5).toString());            
                } catch (Exception e) {}
            }            
            try {
                vat = vat + Double.parseDouble(tbl_BillAmt.getValueAt(0, 1).toString());
                vatamt=totalpurchaseamt*vat/100;
            } catch (Exception e) {
                vat=0.0;
                vatamt=0.0;
            }            
            try {
                if(totalpurchaseamt>0){
                    double f=Double.valueOf(txt_discount.getText());
                    double  dis=(totalpurchaseamt)*f/100;
                    if(flag_discount){
                        discountamt = Double.parseDouble(tbl_BillAmt.getValueAt(0, 3).toString());
                        txt_discount.setText(String.valueOf(discountamt*100/(totalpurchaseamt)));
                    }else{
                        discountamt=dis;
                    }
                }else{
                    discountamt = 0.0;
                }
            } catch (Exception e) {
                e.printStackTrace();
                discountamt = 0.0;
            }

            try {
                payableamt = totalpurchaseamt+vatamt-discountamt;
            } catch (Exception e) {
                payableamt = 0.0;
            }

            try {
                paidamt = Double.parseDouble(tbl_BillAmt.getValueAt(0, 5).toString());
            } catch (Exception e) {
                paidamt = 0.0;
            }

            try {
                balanceamt = payableamt - paidamt;
            } catch (Exception e) {
                balanceamt = 0.0;
            }

            txt_PurchaseTotal.setText(new DecimalFormat("#.##").format(totalpurchaseamt));
            tbl_BillAmt.setValueAt(new DecimalFormat("#.##").format(totalpurchaseamt), 0, 0);
            tbl_BillAmt.setValueAt(new DecimalFormat("#.##").format(vat), 0, 1);
            tbl_BillAmt.setValueAt(new DecimalFormat("#.##").format(vatamt), 0, 2);
            tbl_BillAmt.setValueAt(new DecimalFormat("#.##").format(discountamt), 0, 3);
            tbl_BillAmt.setValueAt(new DecimalFormat("#.##").format(payableamt), 0, 4);
            tbl_BillAmt.setValueAt(new DecimalFormat("#.##").format(paidamt), 0, 5);
            tbl_BillAmt.setValueAt(new DecimalFormat("#.##").format(balanceamt), 0, 6);

            jTabbedPane1.setTitleAt(0, "PURCHASE ("+tbl_purchsemodel.getRowCount()+")");
            
        } catch (Exception e) {}        
        flag_discount=false;
    }

    //checked
    private String checkValidity() {        
        if(txt_Customer.getText().equals("")){
            return "Customer";
        }
        if (jdc_Date.getDate() == null) {
            return "Date";
        }
        else if (tbl_Purchase.getRowCount() == 0) {
            return "Purchase Table";
        }        
        else {
            return "ok";
        }        
    }        
    
    public void updateBarcode() {
        String str =txt_barcode.getText().trim();    
        System.out.println(str);
        int i=0;
        if(!barcode_repeat)
        if (!str.equals("")) {
            String[] data = new String[10];            
            for (i = 0; i < Config.configrvariety.size(); i++) {
                try {
                    ConfigRVariety co=Config.configrvariety.get(i);
                    if( co.getBarcode()!=null&&str.equals(co.getBarcode().trim())){
                        data[0] = co.getRproductname();
                        data[1] = co.getVarietyname();
                        data[2] = co.getUnit();
                         if(Config.config_other_config.get(0).getRetail_billing_stock().equals("true")){
                                ArrayList<Integer> list=new ArrayList<Integer>();
                                 for (int j = 0; j < Config.config_retail_stock.size(); j++) {
                                    RetailStock rs= Config.config_retail_stock.get(j);
                                    if( co.getRproductname().contains(rs.getProduct())&&co.getVarietyname().contains(rs.getVariety())){
                                        list.add(j);
                                    }
                                }
                                if(list.size()>1){
                                    Object[] possibilities=new Object[list.size()];
                                    int ob=0;
                                    for (Integer list1 : list) {possibilities[ob]=Config.config_retail_stock.get(list1).getMrp();ob++;}
                                    String ssss = (String)JOptionPane.showInputDialog(null,  
                                    "There are two MRP exits in this Barcode", "Select",  
                                    JOptionPane.PLAIN_MESSAGE, 
                                    UIManager.getIcon("FileChooser.detailsViewIcon"), 
                                    possibilities, "Numbers");  
                                    int qt=0;
                                    for (Integer list1 : list) {
                                        if(Integer.parseInt(Config.config_retail_stock.get(list1).getQuantity())!=0){
                                            qt=Integer.parseInt(Config.config_retail_stock.get(list1).getQuantity());
                                            break;
                                        }
                                    }
                                    if(qt!=0){
                                        data[3] = ssss ;
                                    }else{
                                        JOptionPane.showMessageDialog(this, "This product is not available in stock.", "Error", JOptionPane.ERROR_MESSAGE);
                                        break;
                                    }
                                }else{
                                    if(list.size()>0 && Integer.parseInt(Config.config_retail_stock.get(list.get(0)).getQuantity())!=0){
                                        data[3] = co.getRate();
                                    }else{
                                        JOptionPane.showMessageDialog(this, "This product is not available in stock.", "Error", JOptionPane.ERROR_MESSAGE);
                                        break;
                                    }
                                }
                        }else{
                             data[3] = co.getRate();
                        }
                        setThread();
                        data[4] = "1";
                        data[5] = co.getRate();
                        int j=checkRow(data);
                        if(j>-1){
                            int qty=1+Integer.parseInt((String) tbl_purchsemodel.getValueAt(j, 4));            
                            data[4] = String.valueOf(qty);            
                            data[5] = String.valueOf(qty*Float.parseFloat(co.getRate()));            
                            updateRow(data,j);
                        }else{
                            insertRow(data);
                        }
                        break;
                    }
                } catch (Exception e) {e.printStackTrace();}
            }
            if(i==Config.configrvariety.size()){
                System.out.println("Barcode does not exist");
//                JOptionPane.showMessageDialog(this, "Barcode does not exist.", "Error", JOptionPane.ERROR_MESSAGE);
            }
        } else {
            System.out.println("There is problem in barcode reader or driver");
//            JOptionPane.showMessageDialog(this, "There is problem in barcode reader or driver.", "Error", JOptionPane.ERROR_MESSAGE);
        }   
//        System.out.println(new Date().getTime());
        txt_barcode.setText("");
    }
    
    public void setThread() {
        new Thread() {
            @Override
            public void run() {
                try {
                    barcode_repeat=true;
                    this.sleep(200);
                    barcode_repeat=false;
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }.start();
    }
    

   public void setData(int selectedRow) {
       txt_locality.setText(Config.customerprofile.get(selectedRow).getLocality());
       txt_org.setText(Config.customerprofile.get(selectedRow).getOrganization());
       txt_Customer.setText(Config.customerprofile.get(selectedRow).getCustomername());
       lbl_Customerid.setText(Config.customerprofile.get(selectedRow).getCustomerid());
       txt_Tinno.setText(Config.customerprofile.get(selectedRow).getTinno());
    }
   public void setEmplyeeName(int a) {
        txt_SalesPerson.setText(Config.employeeprofile.get(a).getEmployeename());
    }   
}
