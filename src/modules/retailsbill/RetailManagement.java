/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package modules.retailsbill;

import datamanager.config.Config;
import datamanager.customer.CustomerProfile;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import static java.time.Duration.between;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Vector;
import javax.swing.AbstractAction;
import javax.swing.ActionMap;
import javax.swing.DefaultComboBoxModel;
import javax.swing.InputMap;
import javax.swing.JComponent;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPopupMenu;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author akki
 */
public class RetailManagement extends javax.swing.JDialog {

    Calendar currentDate;
    SimpleDateFormat sdf1 = new SimpleDateFormat("dd-MM-yyyy");
    SimpleDateFormat sdf2 = new SimpleDateFormat("ddMMyyyyhhmma");
    private boolean hide_flag = false;
    private Vector<String> v = new Vector<String>();
    private JTextField tf = null;    
    ArrayList<String> billids;
    
    DefaultTableModel tbl_billmodel;
    public RetailManagement(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        tbl_billmodel = (DefaultTableModel) tbl_bill.getModel();
        this.setLocationRelativeTo(null);
//        autosugg();
        // Close the dialog when Esc is pressed
        String cancelName = "cancel";
        InputMap inputMap = getRootPane().getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
        inputMap.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), cancelName);
        ActionMap actionMap = getRootPane().getActionMap();
        actionMap.put(cancelName, new AbstractAction() {
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
                
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        btn_View = new javax.swing.JButton();
        btn_Cancel = new javax.swing.JButton();
        btn_Refresh = new javax.swing.JButton();
        jPanel7 = new javax.swing.JPanel();
        btn_Search = new javax.swing.JButton();
        jLabel4 = new javax.swing.JLabel();
        cb_searchby = new javax.swing.JComboBox();
        lbl_From = new javax.swing.JLabel();
        jdc_fromDate = new com.toedter.calendar.JDateChooser();
        lbl_To = new javax.swing.JLabel();
        jdc_toDate = new com.toedter.calendar.JDateChooser();
        chb_date = new javax.swing.JCheckBox();
        jSeparator1 = new javax.swing.JSeparator();
        jButton1 = new javax.swing.JButton();
        txt_search = new javax.swing.JTextField();
        jScrollPane3 = new javax.swing.JScrollPane();
        tbl_bill = new javax.swing.JTable();
        txt_totalBalanceAmount = new javax.swing.JTextField();
        jLabel10 = new javax.swing.JLabel();
        txt_totalPaidAmount = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        txt_totalAmount = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();

        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                closeDialog(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jPanel3.setBackground(new java.awt.Color(2, 79, 99));

        jLabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel2.setForeground(new java.awt.Color(255, 255, 255));
        jLabel2.setText("Retail Management");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel2)
                .addContainerGap())
        );

        btn_View.setText("View");
        btn_View.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_ViewActionPerformed(evt);
            }
        });

        btn_Cancel.setText("Cancel");
        btn_Cancel.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_CancelActionPerformed(evt);
            }
        });

        btn_Refresh.setText("Delete");
        btn_Refresh.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_RefreshActionPerformed(evt);
            }
        });

        btn_Search.setText("Search");
        btn_Search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_SearchActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(51, 51, 51));
        jLabel4.setText("Search By :");

        cb_searchby.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Org. Name", "Cus. Name", "Locality" }));
        cb_searchby.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cb_searchbyActionPerformed(evt);
            }
        });

        lbl_From.setText("From :");

        jdc_fromDate.setDateFormatString("dd-MM-yyyy");
        jdc_fromDate.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jdc_fromDatePropertyChange(evt);
            }
        });

        lbl_To.setText("To :");

        jdc_toDate.setDateFormatString("dd-MM-yyyy");
        jdc_toDate.addPropertyChangeListener(new java.beans.PropertyChangeListener() {
            public void propertyChange(java.beans.PropertyChangeEvent evt) {
                jdc_toDatePropertyChange(evt);
            }
        });

        chb_date.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chb_dateActionPerformed(evt);
            }
        });

        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);

        jButton1.setText("New Bill");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        txt_search.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                txt_searchCaretUpdate(evt);
            }
        });
        txt_search.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txt_searchActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(cb_searchby, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txt_search)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(chb_date)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(lbl_From)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jdc_fromDate, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl_To)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jdc_toDate, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_Search, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jButton1)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addGroup(jPanel7Layout.createSequentialGroup()
                        .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jButton1)
                            .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(btn_Search, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(cb_searchby, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(txt_search, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jdc_fromDate, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lbl_To, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jdc_toDate, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(lbl_From, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(chb_date, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(jSeparator1)))
                        .addGap(6, 6, 6))))
        );

        tbl_bill.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tbl_bill.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "CUSTOMER NAME", "CONTACT NO.", "DATE", "BILL NO.", "PAYABLE AMOUNT", "PAID AMOUNT", "BALANCE AMOUNT"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tbl_bill.setRowHeight(18);
        tbl_bill.setSelectionBackground(new java.awt.Color(96, 118, 159));
        tbl_bill.getTableHeader().setReorderingAllowed(false);
        tbl_bill.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tbl_billMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(tbl_bill);
        if (tbl_bill.getColumnModel().getColumnCount() > 0) {
            tbl_bill.getColumnModel().getColumn(0).setResizable(false);
            tbl_bill.getColumnModel().getColumn(1).setResizable(false);
            tbl_bill.getColumnModel().getColumn(2).setResizable(false);
            tbl_bill.getColumnModel().getColumn(3).setResizable(false);
            tbl_bill.getColumnModel().getColumn(4).setResizable(false);
            tbl_bill.getColumnModel().getColumn(5).setResizable(false);
            tbl_bill.getColumnModel().getColumn(6).setResizable(false);
        }
        popupMenu = new JPopupMenu();

        final JMenuItem billsummary = new JMenuItem("Retail Summary");
        popupMenu.add(billsummary);
        tbl_bill.setComponentPopupMenu(popupMenu);

        billsummary.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                if(cb_searchby.getSelectedIndex()==2){
                    int[] sr = tbl_bill.getSelectedRows();
                    ArrayList<String> wholesalebillids = new ArrayList<String>();
                    for (int i=0; i<sr.length; i++) {
                        wholesalebillids.add(billids.get(sr[i]));
                    }
                    Config.retailbillsummary.onloadReset(wholesalebillids,txt_search.getText());
                    Config.retailbillsummary.setVisible(true);

                }else{
                    showMsg();
                }
            }
        });

        txt_totalBalanceAmount.setEditable(false);
        txt_totalBalanceAmount.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        jLabel10.setText("Total Balance Amount :");

        txt_totalPaidAmount.setEditable(false);
        txt_totalPaidAmount.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        jLabel9.setText("Total Paid Amount :");

        txt_totalAmount.setEditable(false);
        txt_totalAmount.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N

        jLabel8.setText("Total Amount :");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_Refresh)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel8)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_totalAmount, javax.swing.GroupLayout.PREFERRED_SIZE, 101, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_totalPaidAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_totalBalanceAmount, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 328, Short.MAX_VALUE)
                        .addComponent(btn_View, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_Cancel))
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 995, Short.MAX_VALUE)
                    .addContainerGap()))
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[] {txt_totalAmount, txt_totalBalanceAmount, txt_totalPaidAmount});

        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 538, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_Cancel)
                        .addComponent(btn_View)
                        .addComponent(btn_Refresh))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(txt_totalBalanceAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel10, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(txt_totalPaidAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel9, javax.swing.GroupLayout.Alignment.TRAILING)
                        .addComponent(txt_totalAmount, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel8, javax.swing.GroupLayout.Alignment.TRAILING)))
                .addGap(10, 10, 10))
            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                    .addContainerGap(95, Short.MAX_VALUE)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 516, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(45, Short.MAX_VALUE)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * Closes the dialog
     */
    private void closeDialog(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_closeDialog
        dispose();  
    }//GEN-LAST:event_closeDialog

    private void btn_ViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_ViewActionPerformed
        try {
            Config.retailviewbill.onloadReset(billids.get(tbl_bill.getSelectedRow()));
            Config.retailviewbill.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Select any row from the table..", "No row selected", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btn_ViewActionPerformed

    private void btn_CancelActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_CancelActionPerformed
        dispose();
    }//GEN-LAST:event_btn_CancelActionPerformed

    private void btn_RefreshActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_RefreshActionPerformed
        int op = JOptionPane.showConfirmDialog(this,"Are you really want to delete Bill entry ?","Confirm Delete",JOptionPane.YES_NO_OPTION);
        if (op == 0) {
            try {
                if (Config.retailbillmgr.delRetailBill(billids.get(tbl_bill.getSelectedRow()))) {
                    onloadReset();
                    JOptionPane.showMessageDialog(this, "Bill entry deleted successfully.", "Deletion successful", JOptionPane.NO_OPTION);
                } else {
                    JOptionPane.showMessageDialog(this, "Error in entry deletion.", "Error", JOptionPane.ERROR_MESSAGE);
                }            
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(this,ex.getMessage(),"Error : No row Selected",JOptionPane.ERROR_MESSAGE);
            }
        }
    }//GEN-LAST:event_btn_RefreshActionPerformed

    private void btn_SearchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_SearchActionPerformed
        currentDate = Calendar.getInstance();
        int filter = cb_searchby.getSelectedIndex();
        String customerid = null;
        String FromDate;
        String ToDate;
        String str;
        try {
            str = txt_search.getText().trim().toUpperCase();
        } catch (Exception e) {
            str = "";
        }

        try {
            FromDate = sdf1.format(jdc_fromDate.getDate());
        } catch (Exception e) {
            jdc_fromDate.setDate(currentDate.getTime());
            FromDate = sdf1.format(jdc_fromDate.getDate());
        }

        try {
            ToDate = sdf1.format(jdc_toDate.getDate());
        } catch (Exception e) {
            jdc_toDate.setDate(currentDate.getTime());
            ToDate = sdf1.format(jdc_toDate.getDate());
        }

        String sql="";
        if (chb_date.isSelected()) {
            if (str.equals("")) {
                sql = "SELECT * FROM retailbill where STR_TO_DATE(date, '%d-%m-%Y') between STR_TO_DATE('"+FromDate+"', '%d-%m-%Y') AND STR_TO_DATE('"+ToDate+"', '%d-%m-%Y')";
            } else {
                sql = "SELECT * FROM retailbill r ,customerprofile c where r.customerid=c.customerid and (UPPER(organization)='"+str+"' or UPPER(customername)='"+str+"' or UPPER(locality)='"+str+"') and STR_TO_DATE(date, '%d-%m-%Y') between STR_TO_DATE('"+FromDate+"', '%d-%m-%Y') AND STR_TO_DATE('"+ToDate+"', '%d-%m-%Y')";
            }
        } else {
            if (str.equals("")) {
                sql = "SELECT * FROM retailbill";
            }else{
                sql = "SELECT * FROM retailbill r ,customerprofile c where r.customerid=c.customerid and (UPPER(organization)='"+str+"' or UPPER(customername)='"+str+"' or UPPER(locality)='"+str+"') ";
            } 
        }
        
        search(sql);
    
    }//GEN-LAST:event_btn_SearchActionPerformed

    private void cb_searchbyActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cb_searchbyActionPerformed
        txt_search.setText("");
    }//GEN-LAST:event_cb_searchbyActionPerformed

    private void jdc_fromDatePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jdc_fromDatePropertyChange
        btn_SearchActionPerformed(null);
    }//GEN-LAST:event_jdc_fromDatePropertyChange

    private void jdc_toDatePropertyChange(java.beans.PropertyChangeEvent evt) {//GEN-FIRST:event_jdc_toDatePropertyChange
        btn_SearchActionPerformed(null);
    }//GEN-LAST:event_jdc_toDatePropertyChange

    private void chb_dateActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chb_dateActionPerformed
        if (chb_date.isSelected()) {
            lbl_From.setEnabled(true);
            jdc_fromDate.setEnabled(true);
            lbl_To.setEnabled(true);
            jdc_toDate.setEnabled(true);
        } else {
            lbl_From.setEnabled(false);
            jdc_fromDate.setEnabled(false);
            lbl_To.setEnabled(false);
            jdc_toDate.setEnabled(false);
        }
    }//GEN-LAST:event_chb_dateActionPerformed

    private void tbl_billMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tbl_billMouseClicked
        if (evt.getClickCount()==2) {
            btn_ViewActionPerformed(null);
        }
    }//GEN-LAST:event_tbl_billMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        Config.retailnewbill.onloadReset();
        Config.retailnewbill.setVisible(true);
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txt_searchCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_txt_searchCaretUpdate
        btn_SearchActionPerformed(null);
    }//GEN-LAST:event_txt_searchCaretUpdate

    private void txt_searchActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txt_searchActionPerformed
        Config.search.onloadReset(true, cb_searchby.getSelectedIndex());
        Config.search.setVisible(true);
    }//GEN-LAST:event_txt_searchActionPerformed
    
    
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_Cancel;
    private javax.swing.JButton btn_Refresh;
    private javax.swing.JButton btn_Search;
    private javax.swing.JButton btn_View;
    private javax.swing.JComboBox cb_searchby;
    private javax.swing.JCheckBox chb_date;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JSeparator jSeparator1;
    private com.toedter.calendar.JDateChooser jdc_fromDate;
    private com.toedter.calendar.JDateChooser jdc_toDate;
    private javax.swing.JLabel lbl_From;
    private javax.swing.JLabel lbl_To;
    private javax.swing.JTable tbl_bill;
    private javax.swing.JPopupMenu popupMenu;
    private javax.swing.JTextField txt_search;
    private javax.swing.JTextField txt_totalAmount;
    private javax.swing.JTextField txt_totalBalanceAmount;
    private javax.swing.JTextField txt_totalPaidAmount;
    // End of variables declaration//GEN-END:variables

    public void onloadReset() {
        chb_date.setSelected(false);
//        chb_dateActionPerformed(null);
//        cb_searchbyActionPerformed(null);
        search("SELECT * FROM retailbill");
        
    }
    
    
        
    public void search(String sql) {
        billids = new ArrayList<String>();
        try {
        tbl_billmodel.setRowCount(0);
        } catch (Exception e) {
        }

        float totalamount = 0;
        float totalpaidamount = 0;
        float balanceamount = 0;
        try {
            Config.sql = sql;
            Config.rs = Config.stmt.executeQuery(Config.sql);
            
            while(Config.rs.next()) {
                billids.add(Config.rs.getString("retailbillid"));
                String customerid = Config.rs.getString("customerid");
                String cus = null;
                String con = null;
                for (int i = 0; i < Config.customerprofile.size(); i++) {
                    CustomerProfile cp = Config.customerprofile.get(i);
                    if (cp.getCustomerid().equals(customerid)) {
                        cus = cp.getCustomername();
                        con = cp.getContactno();
                        break;
                    }                    
                }
                tbl_billmodel.addRow(new Object[] {                    
                    cus,
                    con,
                    Config.rs.getString("date"),
                    Config.rs.getString("billno"),
                    Config.rs.getString("payableamt"),
                    Config.rs.getString("paidamt"),
                    Config.rs.getString("balanceamt")
                });
                
                try {
                    totalamount = totalamount + Float.parseFloat(Config.rs.getString("payableamt"));
                } catch (Exception e) {}
                
                try {
                    totalpaidamount = totalpaidamount + Float.parseFloat(Config.rs.getString("paidamt"));
                } catch (Exception e) {}
                
                try {
                    balanceamount = balanceamount + Float.parseFloat(Config.rs.getString("balanceamt"));     
                } catch (Exception e) {}                
                
            }   
                    txt_totalAmount.setText(String.valueOf(totalamount));
        txt_totalPaidAmount.setText(String.valueOf(totalpaidamount));
        txt_totalBalanceAmount.setText(String.valueOf(balanceamount));

        } catch (Exception ex) {
//            ex.printStackTrace();
        }
        
    }    

    public void setData(String n) {
        txt_search.setText(n);
        btn_SearchActionPerformed(null);
   }
     public void showMsg(){
        JOptionPane.showMessageDialog(this,"Please Select Locality", "Error", JOptionPane.ERROR_MESSAGE);
    }

}
