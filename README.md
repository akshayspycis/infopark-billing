# README #

Billing Software with GST

### What is this repository for? ###

* This is Billing Software with GST it is fully customize software as for client requirement Which provide Basic module of all business, here is the solution switch to GST equipped billing software User friendly interface support all types of platform and print up to 1 lacks of bill and tested 100 % Satisfaction.
* 3.0.0

### How do I get set up? ###

* Java 7 sdk
* Mysql 5
* Printer Driver

### Module

* Wholesale Management
* tail Management
* Stock Management
* Customer Management
* Employee Management with Payroll
* Supplier Management
* MIS Report Management
* Disaster Management
* Product Configuration
* Point of sale & Barcode